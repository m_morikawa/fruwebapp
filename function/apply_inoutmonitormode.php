<?php
	function apply_mode(){
		global $inoutmonitormode;
		$inoutmonitormode->antenna[0]->enabled[0] = $_POST['antenna_0_enabled'];
		if($inoutmonitormode->antenna[0]->enabled[0] == "true"){
			$inoutmonitormode->antenna[0]->power[0] = $_POST['antenna_0_power'];
			$inoutmonitormode->antenna[0]->dwell[0] = $_POST['antenna_0_dwell'];
			$inoutmonitormode->antenna[0]->invcnt[0] = $_POST['antenna_0_invcnt'];
		}
		$inoutmonitormode->antenna[1]->enabled[0] = $_POST['antenna_1_enabled'];
		if($inoutmonitormode->antenna[1]->enabled[0] == "true"){
			$inoutmonitormode->antenna[1]->power[0] = $_POST['antenna_1_power'];
			$inoutmonitormode->antenna[1]->dwell[0] = $_POST['antenna_1_dwell'];
			$inoutmonitormode->antenna[1]->invcnt[0] = $_POST['antenna_1_invcnt'];
		}
		$inoutmonitormode->frequency[0]->ch[0] = $_POST['frequency_ch'];
		$inoutmonitormode->event[0]->in[0] = $_POST['event_in'];
		$inoutmonitormode->dout[0]->in[0]->enabled[0] = $_POST['dout_in_enabled'];
		if($inoutmonitormode->dout[0]->in[0]->enabled[0] == "true"){
			$inoutmonitormode->dout[0]->in[0]->port[0] = $_POST['dout_in_port'];
			$inoutmonitormode->dout[0]->in[0]->downtime[0] = $_POST['dout_in_downtime'];
		}
		$inoutmonitormode->event[0]->out[0] = $_POST['event_out'];
		$inoutmonitormode->dout[0]->out[0]->enabled[0] = $_POST['dout_out_enabled'];
		if($inoutmonitormode->dout[0]->out[0]->enabled[0] == "true"){
			$inoutmonitormode->dout[0]->out[0]->port[0] = $_POST['dout_out_port'];
			$inoutmonitormode->dout[0]->out[0]->downtime[0] = $_POST['dout_out_downtime'];
		}
		$inoutmonitormode->event[0]->timeout[0] = $_POST['event_timeout'];
		$inoutmonitormode->event[0]->datacond[0] = $_POST['event_datacond'];		
		$inoutmonitormode->monitor[0]->enabled[0] = $_POST['monitor_enabled'];
		if($inoutmonitormode->monitor[0]->enabled[0] == "true"){
			$inoutmonitormode->monitor[0]->interval[0] = $_POST['monitor_interval'];
		}
		$inoutmonitormode->taginfo[0]->format[0] = $_POST['taginfo_format'];
		if($inoutmonitormode->taginfo[0]->format[0] == "part"){
			$inoutmonitormode->taginfo[0]->offset[0] = $_POST['taginfo_offset'];
			$inoutmonitormode->taginfo[0]->length[0] = $_POST['taginfo_length'];
		}
		$inoutmonitormode->addinfo[0]->enabled[0] = $_POST['addinfo_enabled'];
		if($inoutmonitormode->addinfo[0]->enabled[0] == "true"){
			$inoutmonitormode->addinfo[0]->id[0] = $_POST['addinfo_id'];
			$inoutmonitormode->addinfo[0]->time[0] = $_POST['addinfo_time'];
			$inoutmonitormode->addinfo[0]->antennano[0] = $_POST['addinfo_antennano'];
			$inoutmonitormode->addinfo[0]->pc[0] = $_POST['addinfo_pc'];
			$inoutmonitormode->addinfo[0]->rssi[0] = $_POST['addinfo_rssi'];
			$inoutmonitormode->addinfo[0]->state[0] = $_POST['addinfo_state'];
		}
		$inoutmonitormode->errorinfo[0]->enabled[0] = $_POST['errorinfo_enabled'];
		$inoutmonitormode->response[0]->enabled[0] = $_POST['response_enabled'];
		if($inoutmonitormode->response[0]->enabled[0] == "true"){
			$inoutmonitormode->response[0]->timeout[0] = $_POST['response_timeout'];
		}
		$inoutmonitormode->request[0]->enabled[0] = $_POST['request_enabled'];
	}
?>

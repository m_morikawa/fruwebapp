<?php
	function apply_filter(){
		global $config;
		$filter = $config->xml()->automode->filter;
		$filter->pc[0]->enabled[0] = $_POST['pcfilter_enabled'];
		if($filter->pc[0]->enabled[0] == "true"){
			$filter->pc[0]->offset[0] = $_POST['pcfilter_offset'];
			$filter->pc[0]->length[0] = $_POST['pcfilter_length'];
			$filter->pc[0]->data[0] = $_POST['pcfilter_data'];
		}
		$filter->epc[0]->enabled[0] = $_POST['epcfilter_enabled'];
		if($filter->epc[0]->enabled[0] == "true"){
			$filter->epc[0]->offset[0] = $_POST['epcfilter_offset'];
			$filter->epc[0]->length[0] = $_POST['epcfilter_length'];
			$filter->epc[0]->data[0] = $_POST['epcfilter_data'];
		}
		$filter->rssi[0]->lower[0]->enabled[0] = $_POST['rssifilter_lower_enabled'];
		if($_POST['rssifilter_lower_enabled'] == "true"){
			$filter->rssi[0]->lower[0]->value[0] = $_POST['rssifilter_lower_value'];
		}
		$filter->rssi[0]->upper[0]->enabled[0] = $_POST['rssifilter_upper_enabled'];
		if($_POST['rssifilter_upper_enabled'] == "true"){
			$filter->rssi[0]->upper[0]->value[0] = $_POST['rssifilter_upper_value'];
		}
	}
?>

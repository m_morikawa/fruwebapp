<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'LogParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}
	$model = new Model();

	$parameter = new LogParameter();
	$sysloginfo = $parameter->xml()->syslog;
	$alertstate = array('visible' => false,'result'=> false,'msg' => '');

	if(isset($_POST['syslog_submit'])){
		function validateSyslog(&$msg){
			$ret = TRUE;
			if(!filter_has_var(INPUT_POST, 'syslog_transfer_enabled')){
				$msg[] = "システムログ外部転送の有効/無効を入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['syslog_transfer_enabled'] !== "true" && $_POST['syslog_transfer_enabled'] !== "false"){
				$msg[] = "不正なシステムログ外部転送の有効/無効が入力されています。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'syslog_transfer_address')){
				$msg[] = "転送先のsyslogサーバーのアドレスを入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_input(INPUT_POST,'syslog_transfer_address',FILTER_VALIDATE_IP,FILTER_FLAG_IPV4)){
				$msg[] = "不正な転送先のsyslogサーバーのアドレスが入力されています。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'syslog_transfer_port')){
				$msg[] = "転送先のsyslogサーバーのポート番号を入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['syslog_transfer_port'] !== "0"){
				if(!filter_input(INPUT_POST, 'syslog_transfer_port', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>65535)))){
					$msg[] = "転送先のsyslogサーバーのポート番号は0から65535の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			return $ret;
		}
		$msg = array();
		if(!validateSyslog($msg)){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			$sysloginfo->transfer[0]->enabled[0] = $_POST['syslog_transfer_enabled'];
			if($sysloginfo->transfer[0]->enabled[0] == 'true'){
				$sysloginfo->transfer[0]->address[0] = $_POST['syslog_transfer_address'];
				$sysloginfo->transfer[0]->port[0] = $_POST['syslog_transfer_port'];
			}
			$parameter->save();
			exec('sudo frucgi update logconfig',$res);
			$status = json_decode($res[0],true);
			if(strpos($status['status'],'success') !== false){
				$alertstate['visible'] = true;
				$alertstate['state'] = true;
			}else{
				$alertstate['visible'] = true;
				$alertstate['state'] = false;
				$alertstate['msg']  = "ネットワーク設定に失敗しました。";
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
	function logToDisabled(){
		document.getElementById("debug_systemtab").disabled=true;
		document.getElementById("debug_apptab").disabled=true;
		document.getElementById("debug_sendactiontab").disabled=true;
		document.getElementById("debug_networktab").disabled=true;
		document.getElementById("system_debugsave").disabled=true;
		document.getElementById('system_debugclear').disabled=true;
	}
	function logToEnabled(){
		document.getElementById("debug_systemtab").disabled=false;
		document.getElementById("debug_apptab").disabled=false;
		document.getElementById("debug_sendactiontab").disabled=false;
		document.getElementById("debug_networktab").disabled=false;
		document.getElementById("system_debugsave").disabled=false;
	}
	function tabRefresh(){
		document.getElementById("debug_systemtab").style.background = "#FFF";
		document.getElementById("debug_systemtab").style.color = "#2f77d1";
		document.getElementById("debug_apptab").style.background = "#FFF";
		document.getElementById("debug_apptab").style.color = "#2f77d1";
		document.getElementById("debug_sendactiontab").style.background = "#FFF";
		document.getElementById("debug_sendactiontab").style.color = "#2f77d1";
		document.getElementById("debug_networktab").style.background = "#FFF";
		document.getElementById("debug_networktab").style.color = "#2f77d1";
	}

	currentlogaction = 'system';
	function onWindowLoaded(){
		document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";

		tabRefresh();
		logToDisabled();
		document.getElementById("debug_systemtab").style.background = "#2f77d1";
		document.getElementById("debug_systemtab").style.color = "#FFF";
		var rand = Math.floor( Math.random() * 9998 ) + 1 ;
		var script = document.createElement('script');
		script.id = "debug_logscript";
		script.src = './js/loggetter.php?logtype=system&rand=' + rand;
		script.type = "text/javascript";
		appendLog = undefined;
		if(script.addEventListener){
			script.addEventListener("load", function(){
				if(typeof appendLog == 'function'){
					appendLog(document.getElementById("debug_loglist"));
				}
				logToEnabled();
			}, false);
		}else if(script.attachEvent){
			script.attachEvent("onload", function(){
				if(typeof appendLog == 'function'){
					appendLog(document.getElementById("debug_loglist"));
				}
				logToEnabled();
			});
			script.onreadystatechange = function(){
					if(this.readyState=="loaded"||this.readyState=="complete"){
						if(typeof appendLog == 'function'){
							appendLog(document.getElementById("debug_loglist"));
						}
						logToEnabled();
					}
			}
		}
		document.getElementById("debug_loglist").appendChild(script);
	}

	(function (myWindow) {
		if(myWindow.addEventListener){
			myWindow.addEventListener('load', onWindowLoaded, false);
		}else if(myWindow.attachEvent){
			myWindow.attachEvent('onload', onWindowLoaded);
		}else{
			myWindow.onload = onWindowLoaded;
		}
	}(window));
</script>
</head>
<body>

<div id="wrapper">
        <div id="header">
		<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
		<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  </div>

  <div id="contents">
  	<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::DEBUGLOG); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF097;"></span><strong>デバッグログ</strong></h2>
				<?php
					if($alertstate['visible']){
						if($alertstate['state']){
echo <<< EOF
<div class="alert alert-primary">
<strong>システムログ転送設定を変更しました。</strong> <br/>
この設定を反映するするためには、<strong>デバイスの電源を切り、再起動してください</strong>。
</div>
EOF;
			}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
						}
					}
				?>
				<div style="padding:16px;">
					<a href="#tab_items"><span class="batch" data-icon="&#xF149;"><strong>ログを確認する</strong></a>
				</div>
				<form method="post" action="debuglog.php" class="forms">
					<fieldset>
						<legend><h5>システムログ転送設定</h5></legend>
						<section>
							<h4>システムログを外部のsyslogサーバーへ転送できます。</h4>
							<p>syslogサーバーの設定をしてください。</p>
							<div style="padding-bottom:16px;">
								<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
								<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
									システムログはUDPプロトコルで送信されます。
								</div>
							</div>
							<div style="padding-bottom:16px;">
								<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
								<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
									この設定は、デバイスの電源を切り、再起動後に反映されます。
								</div>
							</div>
							<section>
								<label>システムログ転送</label>
								<row>
									<column cols="6">
										<select class="select custom" class="width-10" id="syslog_transfer_enabled" name="syslog_transfer_enabled">
											<?php
												if ( $sysloginfo->transfer->enabled == "true"){
													echo "<option value=true selected>転送する</option><option value=false>転送しない</option>";
												} else if ( $sysloginfo->transfer->enabled == "false"){
													echo "<option value=true>転送する</option><option value=false selected>転送しない</option>";
												}
											?>
										</select>
									</column>
								</row>
								<script>
									function onTransferEnabledChanged(){
										if(this.options[this.selectedIndex].value == "true"){
											document.getElementById("transferconfig").style.display="block";

										}else if(this.options[this.selectedIndex].value == "false"){
											document.getElementById("transferconfig").style.display="none";
										}
									}
									if(document.getElementById("syslog_transfer_enabled").addEventListener){
										document.getElementById("syslog_transfer_enabled").addEventListener("change", onTransferEnabledChanged, false);
									}else if(document.getElementById("syslog_transfer_enabled").attachEvent){
										document.getElementById("syslog_transfer_enabled").attachEvent("onchange", onTransferEnabledChanged);
									}else{
										document.getElementById("syslog_transfer_enabled").onchange = onTransferEnabledChanged;
									}
								</script>
							</section>
						</section>
						<div id="transferconfig">
							<section>
								<label>転送先syslogサーバー アドレス</label>
								<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="syslog_transfer_address" value="{$sysloginfo->transfer->address}" placeholder="{$sysloginfo->transfer->address}">
EOF;
								?>
							</section>
							<section>
								<label>転送先syslogサーバー ポート番号</label>
								<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="syslog_transfer_port" value="{$sysloginfo->transfer->port}" placeholder="{$sysloginfo->transfer->port}">
EOF;
								?>
							</section>
							<?php
								if ( $sysloginfo->transfer->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("transferconfig").style.display="block";
</script>
EOF;
								} else if ( $sysloginfo->transfer->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("transferconfig").style.display="none";
</script>
EOF;
								}
							?>
						</div>
						<section>
							<button class="primary" name="syslog_submit">変更する</button>
						</section>
					</fieldset>
				</form>
				<div id="tab_items" style="padding:12px 0px 12px 0px;">
					<span class="btn-group">
						<button id="debug_systemtab" class="primary" outline>システムログ</button>
						<button id="debug_apptab" class="primary" outline>アプリケーションログ</button>
						<button id="debug_sendactiontab" class="primary" outline>送受信ログ</button>
						<button id="debug_networktab" class="primary" outline>ネットワークログ</button>
					</span>
				</div>
				<div style="height:32em;max-width:60em;overflow: auto;background: #000000;border-radius:5px;">
					<ul id="debug_loglist" class="loglist" style="margin:0.4em 3em 0.4em 3em;line-height:1.2;list-style-type:none;color:#E0E0E0;">

					</ul>
				</div>
				<div style="padding:12px 0px 12px 0px;">
					<button id="system_debugclear" class="primary">ログをクリアする</button>
					<?php
echo <<< EOF
<script>
function onDebugClearClicked(){
var elem = document.getElementById('debug_logclearscript');
if(elem){
this.removeChild(elem);
}
var rand = Math.floor( Math.random() * 9998 ) + 1 ;
var script = document.createElement('script');
if(currentlogaction == 'sendaction'){
script.src = './js/logclear.php?logtype=sendaction&rand=' + rand;
}
script.id = 'debug_logclearscript';
this.appendChild(script);
document.getElementById('system_debugclear').style.disabled = true;
document.getElementById('system_debugclear').className = "feedback";
document.getElementById('system_debugclear').innerHTML = "クリアしました";
clearLogList();
setTimeout(function(){
document.getElementById('system_debugclear').style.disabled = false;
document.getElementById('system_debugclear').className = "primary";
document.getElementById('system_debugclear').innerHTML = "ログをクリアする";
}, 3000)
}
if(document.getElementById('system_debugclear').addEventListener){
document.getElementById('system_debugclear').addEventListener("click", onDebugClearClicked, false);
}else if(document.getElementById('system_debugclear').attachEvent){
document.getElementById('system_debugclear').attachEvent("onclick", onDebugClearClicked);
}else{
document.getElementById('system_debugclear').onclick = onDebugClearClicked;
}
</script>
EOF;
					?>
					<button id="system_debugsave" class="primary">ログを保存する</button>
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	ul.loglist li{
		font-size: 16.5px;
		font-weight: 100;
		display:block;
		width: 100%;
	}
</style>
<script>
	function clearLogList(){
		if(document.getElementById("debug_loglist").hasChildNodes()){
			while(document.getElementById("debug_loglist").childNodes.length > 0){
				document.getElementById("debug_loglist").removeChild(document.getElementById("debug_loglist").firstChild);
			}
		}
	}
</script>
<script>
	function onSystemTabClicked(){
		currentlogaction = 'system';

		logToDisabled();
		tabRefresh();
		document.getElementById("debug_systemtab").style.background = "#2f77d1";
		document.getElementById("debug_systemtab").style.color = "#FFF";
		clearLogList();
		document.getElementById("debug_loglist").innerHTML = "<p style=\"font-size:16.5px;\"><span class=\"batch\" data-icon=\"&#xF090;\"></span> システムログを取得しています...</p>"
		var elem = document.getElementById('debug_logscript');
		if(elem){
			document.getElementById("debug_loglist").removeChild(elem);
		}
		var rand = Math.floor( Math.random() * 9998 ) + 1 ;
		var script = document.createElement('script');
		script.id = "debug_logscript";
		script.src = './js/loggetter.php?logtype=system&rand='+rand;
		script.type = "text/javascript";
		appendLog = undefined;
		if(script.addEventListener){
			script.addEventListener("load", function(){
				document.getElementById("debug_loglist").innerHTML = "";
				if(typeof appendLog == 'function'){
					appendLog(document.getElementById("debug_loglist"));
				}
				logToEnabled();
			}, false);
		}else if(script.attachEvent){
			script.attachEvent("onload", function(){
				document.getElementById("debug_loglist").innerHTML = "";
				if(typeof appendLog == 'function'){
					appendLog(document.getElementById("debug_loglist"));
				}
				logToEnabled();
			});
			script.onreadystatechange = function(){
				if(this.readyState=="loaded"||this.readyState=="complete"){
					document.getElementById("debug_loglist").innerHTML = "";
					if(typeof appendLog == 'function'){
						appendLog(document.getElementById("debug_loglist"));
					}
					logToEnabled();
				}
			}
		}
		document.getElementById("debug_loglist").appendChild(script);
	}
	if(document.getElementById("debug_systemtab").addEventListener){
		document.getElementById("debug_systemtab").addEventListener("click", onSystemTabClicked, false);
	}else if(document.getElementById("debug_systemtab").attachEvent){
		document.getElementById("debug_systemtab").attachEvent("onclick", onSystemTabClicked);
	}else{
		document.getElementById("debug_systemtab").onclick = onSystemTabClicked;
	}
</script>
<script>
	function onAppTabClicked(){
		currentlogaction = 'app';

		logToDisabled();
		tabRefresh();
		document.getElementById("debug_apptab").style.background = "#2f77d1";
		document.getElementById("debug_apptab").style.color = "#FFF";
		clearLogList();
		document.getElementById("debug_loglist").innerHTML = "<p style=\"font-size:16.5px;\"><span class=\"batch\" data-icon=\"&#xF090;\"></span> アプリケーションログを取得しています...</p>"
		var elem = document.getElementById('debug_logscript');
		if(elem){
			document.getElementById("debug_loglist").removeChild(elem);
		}
		var rand = Math.floor( Math.random() * 9998 ) + 1 ;
		var script = document.createElement('script');
		script.id = "debug_logscript";
		script.src = './js/loggetter.php?logtype=app&rand=' + rand;
		script.type = "text/javascript";
		appendLog = undefined;
		if(script.addEventListener){
			script.addEventListener("load", function(){
				document.getElementById("debug_loglist").innerHTML = "";
				if(typeof appendLog == 'function'){
					appendLog(document.getElementById("debug_loglist"));
				}
				logToEnabled();
			}, false);
		}else if(script.attachEvent){
			script.attachEvent("onload", function(){
				document.getElementById("debug_loglist").innerHTML = "";
				if(typeof appendLog == 'function'){
					appendLog(document.getElementById("debug_loglist"));
				}
				logToEnabled();
			});
			script.onreadystatechange = function(){
					if(this.readyState=="loaded"||this.readyState=="complete"){
						document.getElementById("debug_loglist").innerHTML = "";
						if(typeof appendLog == 'function'){
							appendLog(document.getElementById("debug_loglist"));
						}
						logToEnabled();
					}
			}
		}
		document.getElementById("debug_loglist").appendChild(script);
	}
	if(document.getElementById("debug_apptab").addEventListener){
		document.getElementById("debug_apptab").addEventListener("click", onAppTabClicked, false);
	}else if(document.getElementById("debug_apptab").attachEvent){
		document.getElementById("debug_apptab").attachEvent("onclick", onAppTabClicked);
	}else{
		document.getElementById("debug_apptab").onclick = onAppTabClicked;
	}
</script>
<script>
	function onSendActionTabClicked(){
		currentlogaction = 'sendaction';

		logToDisabled();
		tabRefresh();
		document.getElementById("debug_sendactiontab").style.background = "#2f77d1";
		document.getElementById("debug_sendactiontab").style.color = "#FFF";
		clearLogList();
		document.getElementById("debug_loglist").innerHTML = "<p style=\"font-size:16.5px;\"><span class=\"batch\" data-icon=\"&#xF090;\"></span> 送受信ログを取得しています...</p>"
		var elem = document.getElementById('debug_logscript');
		if(elem){
			document.getElementById("debug_loglist").removeChild(elem);
		}
		var rand = Math.floor( Math.random() * 9998 ) + 1 ;
		var script = document.createElement('script');
		script.id = "debug_logscript";
		script.src = './js/loggetter.php?logtype=sendaction&rand=' + rand;
		script.type = "text/javascript";
		appendLog = undefined;
		if(script.addEventListener){
			script.addEventListener("load", function(){
				document.getElementById("debug_loglist").innerHTML = "";
				if(typeof appendLog == 'function'){
					appendLog(document.getElementById("debug_loglist"));
				}
				logToEnabled();
				document.getElementById("system_debugclear").disabled=false;
			}, false);
		}else if(script.attachEvent){
			script.attachEvent("onload", function(){
				document.getElementById("debug_loglist").innerHTML = "";
				if(typeof appendLog == 'function'){
					appendLog(document.getElementById("debug_loglist"));
				}
				logToEnabled();
				document.getElementById("system_debugclear").disabled=false;
			});
			script.onreadystatechange = function(){
					if(this.readyState=="loaded"||this.readyState=="complete"){
						document.getElementById("debug_loglist").innerHTML = "";
						if(typeof appendLog == 'function'){
							appendLog(document.getElementById("debug_loglist"));
						}
						logToEnabled();
						document.getElementById("system_debugclear").disabled=false;
					}
			}
		}
		document.getElementById("debug_loglist").appendChild(script);
	}
	if(document.getElementById("debug_sendactiontab").addEventListener){
		document.getElementById("debug_sendactiontab").addEventListener("click", onSendActionTabClicked,false);
	}else if(document.getElementById("debug_sendactiontab").attachEvent){
		document.getElementById("debug_sendactiontab").attachEvent("onclick", onSendActionTabClicked);
	}else{
		document.getElementById("debug_sendactiontab").onclick = onSendActionTabClicked;
	}
</script>
<script>
	function onNetworkTabClicked(){
		currentlogaction = 'network';

		logToDisabled();
		tabRefresh();
		document.getElementById("debug_networktab").style.background = "#2f77d1";
		document.getElementById("debug_networktab").style.color = "#FFF";

		clearLogList();
		document.getElementById("debug_loglist").innerHTML = "<p style=\"font-size:16.5px;\"><span class=\"batch\" data-icon=\"&#xF090;\"></span> ネットワークログを取得しています...</p>"
		var elem = document.getElementById('debug_logscript');
		if(elem){
			document.getElementById("debug_loglist").removeChild(elem);
		}
		var rand = Math.floor( Math.random() * 9998 ) + 1 ;
		var script = document.createElement('script');
		script.id = "debug_logscript";
		script.src = './js/loggetter.php?logtype=network&rand=' + rand;
		script.type = "text/javascript";
		appendLog = undefined;
		if(script.addEventListener){
			script.addEventListener("load", function(){
				document.getElementById("debug_loglist").innerHTML = "";
				if(typeof appendLog == 'function'){
					appendLog(document.getElementById("debug_loglist"));
				}
				logToEnabled();
			}, false);
		}else if(script.attachEvent){
			script.attachEvent("onload", function(){
				document.getElementById("debug_loglist").innerHTML = "";
				if(typeof appendLog == 'function'){
					appendLog(document.getElementById("debug_loglist"));
				}
				logToEnabled();
			});
			script.onreadystatechange = function(){
					if(this.readyState=="loaded"||this.readyState=="complete"){
						document.getElementById("debug_loglist").innerHTML = "";
						if(typeof appendLog == 'function'){
							appendLog(document.getElementById("debug_loglist"));
						}
						logToEnabled();
					}
			}
		}
		document.getElementById("debug_loglist").appendChild(script);
	}
	if(document.getElementById("debug_networktab").addEventListener){
		document.getElementById("debug_networktab").addEventListener("click", onNetworkTabClicked,false);
	}else if(document.getElementById("debug_networktab").attachEvent){
		document.getElementById("debug_networktab").attachEvent("onclick", onNetworkTabClicked);
	}else{
		document.getElementById("debug_networktab").onclick = onNetworkTabClicked;
	}
</script>
<script type="text/javascript">
	function onDebugSaveClicked(){
		var rand = Math.floor( Math.random() * 9998 ) + 1 ;
		if(currentlogaction == 'system'){
			window.location.href = 'logdownload.php?logtype=system&rand='+rand;
		}else if(currentlogaction == 'app'){
			window.location.href = 'logdownload.php?logtype=app&rand='+rand;
		}else if(currentlogaction == 'sendaction'){
			window.location.href = 'logdownload.php?logtype=sendaction&rand='+rand;
		}else if(currentlogaction == 'network'){
			window.location.href = 'logdownload.php?logtype=network&rand='+rand;
		}
		document.getElementById('system_debugsave').style.disabled = true;
		document.getElementById('system_debugsave').className = "feedback";
		document.getElementById('system_debugsave').innerHTML = "保存しています";
		setTimeout(function(){
		document.getElementById('system_debugsave').style.disabled = false;
		document.getElementById('system_debugsave').className = "primary";
		document.getElementById('system_debugsave').innerHTML = "ログを保存する";
		}, 3000)
	}
	if(document.getElementById('system_debugsave').addEventListener){
		document.getElementById('system_debugsave').addEventListener("click", onDebugSaveClicked, false);
	}else if(document.getElementById('system_debugsave').attachEvent){
		document.getElementById('system_debugsave').attachEvent("onclick", onDebugSaveClicked);
	}else{
		document.getElementById('system_debugsave').onclick = onDebugSaveClicked;
	}
</script>
</body>
</html>

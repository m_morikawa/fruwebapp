<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'ModeParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}

	$model = new Model();

	$config = new ModeParameter();
	$mode2conf = $config->mode2conf();
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>

<body>
<div id="wrapper">
	<div id="header">
		<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
		<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
	</div>

	<div id="contents">
  		<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::MODE2); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>オートモード設定 > モード2設定</strong></h2>
				<form method="post" action="senddat.php" class="forms">
					<fieldset>
						<legend><h5>スキャンモード</h5></legend>
						<section>
							<div style="padding-bottom:16px;">
								<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
								<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
									スキャンモード1は、トリガ入力中にRFIDタグをスキャンします。タイムアウト時間を過ぎると自動的に終了します。<br>
									スキャンモード2は、トリガが入った後、ディレイタイム時間経過してからスキャンタイム時間分RFIDタグをスキャンします。
								</div>
							</div>
							<label>スキャンモード</label>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" name="inventory_scanmode" id="inventory_scanmode">
									<?php
										if ( $mode2conf->inventory->mode == "1"){
											echo "<option value=1 selected>スキャンモード1</option><option value=2>スキャンモード2</option>";
										} else if ( $mode2conf->inventory->mode == "2"){
											echo "<option value=1>スキャンモード1</option><option value=2 selected>スキャンモード2</option>";
										}
									?>
									</select>
								</column>
							</row>
							<script>
								function onInventoryScanmodeChanged(){
									if(this.options[this.selectedIndex].value == "1"){
										document.getElementById("scanmode1config").style.display="block";
										document.getElementById("scanmode2config").style.display="none";
									}else if(this.options[this.selectedIndex].value == "2"){
										document.getElementById("scanmode1config").style.display="none";
										document.getElementById("scanmode2config").style.display="block";
									}
								}
								if(document.getElementById("inventory_scanmode").addEventListener){
									document.getElementById("inventory_scanmode").addEventListener("change", onInventoryScanmodeChanged, false);
								}else if(document.getElementById("inventory_scanmode").attachEvent){
									document.getElementById("inventory_scanmode").attachEvent("onchange", onInventoryScanmodeChanged);
								}else{
									document.getElementById("inventory_scanmode").onchange = onInventoryScanmodeChanged;
								}
							</script>
							<div id="scanmode1config">
								<section>
									<label>タイムアウト : ms</label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="inventory_timeout" value="{$mode2conf->inventory->mode1conf->timeout}" placeholder="{$mode2conf->inventory->mode1conf->timeout}">
EOF;
									?>
								</section>
							</div>
							<div id="scanmode2config">
								<section>
									<label>ディレイタイム : ms</label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="inventory_delaytime" value="{$mode2conf->inventory->mode2conf->delaytime}" placeholder="{$mode2conf->inventory->mode2conf->delaytime}">
EOF;
									?>
								</section>
								<section>
									<label>スキャンタイム : ms</label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="inventory_scantime" value="{$mode2conf->inventory->mode2conf->scantime}" placeholder="{$mode2conf->inventory->mode2conf->scantime}">
EOF;
									?>
								</section>
								<?php
								if ( $mode2conf->inventory->mode == "1"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("scanmode1config").style.display="block";
document.getElementById("scanmode2config").style.display="none";
</script>
EOF;
								} else if ( $mode2conf->inventory->mode == "2"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("scanmode1config").style.display="none";
document.getElementById("scanmode2config").style.display="block";
</script>
EOF;
									}
								?>
							</div>
						</section>
					</fieldset>
					<fieldset>
						<legend><h5>外部入力制御</h5></legend>
						<section>
							<label>トリガ入力</label>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" name="inventory_port">
									<?php
										if ( $mode2conf->inventory->port == "0"){
											echo "<option value=0 selected>ポート0</option><option value=1>ポート1</option>";
										} else if ( $mode2conf->inventory->port == "1"){
											echo "<option value=0>ポート0</option><option value=1 selected>ポート1</option>";
										}
									?>
									</select>
								</column>
							</row>
						</section>
						<section>
							<label>トリガ条件</label>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" name="inventory_trigger">
									<?php
										if ( $mode2conf->inventory->trigger == "up"){
											echo "<option value=\"up\" selected>立ち上がり</option><option value=\"down\">立ち下がり</option>";
										} else if ( $mode2conf->inventory->trigger == "down"){
											echo "<option value=\"up\">立ち上がり</option><option value=\"down\" selected>立ち下がり</option>";
										}
									?>
									</select>
								</column>
							</row>
						</section>
					</fieldset>
					<fieldset>
						<legend><h5>外部出力制御</h5></legend>
						<label>外部出力制御</label>
						<section>
						
							<section>
								<row>
									<column cols="6">
										<select class="select custom" class="width-10" id="dout_enabled" name="dout_enabled">
											<?php
												if ( $mode2conf->dout->enabled == "true"){
													echo "<option value=true selected>外部出力を制御する</option><option value=false>外部出力を制御しない</option>";
												} else if ( $mode2conf->dout->enabled == "false"){
													echo "<option value=true>外部出力を制御する</option><option value=false selected>外部出力を制御しない</option>";
												}
											?>
										</select>
									</column>
								</row>
								<script>
									function onDoutEnabledChanged(){
										if(this.options[this.selectedIndex].value == "true"){
											document.getElementById("doutconfig").style.display="block";
										}else if(this.options[this.selectedIndex].value == "false"){
											document.getElementById("doutconfig").style.display="none";
										}
									}
									if(document.getElementById("dout_enabled").addEventListener){
										document.getElementById("dout_enabled").addEventListener("change", onDoutEnabledChanged, false);
									}else if(document.getElementById("dout_enabled").attachEvent){
										document.getElementById("dout_enabled").attachEvent("onchange", onDoutEnabledChanged);
									}else{
										document.getElementById("dout_enabled").onchange = onDoutEnabledChanged;
									}
								</script>
								<div id="doutconfig">
									<section>
										<label>ポート</label>
										<row>
											<column cols="6">
												<select class="select custom" class="width-10" name="dout_port">
												<?php
													if ( $mode2conf->dout->port == "0"){
														echo "<option value=0 selected>ポート0</option><option value=1>ポート1</option>";
													} else if ( $mode2conf->dout->port == "1"){
														echo "<option value=0>ポート0</option><option value=1 selected>ポート1</option>";
													}
												?>
												</select>
											</column>
										</row>
									</section>
									<section>
										<label>保持時間 : ms</label>
										<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="dout_downtime" value="{$mode2conf->dout->downtime}" placeholder="{$mode2conf->dout->downtime}">
EOF;
										?>
									</section>
									<?php
										if ( $mode2conf->dout->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("doutconfig").style.display="block";
</script>
EOF;
										} else if ( $mode2conf->dout->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("doutconfig").style.display="none";
</script>
EOF;
										}
									?>
								</div>
							</section>
						</section>
					</fieldset>
					<fieldset>
						<legend><h5>データ送信タイミング</h5></legend>
						<label>送信タイミング</label>
						<row>
							<column cols="6">
								<select class="select custom" class="width-10" id="whendopost" name="whendopost">
								<?php
									if ( $mode2conf->whendopost == "byscanend"){
										echo "<option value=byscanend selected>スキャン終了時にまとめて送信する</option><option value=bytagdetected>タグを検知するごとに送信する</option>";
									} else if ( $mode2conf->whendopost == "bytagdetected"){
										echo "<option value=byscanend>スキャン終了時にまとめて送信する</option><option value=bytagdetected selected>タグを検知するごとに送信する</option>";
									}
								?>
								</select>
							</column>
						</row>
					</fieldset>
					<fieldset>
						<legend><h5>重複チェック</h5></legend>
						<section>
							<label>重複チェック</label>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" name="multiepccheck_enabled" id="multiepccheck_enabled">
										<?php
											if ( $mode2conf->multiepccheck->enabled == "true"){
												echo "<option value=true selected>重複チェックを行う</option><option value=false>重複チェックを行わない</option>";
											} else if ( $mode2conf->multiepccheck->enabled == "false"){
												echo "<option value=true>重複チェックを行う</option><option value=false selected>重複チェックを行わない</option>";
											}
										?>
									</select>
								</column>
							</row>
						</section>
					</fieldset>
					<fieldset>
						<legend><h5>サーバー通信</h5></legend>
						<section>
							<label>サーバーレスポンス</label>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" name="response_enabled" id="response_enabled">
									<?php
										if ( $mode2conf->response->enabled == "true"){
											echo "<option value=true selected>有効</option><option value=false>無効</option>";
										} else if ( $mode2conf->response->enabled == "false"){
											echo "<option value=true>有効</option><option value=false selected>無効</option>";
										}
									?>
									</select>
								</column>
							</row>
							<script>
								function onResponseEnabledChanged() {
									if(this.options[this.selectedIndex].value == "true"){
										document.getElementById("responseconfig").style.display="block";
									}else if(this.options[this.selectedIndex].value == "false"){
										document.getElementById("responseconfig").style.display="none";
									}
								}
								if(document.getElementById("response_enabled").addEventListener){
									document.getElementById("response_enabled").addEventListener("change", onResponseEnabledChanged, false);
								}else if(document.getElementById("response_enabled").attachEvent){
									document.getElementById("response_enabled").attachEvent("onchange", onResponseEnabledChanged);
								}else{
									document.getElementById("response_enabled").onchange = onResponseEnabledChanged;
								}
							</script>
							<div id="responseconfig">
								<section>
									<div style="padding-bottom:16px;">
										<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
										<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
											リーダーは上位へデータ送信後、タイムアウト時間レスポンスを待ちます。<br>
											レスポンス待ちの間は、上位へのデータ送信は行われずそのデータは破棄されます。
										</div>
									</div>
									<label>タイムアウト時間 : ms</label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="response_timeout" value="{$mode2conf->response->timeout}" placeholder="{$mode2conf->response->timeout}">
EOF;
									?>
								</section>
								<?php
									if ( $mode2conf->response->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("responseconfig").style.display="block";
</script>
EOF;
									} else if ( $mode2conf->response->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("responseconfig").style.display="none";
</script>
EOF;
									}
								?>
							</div>
						</section>
						<section>
							<label>サーバーリクエスト</label>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" name="request_enabled" id="request_enabled">
									<?php
										if ( $mode2conf->request->enabled == "true"){
											echo "<option value=true selected>有効</option><option value=false>無効</option>";
										} else if ( $mode2conf->request->enabled == "false"){
											echo "<option value=true>有効</option><option value=false selected>無効</option>";
										}
									?>
									</select>
								</column>
							</row>
						</section>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
</body>


<?php
	class GetFilter extends CommandBase
	{
		public function __construct(){
			$this->name = "getFilter";
		}

		public function name(){
			return $this->name;
		}

		public function exec(){
			return TRUE;
		}

		public function getResults(){
			$xml = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'] . '/fruplus/others/fruappconfig.xml', null ,true);
			$filter = $xml->autoMode->filter;

			include_once($_SERVER['DOCUMENT_ROOT'] . '/fruplus/class/JAX.php');
			$jax = new JAX();
			$filterArray = $jax->xml2array($filter->asXml());
			$filterArray['pcFilter']['enable'] = ($filterArray['pcFilter']['enable'] === "true");
			$filterArray['pcFilter']['offset'] = intval($filterArray['pcFilter']['offset']);
			$filterArray['pcFilter']['length'] = intval($filterArray['pcFilter']['length']);
			$filterArray['epcFilter']['enable'] = ($filterArray['epcFilter']['enable'] === "true");
			$filterArray['epcFilter']['offset'] = intval($filterArray['epcFilter']['offset']);
			$filterArray['epcFilter']['length'] = intval($filterArray['epcFilter']['length']);
			$filterArray['rssiFilter']['lower']['enable'] = ($filterArray['rssiFilter']['lower']['enable'] === "true");
			$filterArray['rssiFilter']['lower']['value'] = intval($filterArray['rssiFilter']['lower']['value']);
			$filterArray['rssiFilter']['upper']['enable'] = ($filterArray['rssiFilter']['upper']['enable'] === "true");
			$filterArray['rssiFilter']['upper']['value'] = intval($filterArray['rssiFilter']['upper']['value']);
			return array('filter'=>$filterArray);
		}
	}
?>

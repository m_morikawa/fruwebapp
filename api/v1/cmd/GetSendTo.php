<?php
	class GetSendTo extends CommandBase
	{
		public function __construct(){
			$this->name = "getSendTo";
		}

		public function name(){
			return $this->name;
		}

		public function exec(){
			return TRUE;
		}

		public function getResults(){
			$xml = new SimpleXMLElement($_SERVER['DOCUMENT_ROOT'] . '/fruplus/others/fruappconfig.xml', null ,true);
			$sendTo = $xml->autoMode->sendTo;

			include_once($_SERVER['DOCUMENT_ROOT'] . '/fruplus/class/JAX.php');
			$jax = new JAX();
			$sendToArray = $jax->xml2array($sendTo->asXml());
			$sendToArray['socket']['port'] = intval($sendToArray['socket']['port']);
			$sendToArray['http']['port'] = intval($sendToArray['http']['port']);
			return array('sendTo'=>$sendToArray);
		}
	}
?>

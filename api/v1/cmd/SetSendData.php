<?php
	class SetSendData extends CommandBase
	{
		private $sendData = null;
		private $parameters = null;

		public function __construct($parameters){
			$this->name = "setSendData";
			$this->parameters = $parameters;
		}

		public function name(){
			return $this->name;
		}

		public function exec(){
			//Check SendData
			if(!isset($this->parameters['sendData'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_MISSING";
				return FALSE;
			}

			//Check TagInfo
			if(!isset($this->parameters['sendData']['tagInfo'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_TAGINFO_MISSING";
				return FALSE;
			}

			//Check TagInfo Enable
			if(!isset($this->parameters['sendData']['tagInfo']['format'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_TAGINFO_FORMAT_MISSING";
				return FALSE;
			}
			$formats = array('all','part');
			if(!in_array($this->parameters['sendData']['tagInfo']['format'], $formats)){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "SENDDATA_TAGINFO_FORMAT_INVALID";
				return FALSE;
			}

			if($this->parameters['sendData']['tagInfo']['format'] === "part"){
				//Check Offset
				if(!isset($this->parameters['sendData']['tagInfo']['offset'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDDATA_TAGINFO_OFFSET_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['sendData']['tagInfo']['offset'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>0,"max_range"=>15))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDDATA_TAGINFO_OFFSET_OUTRANGE";
					return FALSE;
				}

				//Check Length
				if(!isset($this->parameters['sendData']['tagInfo']['length'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDDATA_TAGINFO_LENGTH_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['sendData']['tagInfo']['length'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>1,"max_range"=>16))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDDATA_TAGINFO_LENGTH_OUTRANGE";
					return FALSE;
				}
			}

			//Check AddInfo
			if(!isset($this->parameters['sendData']['addInfo'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_ADDINFO_MISSING";
				return FALSE;
			}

			//Check AddInfo Id
			if(!isset($this->parameters['sendData']['addInfo']['id'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_ADDINFO_ID_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['sendData']['addInfo']['id'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "SENDDATA_ADDINFO_ID_INVALID";
				return FALSE;
			}

			//Check AddInfo Time
			if(!isset($this->parameters['sendData']['addInfo']['time'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_ADDINFO_TIME_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['sendData']['addInfo']['time'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "SENDDATA_ADDINFO_TIME_INVALID";
				return FALSE;
			}

			//Check AddInfo Antenna
			if(!isset($this->parameters['sendData']['addInfo']['antenna'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_ADDINFO_ANTENNA_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['sendData']['addInfo']['antenna'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "SENDDATA_ADDINFO_ANTENNA_INVALID";
				return FALSE;
			}

			//Check AddInfo Pc
			if(!isset($this->parameters['sendData']['addInfo']['pc'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_ADDINFO_PC_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['sendData']['addInfo']['pc'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "SENDDATA_ADDINFO_PC_INVALID";
				return FALSE;
			}

			//Check AddInfo RSSI
			if(!isset($this->parameters['sendData']['addInfo']['rssi'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_ADDINFO_RSSI_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['sendData']['addInfo']['rssi'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "SENDDATA_ADDINFO_RSSI_INVALID";
				return FALSE;
			}

			//Check AddInfo State
			if(!isset($this->parameters['sendData']['addInfo']['state'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_ADDINFO_STATE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['sendData']['addInfo']['state'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "SENDDATA_ADDINFO_STATE_INVALID";
				return FALSE;
			}

			//Check AddInfo Memory
			if(!isset($this->parameters['sendData']['addInfo']['memory'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_ADDINFO_MEMORY_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['sendData']['addInfo']['memory'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "SENDDATA_ADDINFO_MEMORY_INVALID";
				return FALSE;
			}

			if($this->parameters['sendData']['addInfo']['memory']){
				//Check AddInfo AccessInfo
				if(!isset($this->parameters['sendData']['addInfo']['accessInfo'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDDATA_ADDINFO_ACCESSINFO_MISSING";
					return FALSE;
				}

				//Check AddInfo AccessInfo Password
				if(!isset($this->parameters['sendData']['addInfo']['accessInfo']['password'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDDATA_ADDINFO_ACCESSINFO_PASSWORD_MISSING";
					return FALSE;
				}
				if(!ctype_xdigit($this->parameters['sendData']['addInfo']['accessInfo']['password'])){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDDATA_ADDINFO_ACCESSINFO_PASSWORD_INVALID_FORMAT";
					return FALSE;
				}
				if(strlen($this->parameters['sendData']['addInfo']['accessInfo']['password']) != 8){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDDATA_ADDINFO_ACCESSINFO_PASSWORD_INVALID_LENGTH";
					return FALSE;
				}

				//Check AddInfo AccessInfo Bank
				if(!isset($this->parameters['sendData']['addInfo']['accessInfo']['bank'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDDATA_ADDINFO_ACCESSINFO_BANK_MISSING";
					return FALSE;
				}
				$banks = array('reserved','epc','tid','user');
				if(!in_array($this->parameters['sendData']['addInfo']['accessInfo']['bank'], $banks)){
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDDATA_ADDINFO_ACCESSINFO_BANK_INVALID";
					return FALSE;
				}

				//Check AddInfo AccessInfo Offset
				if(!isset($this->parameters['sendData']['addInfo']['accessInfo']['offset'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDDATA_ADDINFO_ACCESSINFO_OFFSET_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['sendData']['addInfo']['accessInfo']['offset'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>0,"max_range"=>255))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDDATA_ADDINFO_ACCESSINFO_OFFSET_OUTRANGE";
					return FALSE;
				}

				//Check AddInfo AccessInfo Length
				if(!isset($this->parameters['sendData']['addInfo']['accessInfo']['length'])){
					$this->errorCode = CommandBase::MISSINGPARAMETER;
					$this->errorDetail = "SENDDATA_ADDINFO_ACCESSINFO_LENGTH_MISSING";
					return FALSE;
				}
				if(filter_var($this->parameters['sendData']['addInfo']['accessInfo']['length'],
					FILTER_VALIDATE_INT,
					array('options' => array("min_range"=>1,"max_range"=>256))) === FALSE)
				{
					$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
					$this->errorDetail = "SENDDATA_ADDINFO_ACCESSINFO_LENGTH_OUTRANGE";
					return FALSE;
				}
			}

			//Check ErrorInfo
			if(!isset($this->parameters['sendData']['errorInfo'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_ERRORINFO_MISSING";
				return FALSE;
			}
			
			//Check ErrorInfo Enable
			if(!isset($this->parameters['sendData']['errorInfo']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_ERRORINFO_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['sendData']['errorInfo']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "SENDDATA_ERRORINFO_ENABLE_INVALID";
				return FALSE;
			}

			//Check NoReadInfo
			if(!isset($this->parameters['sendData']['noReadInfo'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_NOREADINFO_MISSING";
				return FALSE;
			}
			
			//Check NoReadInfo Enable
			if(!isset($this->parameters['sendData']['noReadInfo']['enable'])){
				$this->errorCode = CommandBase::MISSINGPARAMETER;
				$this->errorDetail = "SENDDATA_NOREADINFO_ENABLE_MISSING";
				return FALSE;
			}
			if(filter_var($this->parameters['sendData']['noReadInfo']['enable'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) === NULL){
				$this->errorCode = CommandBase::INVALIDPARAMETERVALUE;
				$this->errorDetail = "SENDDATA_NOREADINFO_ENABLE_INVALID";
				return FALSE;
			}

			//Save Config
			$configPath = $_SERVER['DOCUMENT_ROOT'] . '/fruplus/others/fruappconfig.xml';
			$xml = new SimpleXMLElement($configPath, null ,true);
			$sendData = $xml->autoMode->sendData;
			$sendData->tagInfo->format = $this->parameters['sendData']['tagInfo']['format'];
			if($this->parameters['sendData']['tagInfo']['format'] === "part"){
				$sendData->tagInfo->offset = $this->parameters['sendData']['tagInfo']['offset'];
				$sendData->tagInfo->length = $this->parameters['sendData']['tagInfo']['length'];
			}
			$sendData->addInfo->id = var_export($this->parameters['sendData']['addInfo']['id'],true);
			$sendData->addInfo->time = var_export($this->parameters['sendData']['addInfo']['time'],true);
			$sendData->addInfo->antenna = var_export($this->parameters['sendData']['addInfo']['antenna'],true);
			$sendData->addInfo->pc = var_export($this->parameters['sendData']['addInfo']['pc'],true);
			$sendData->addInfo->rssi = var_export($this->parameters['sendData']['addInfo']['rssi'],true);
			$sendData->addInfo->state = var_export($this->parameters['sendData']['addInfo']['state'],true);
			$sendData->addInfo->memory = var_export($this->parameters['sendData']['addInfo']['memory'],true);
			if($this->parameters['sendData']['addInfo']['memory']){
				$sendData->addInfo->accessInfo->password = $this->parameters['sendData']['addInfo']['accessInfo']['password'];
				$sendData->addInfo->accessInfo->bank = $this->parameters['sendData']['addInfo']['accessInfo']['bank'];
				$sendData->addInfo->accessInfo->offset = $this->parameters['sendData']['addInfo']['accessInfo']['offset'];
				$sendData->addInfo->accessInfo->length = $this->parameters['sendData']['addInfo']['accessInfo']['length'];
			}
			$sendData->errorInfo->enable = var_export($this->parameters['sendData']['errorInfo']['enable'],true);
			$sendData->noReadInfo->enable = var_export($this->parameters['sendData']['noReadInfo']['enable'],true);
			
			$xml->asXml($configPath);

			return TRUE;
		}

		public function getResults(){
			return null;
		}
	}
?>

<?php
$responseStatus='HTTP/1.1 200 OK';
$serverRequest='';
if($_SERVER["REQUEST_METHOD"] == 'GET'){
	$serverRequest =  urldecode($_SERVER["QUERY_STRING"]);
}else if($_SERVER["REQUEST_METHOD"] == 'POST'){
	$serverRequest =  file_get_contents("php://input");
}

include_once($_SERVER['DOCUMENT_ROOT'] . '/class/LocalBridge.php');
$localBridge = new LocalBridge("s");
try{
	if(!$localBridge->connect()){
		throw new Exception('RWとの接続に失敗しました');
	}

	if(!$localBridge->send($serverRequest)){
		throw new Exception('コマンド送信に失敗しました');
	}
}catch(Exception $e){
	$responseStatus = 'HTTP/1.1 500 Internal Server Error';
}

header("$responseStatus");
header("X-Content-Type-Options: nosniff");
?>

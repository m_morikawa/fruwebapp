<?php
class RwConfigParameter{
	private $dat = null;
	public function __construct()
	{
		$file = fopen('./others/rwcfg.dat', "rb");
		$binary = fread($file, filesize('./others/rwcfg.dat'));
		fclose($file);
		$this->dat = unpack("C*",$binary);
	}

	function __destruct()
	{
       unset($xml);
  }

  public function dat(){
  	return $this->dat;
  }

  public function save($dat){
  	call_user_func(function(){
		$permission = substr(sprintf('%o', fileperms('./others/rwcfg.dat')), -4);
		$cmd = "sudo chmod %s ./others/rwcfg.dat";
		if (!strstr($permission, '777')) {
			exec(sprintf($cmd, '777'), $output);
		}
		});
		$file = fopen('./others/rwcfg.dat', "wb");
		$binary = call_user_func_array("pack", array_merge(array("C*"), $dat));
		$ret = TRUE;
		if(!fwrite($file,$binary)) {
			$ret  = FALSE;
		}
		fclose($file);
		return $ret;
  }
}
?>

<?php
class TransferConfig{
	private $xml = null;
	public function __construct()
	{
		$this->xml = new SimpleXMLElement('./others/auto_transfer.xml',null,true);
	}
	function __destruct() 
	{
       unset($xml);
    }
    public function xml(){
    	return $this->xml;
    }
    public function save(){
    	call_user_func(function(){
			$permission = substr(sprintf('%o', fileperms('./others/auto_transfer.xml')), -4);
			$cmd = "sudo chmod %s ./others/auto_transfer.xml";
			if (!strstr($permission, '777')) {
				exec(sprintf($cmd, '777'), $output);
			}
		});
		$this->xml->asXml('./others/auto_transfer.xml');
    }	
}
?>

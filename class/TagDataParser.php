<?php
	class TagDataParser{
		public static function parse($tagData){
			$taginfo = array();
			$len;
			$ant;
			$tsp;
			$rep;
			$rssi;
			$crc;
			$pc;
			$epc;

			$count = 1;
			while(strlen($tagData) > 0){
				$len = hexdec(substr($tagData,0,2));
				$ant = substr($tagData,2,2);
				$tsp = substr($tagData,4,8);
				$rep = substr($tagData,12,4);
				$rssi = substr($tagData,16,4);
				$crc = substr($tagData,20,4);
				$pc = substr($tagData,24,4);
				$epc = substr($tagData,28,($len*2) - 28);

				$taginfo['tag'.$count] = array('ant'=>$ant,'tsp'=>$tsp,'rep'=>$rep,'rssi'=>$rssi,'crc'=>$crc,'pc'=>$pc,'epc'=>$epc);

				$tagData = substr($tagData,($len*2),strlen($tagData) - ($len*2));
				$count++;
			}
			return $taginfo;
		}
	}
?>

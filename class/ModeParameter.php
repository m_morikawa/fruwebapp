<?php
class ModeParameter{
	private $xml = null;
	private $path = './others/fruappconfig.xml';
	public function __construct()
	{
		$this->xml = new SimpleXMLElement($this->path, null ,true);
	}

	function __destruct() 
	{
       unset($this->xml);
	}
	
    public function xml(){
    	return $this->xml;
	}
	
    public function save(){
    	call_user_func(function(){
			$permission = substr(sprintf('%o', fileperms($this->path)), -4);
			$cmd = "sudo chmod %s " . $this->path;
			if (!strstr($permission, '777')) {
				exec(sprintf($cmd, '777'), $output);
			}
		});
		$this->xml->asXml($this->path);
    }	
}
?>

<?php
class LocalBridge{
	private $resource = null;
	private $ftokKey = "m";
	private $buffersize = 4096;

	function __construct($ftokKey){
		$this->ftokKey = $ftokKey;
	}	

	function __destruct() 
	{
		if(!is_null($this->resource)){
			if(msg_remove_queue($this->resource) < 0){
				echo "fail to msg_remove_queue";
			}
			$this->resource = null;
		}
	}
	
	public function connect()
	{
		static $CONSOLE_QUEUE_ID;
		if(!isset($CONSOLE_QUEUE_ID)){
			$queueId = ftok("/etc/fruapp/bridgekey",$this->ftokKey);
			if($queueId < 0){
				echo "fail to ftok";
				return false;
			}
			$CONSOLE_QUEUE_ID = $queueId;
		}
		$this->resource =  msg_get_queue($CONSOLE_QUEUE_ID,0666);

		return true;
	}
	
	public function send($command){
		if(is_null($this->resource)){
			return false;
		}
		if(!msg_send($this->resource, 1, $command.PHP_EOL ,false)){
			echo "fail to msg_send";
			return false;
		}
			return true;
	}
	
	public function receive(&$response){
		if(is_null($this->resource)){
			return false;
		}
		$msgtype = null;
		$tmp_response = null;
		if(!msg_receive($this->resource, 2, $msgtype, $this->buffersize, &$tmp_response,false,MSG_NOERROR)){
			echo "fail to msg_receive";
			return false;
		}
		$ary_response = explode("\r\n", $tmp_response);
		if(count($ary_response) == 1){
			return false;
		}
		$response = substr($ary_response[0], 0, -2);
		return true;
	}
}
?>

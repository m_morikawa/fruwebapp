<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'ModeParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}

	$model = new Model();

	$config = new ModeParameter();
	$mode1conf = $config->mode1conf();
?>

<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>

<body>
<div id="wrapper">
	<div id="header">
		<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
		<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
	</div>

	<div id="contents">
  		<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::MODE1); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>オートモード設定 > モード1設定</strong></h2>
				<form method="post" action="senddat.php" class="forms">
					<fieldset>
						<legend><h5>重複チェック</h5></legend>
						<section>
							<label>重複チェック</label>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" name="multiepccheck_enabled" id="multiepccheck_enabled">
										<?php
											if ( $mode1conf->multiepccheck->enabled == "true"){
												echo "<option value=true selected>重複チェックを行う</option><option value=false>重複チェックを行わない</option>";
											} else if ( $mode1conf->multiepccheck->enabled == "false"){
												echo "<option value=true>重複チェックを行う</option><option value=false selected>重複チェックを行わない</option>";
											}
										?>
									</select>
								</column>
							</row>
							<script>
								function onMultiEpcCheckEnabledChanged(){
									if(this.options[this.selectedIndex].value == "true"){
										document.getElementById("multiepccheckconfig").style.display="block";
									}else if(this.options[this.selectedIndex].value == "false"){
										document.getElementById("multiepccheckconfig").style.display="none";
									}
								}
								if(document.getElementById("multiepccheck_enabled").addEventListener){
									document.getElementById("multiepccheck_enabled").addEventListener("change", onMultiEpcCheckEnabledChanged, false);
								}else if(document.getElementById("multiepccheck_enabled").attachEvent){
									document.getElementById("multiepccheck_enabled").attachEvent("onchange", onMultiEpcCheckEnabledChanged);
								}else{
									document.getElementById("multiepccheck_enabled").onchange = onMultiEpcCheckEnabledChanged;
								}
							</script>
							<div id="multiepccheckconfig">
								<section>
									<h4>定期的に重複チェックをリセットすることで、上位への送信を繰り返し行います。</h4>
									<p>定期的な重複チェックリセットの設定をしてください。</p>
									<section>
										<label>定期リセット</label>
										<row>
											<column cols="6">
												<select class="select custom" class="width-10" name="multiepccheck_scanreset_byinterval_enabled" id="multiepccheck_scanreset_byinterval_enabled">
													<?php
														if ( $mode1conf->multiepccheck->scanreset->byinterval->enabled == "true"){
															echo "<option value=true selected>定期的なリセットを行う</option><option value=false>定期的なリセットを行わない</option>";
														} else if ( $mode1conf->multiepccheck->scanreset->byinterval->enabled == "false"){
															echo "<option value=true>定期的なリセットを行う</option><option value=false selected>定期的なリセットを行わない</option>";
														}
													?>
												</select>
											</column>
										</row>
										<script>
											function onScanResetByIntervalEnabledChanged(){
												if(this.options[this.selectedIndex].value == "true"){
													document.getElementById("scanresetbyintervalconfig").style.display="block";
												}else if(this.options[this.selectedIndex].value == "false"){
													document.getElementById("scanresetbyintervalconfig").style.display="none";
												}
											}
											if(document.getElementById("multiepccheck_scanreset_byinterval_enabled").addEventListener){
												document.getElementById("multiepccheck_scanreset_byinterval_enabled").addEventListener("change", onScanResetByIntervalEnabledChanged, false);
											}else if(document.getElementById("multiepccheck_scanreset_byinterval_enabled").attachEvent){
												document.getElementById("multiepccheck_scanreset_byinterval_enabled").attachEvent("onchange", onScanResetByIntervalEnabledChanged);
											}else{
												document.getElementById("multiepccheck_scanreset_byinterval_enabled").onchange = onScanResetByIntervalEnabledChanged;
											}
										</script>
									</section>
									<div id="scanresetbyintervalconfig">
										<section>
											<label>インターバル : ms</label>
											<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="multiepccheck_scanreset_byinterval_interval" value="{$mode1conf->multiepccheck->scanreset->byinterval->interval}" placeholder="{$realtimemode->multiepccheck->scanreset->byinterval->interval}">
EOF;
											?>
										</section>
										<?php
										if ( $mode1conf->multiepccheck->scanreset->byinterval->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("scanresetbyintervalconfig").style.display="block";
</script>
EOF;
										} else if ( $mode1conf->multiepccheck->scanreset->byinterval->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("scanresetbyintervalconfig").style.display="none";
</script>
EOF;
										}
										?>
									</div>
								</section>
								<section>
									<h4>外部入力を使用して重複チェックをリセットすることで、上位への送信を再度行います。</h4>
									<p>外部入力による重複チェックリセットの設定をしてください。</p>
									<section>
										<label>トリガリセット</label>
										<row>
											<column cols="6">
												<select class="select custom" class="width-10" name="multiepccheck_scanreset_bytrigger_enabled" id="multiepccheck_scanreset_bytrigger_enabled">
													<?php
														if ( $mode1conf->multiepccheck->scanreset->bytrigger->enabled == "true"){
															echo "<option value=true selected>トリガによるリセットを行う</option><option value=false>トリガによるリセットを行わない</option>";
														} else if ( $mode1conf->multiepccheck->scanreset->bytrigger->enabled == "false"){
															echo "<option value=true>トリガによるリセットを行う</option><option value=false selected>トリガによるリセットを行わない</option>";
														}
													?>
												</select>
											</column>
										</row>
										<script>
											function onScanResetByTriggerEnabledChanged(){
												if(this.options[this.selectedIndex].value == "true"){
													document.getElementById("scanresetbytriggerconfig").style.display="block";
												}else if(this.options[this.selectedIndex].value == "false"){
													document.getElementById("scanresetbytriggerconfig").style.display="none";
												}
											}
											if(document.getElementById("multiepccheck_scanreset_bytrigger_enabled").addEventListener){
												document.getElementById("multiepccheck_scanreset_bytrigger_enabled").addEventListener("change", onScanResetByTriggerEnabledChanged, false);
											}else if(document.getElementById("multiepccheck_scanreset_bytrigger_enabled").attachEvent){
												document.getElementById("multiepccheck_scanreset_bytrigger_enabled").attachEvent("onchange", onScanResetByTriggerEnabledChanged);
											}else{
												document.getElementById("multiepccheck_scanreset_bytrigger_enabled").onchange = onScanResetByTriggerEnabledChanged;
											}
										</script>
									</section>
									<div id="scanresetbytriggerconfig">
										<section>
											<label>ポート</label>
											<row>
												<column cols="6">
													<select class="select custom" class="width-10" name="multiepccheck_scanreset_bytrigger_port">
													<?php
														if ( $mode1conf->multiepccheck->scanreset->bytrigger->port == "0"){
															echo "<option value=0 selected>ポート0</option><option value=1>ポート1</option>";
														} else if ( $mode1conf->multiepccheck->scanreset->bytrigger->port == "1"){
															echo "<option value=0>ポート0</option><option value=1 selected>ポート1</option>";
														}
													?>
													</select>
												</column>
											</row>
										</section>
											<label>外部入力トリガ</label>
											<row>
												<column cols="6">
													<select class="select custom" class="width-10" name="multiepccheck_scanreset_bytrigger_trigger">
													<?php
														if ( $mode1conf->multiepccheck->scanreset->bytrigger->trigger == "up"){
															echo "<option value=up selected>立ち上がり</option><option value=down>立ち下がり</option>";
														} else if ( $mode1conf->multiepccheck->scanreset->bytrigger->trigger == "down"){
															echo "<option value=up>立ち上がり</option><option value=down selected>立ち下がり</option>";
														}
													?>
													</select>
												</column>
											</row>
										<section>
										</section>
										<?php
										if ( $mode1conf->multiepccheck->scanreset->bytrigger->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("scanresetbytriggerconfig").style.display="block";
</script>
EOF;
										} else if ( $mode1conf->multiepccheck->scanreset->bytrigger->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("scanresetbytriggerconfig").style.display="none";
</script>
EOF;
										}
										?>
									</div>
								</section>
								<?php
								if ( $mode1conf->multiepccheck->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("multiepccheckconfig").style.display="block";
</script>
EOF;
								} else if ($mode1conf->multiepccheck->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("multiepccheckconfig").style.display="none";
</script>
EOF;
								}
								?>
							</div>
						</section>
					</fieldset>
					<fieldset>
						<legend><h5>外部出力制御</h5></legend>
						<section>
							<label>外部出力制御</label>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" id="dout_enabled" name="dout_enabled">
										<?php
											if ( $mode1conf->dout->enabled == "true"){
												echo "<option value=true selected>外部出力を制御する</option><option value=false>外部出力を制御しない</option>";
											} else if ( $mode1conf->dout->enabled == "false"){
												echo "<option value=true>外部出力を制御する</option><option value=false selected>外部出力を制御しない</option>";
											}
										?>
									</select>
								</column>
							</row>
							<script>
								function onDoutEnabledChanged(){
									if(this.options[this.selectedIndex].value == "true"){
										document.getElementById("doutconfig").style.display="block";
									}else if(this.options[this.selectedIndex].value == "false"){
										document.getElementById("doutconfig").style.display="none";
									}
								}
								if(document.getElementById("dout_enabled").addEventListener){
									document.getElementById("dout_enabled").addEventListener("change", onDoutEnabledChanged, false);
								}else if(document.getElementById("dout_enabled").attachEvent){
									document.getElementById("dout_enabled").attachEvent("onchange", onDoutEnabledChanged);
								}else{
									document.getElementById("dout_enabled").onchange = onDoutEnabledChanged;
								}
							</script>
							<div id="doutconfig">
								<section>
									<label>ポート</label>
									<row>
										<column cols="6">
											<select class="select custom" class="width-10" name="dout_port">
											<?php
												if ( $mode1conf->dout->port == "0"){
													echo "<option value=0 selected>ポート0</option><option value=1>ポート1</option>";
												} else if ( $mode1conf->dout->port == "1"){
													echo "<option value=0>ポート0</option><option value=1 selected>ポート1</option>";
												}
											?>
											</select>
										</column>
									</row>
								</section>
								<section>
									<label>保持時間 : ms</label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="dout_downtime" value="{$mode1conf->dout->downtime}" placeholder="{$mode1conf->dout->downtime}">
EOF;
									?>
								</section>
								<?php
									if ( $mode1conf->dout->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("doutconfig").style.display="block";
</script>
EOF;
									} else if ( $mode1conf->dout->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("doutconfig").style.display="none";
</script>
EOF;
									}
								?>
							</div>
						</section>
					</fieldset>
					<fieldset>
						<legend><h5>サーバー通信</h5></legend>
						<section>
							<label>サーバーレスポンス</label>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" name="response_enabled" id="response_enabled">
									<?php
										if ( $mode1conf->response->enabled == "true"){
											echo "<option value=true selected>有効</option><option value=false>無効</option>";
										} else if ( $mode1conf->response->enabled == "false"){
											echo "<option value=true>有効</option><option value=false selected>無効</option>";
										}
									?>
									</select>
								</column>
							</row>
							<script>
								function onResponseEnabledChanged() {
									if(this.options[this.selectedIndex].value == "true"){
										document.getElementById("responseconfig").style.display="block";
									}else if(this.options[this.selectedIndex].value == "false"){
										document.getElementById("responseconfig").style.display="none";
									}
								}
								if(document.getElementById("response_enabled").addEventListener){
									document.getElementById("response_enabled").addEventListener("change", onResponseEnabledChanged, false);
								}else if(document.getElementById("response_enabled").attachEvent){
									document.getElementById("response_enabled").attachEvent("onchange", onResponseEnabledChanged);
								}else{
									document.getElementById("response_enabled").onchange = onResponseEnabledChanged;
								}
							</script>
							<div id="responseconfig">
								<section>
									<div style="padding-bottom:16px;">
										<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
										<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
											リーダーは上位へデータ送信後、タイムアウト時間レスポンスを待ちます。<br>
											レスポンス待ちの間は、上位へのデータ送信は行われずそのデータは破棄されます。
										</div>
									</div>
									<label>タイムアウト時間 : ms</label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="response_timeout" value="{$mode1conf->response->timeout}" placeholder="{$mode1conf->response->timeout}">
EOF;
									?>
								</section>
								<?php
									if ( $mode1conf->response->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("responseconfig").style.display="block";
</script>
EOF;
									} else if ( $mode1conf->response->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("responseconfig").style.display="none";
</script>
EOF;
									}
								?>
							</div>
						</section>
						<section>
							<label>サーバーリクエスト</label>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" name="request_enabled" id="request_enabled">
									<?php
										if ( $mode1conf->request->enabled == "true"){
											echo "<option value=true selected>有効</option><option value=false>無効</option>";
										} else if ( $mode1conf->request->enabled == "false"){
											echo "<option value=true>有効</option><option value=false selected>無効</option>";
										}
									?>
									</select>
								</column>
							</row>
						</section>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
</div>
</body>


<?php
	require_once('function/load_translation.php');
	list($langFlag,$common_translation) = loadTranslation('common');
	list($langFlag,$translation) = loadTranslation('senddat');

	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'ModeParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}

	$model = new Model();
	$config = new ModeParameter();

	$isUpdateFormSeen = 0;
	$isErrorLabelSeen = 0;
	$errorMessage = "";

	if(isset($_POST['filter_submit'])){
		

		$msg = array();
		if(!validateFilter($msg)){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			$filter->pc[0]->enabled[0] = $_POST['pcfilter_enabled'];
			if($filter->pc[0]->enabled[0] == "true"){
				$filter->pc[0]->offset[0] = $_POST['pcfilter_offset'];
				$filter->pc[0]->length[0] = $_POST['pcfilter_length'];
				$filter->pc[0]->data[0] = $_POST['pcfilter_data'];
			}
			$filter->epc[0]->enabled[0] = $_POST['epcfilter_enabled'];
			if($filter->epc[0]->enabled[0] == "true"){
				$filter->epc[0]->offset[0] = $_POST['epcfilter_offset'];
				$filter->epc[0]->length[0] = $_POST['epcfilter_length'];
				$filter->epc[0]->data[0] = $_POST['epcfilter_data'];
			}
			$filter->rssi[0]->lower[0]->enabled[0] = $_POST['rssifilter_lower_enabled'];
			if($_POST['rssifilter_lower_enabled'] == "true"){
				$filter->rssi[0]->lower[0]->value[0] = $_POST['rssifilter_lower_value'];
			}
			$filter->rssi[0]->upper[0]->enabled[0] = $_POST['rssifilter_upper_enabled'];
			if($_POST['rssifilter_upper_enabled'] == "true"){
				$filter->rssi[0]->upper[0]->value[0] = $_POST['rssifilter_upper_value'];
			}
			$config->save();
			exec('sudo frucgi update mode',$res);	
			$status = json_decode($res[0],true);
			if(strpos($status['status'],'success') !== false){
				$alertstate['visible'] = true;
				$alertstate['state'] = true;
			}else{
				$alertstate['visible'] = true;
				$alertstate['state'] = false;
				$alertstate['msg']  = "フィルタ設定に失敗しました。";
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
	<div id="header">
		<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
		<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
	</div>

	<div id="contents">
  		<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::SENDDAT); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>オートモード設定 > 送信データ設定</strong></h2>
				<?php
					if($alertstate['visible']){
						if($alertstate['state']){
echo <<< EOF
<form method="post" action="index.php" class="forms" id="updateform">
<input type="hidden" name="modeupdateflag" value="update">
<div class="alert alert-primary">
<strong>フィルタ設定を変更しました。</strong> <br/>
この設定をリーダーに反映して、動作モードを再起動するためには、以下の<strong>設定を反映する</strong>ボタンをクリックして下さい。
<div style="padding-top:24px;">
<script>
function onCancelUpdate(){
document.getElementById("updateform").style.display = "none";
}
</script>
<button id="btn_do_update" class="width-4 primary"><strong>設定を反映する</strong></button>
<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()">今は反映しない</button>
<script>
function onUpdate(){
document.getElementById('btn_do_update').style.disabled = true;
document.getElementById('btn_do_update').style.background = "#4CAF50";
document.getElementById('btn_do_update').innerHTML = "設定を反映しました";
setTimeout(function(){
document.getElementById('updateform').submit();
return true;
}, 3000)
return false;
}
if(document.getElementById('btn_do_update').addEventListener){
document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
}else if(document.getElementById('btn_do_update').attachEvent){
document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
}else{
document.getElementById('btn_do_update').onclick = onUpdate;
}
</script>
</div>
</div>
</form>
EOF;
			}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
						}
					}
				?>
				<form method="post" action="senddat.php" class="forms">
					<fieldset>
						<legend><h5>タグ情報</h5></legend>
						<section>
							<label>EPCフォーマット</label>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" id="taginfo_format" name="taginfo_format">
										<?php
										if ( $senddat->taginfo->format == "all"){
											echo "<option value=all selected>EPCコードすべてを送信する</option><option value=part>EPCコードの一部を送信する</option>";
										} else if ( $senddat->taginfo->format == "part"){
											echo "<option value=all>EPCコードすべてを送信する</option><option value=part selected>EPCコードの一部を送信する</option>";
										}
										?>
									</select>
								</column>
							</row>
							<script>
								function onTaginfoFormatChanged(){
									if(this.options[this.selectedIndex].value == "all"){
										document.getElementById("tagformatconfig").style.display="none";
									}else if(this.options[this.selectedIndex].value == "part"){
										document.getElementById("tagformatconfig").style.display="block";
									}
								}
								if(document.getElementById("taginfo_format").addEventListener){
									document.getElementById("taginfo_format").addEventListener("change", onTaginfoFormatChanged, false);
								}else if(document.getElementById("taginfo_format").attachEvent){
									document.getElementById("taginfo_format").attachEvent("onchange", onTaginfoFormatChanged);
								}else{
									document.getElementById("taginfo_format").onchange = onTaginfoFormatChanged;
								}
							</script>
						</section>
						<div id="tagformatconfig">
							<section>
								<label>Offset : byte</label>
								<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="taginfo_offset" value="{$senddat->taginfo->offset}" placeholder="{$senddat->taginfo->offset}">

EOF;
								?>
							</section>
							<section>
								<label>Length : byte</label>
								<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="taginfo_length" value="{$senddat->taginfo->length}" placeholder="{$senddat->taginfo->length}">
EOF;
								?>
							</section>
							<?php
				if ( $senddat->taginfo->format == "all"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("tagformatconfig").style.display="none";
</script>
EOF;
				} else if ( $senddat->taginfo->format == "part"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("tagformatconfig").style.display="block";
</script>
EOF;
	}
							?>
						</div>
					</fieldset>
					<fieldset>
						<legend><h5>付加情報</h5></legend>
						<section>
							<row>
								<column cols="6">
									<section>
										<select class="select custom" class="width-10" name="addinfo_enabled" id="addinfo_enabled">
										<?php
											if ( $senddat->addinfo->enabled == "true"){
												echo "<option value=true selected>付加情報を送信する</option><option value=false>付加情報を送信しない</option>";
											} else if ( $senddat->addinfo->enabled == "false"){

												echo "<option value=true>付加情報を送信する</option><option value=false selected>付加情報を送信しない</option>";
											}
										?>
										</select>
									</section>
								</column>
							</row>
							<script>
								function onAddinfoEnabledChanged(){
									if(this.options[this.selectedIndex].value == "true"){
										document.getElementById("addinfoconfig").style.display="block";
									}else if(this.options[this.selectedIndex].value == "false"){
										document.getElementById("addinfoconfig").style.display="none";
									}
								}
								if(document.getElementById("addinfo_enabled").addEventListener){
									document.getElementById("addinfo_enabled").addEventListener("change", onAddinfoEnabledChanged, false);
								}else if(document.getElementById("addinfo_enabled").attachEvent){
									document.getElementById("addinfo_enabled").attachEvent("onchange", onAddinfoEnabledChanged);
								}else{
									document.getElementById("addinfo_enabled").onchange = onAddinfoEnabledChanged;
								}
							</script>
						</section>
						<div id="addinfoconfig">
							<row>
								<column cols="6">
									<section>
										<label>端末ID</label>
										<select class="select custom" class="width-10" name="addinfo_id">
										<?php
											if ( $senddat->addinfo->id == "true"){
												echo "<option value=true selected>付加する</option><option value=false>付加しない</option>";
											} else if ( $senddat->addinfo->id == "false"){
												echo "<option value=true>付加する</option><option value=false selected>付加しない</option>";
											}
										?>
										</select>
									</section>
									<section>
										<label>端末時刻</label>
										<select class="select custom" class="width-10" name="addinfo_time">
										<?php
											if ( $senddat->addinfo->time == "true"){
												echo "<option value=true selected>付加する</option><option value=false>付加しない</option>";
											} else if ( $senddat->addinfo->time == "false"){
												echo "<option value=true>付加する</option><option value=false selected>付加しない</option>";
											}
										?>
										</select>
									</section>
									<section>
										<label>アンテナポート</label>
										<select class="select custom" class="width-10" name="addinfo_antennano">
										<?php
											if ( $senddat->addinfo->antennano == "true"){
												echo "<option value=true selected>付加する</option><option value=false>付加しない</option>";
											} else if ( $senddat->addinfo->antennano == "false"){
												echo "<option value=true>付加する</option><option value=false selected>付加しない</option>";
											}
										?>
										</select>
									</section>
									<section>
										<label>PCコード</label>
										<select class="select custom" class="width-10" name="addinfo_pc">
										<?php
											if ( $senddat->addinfo->pc == "true"){
												echo "<option value=true selected>付加する</option><option value=false>付加しない</option>";
											} else if ( $senddat->addinfo->pc == "false"){
												echo "<option value=true>付加する</option><option value=false selected>付加しない</option>";
											}
										?>
										</select>
									</section>
									<section>
										<label>RSSI</label>
										<select class="select custom" class="width-10" name="addinfo_rssi">
										<?php
											if ( $senddat->addinfo->rssi == "true"){
												echo "<option value=true selected>付加する</option><option value=false>付加しない</option>";
											} else if ( $senddat->addinfo->rssi == "false"){
												echo "<option value=true>付加する</option><option value=false selected>付加しない</option>";
											}
										?>
										</select>
									</section>
									<section>
										<label>タグ状態</label>
										<div style="padding-bottom:16px;">
											<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
											<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
												タグ状態はモード3のみで有効です。
											</div>
										</div>
										<select class="select custom" class="width-10" name="addinfo_state">
										<?php
											if ( $senddat->addinfo->state == "true"){
												echo "<option value=true selected>付加する</option><option value=false>付加しない</option>";
											} else if ( $senddat->addinfo->state == "false"){
												echo "<option value=true>付加する</option><option value=false selected>付加しない</option>";
											}
										?>
										</select>
									</section>
									<section>
										<label>タグメモリ</label>
										<div style="padding-bottom:16px;">
											<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
											<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
												タグメモリはモード1、モード2で有効です。
											</div>
										</div>
										<select class="select custom" class="width-10" id="addinfo_memory_enabled" name="addinfo_memory_enabled">
										<?php
											if ( $senddat->addinfo->memory->enabled == "true"){
												echo "<option value=true selected>付加する</option><option value=false>付加しない</option>";
											} else if ( $senddat->addinfo->memory->enabled == "false"){
												echo "<option value=true>付加する</option><option value=false selected>付加しない</option>";
											}
										?>
										</select>
										<script>
											function onAddinfoMemoryEnabledChanged(){
												if(this.options[this.selectedIndex].value == "true"){
													document.getElementById("addmemoryconfig").style.display="block";
													// document.getElementsByName("antenna_0_dwell")[0].readOnly = true;
													// document.getElementsByName("antenna_0_dwell")[0].value="100";
													// document.getElementsByName("antenna_0_invcnt")[0].readOnly = true;
													// document.getElementsByName("antenna_0_invcnt")[0].value="2";
													// document.getElementsByName("antenna_1_dwell")[0].readOnly = true;
													// document.getElementsByName("antenna_1_dwell")[0].value="100";
													// document.getElementsByName("antenna_1_invcnt")[0].readOnly = true;
													// document.getElementsByName("antenna_1_invcnt")[0].value="2";
												}else if(this.options[this.selectedIndex].value == "false"){
													document.getElementById("addmemoryconfig").style.display="none";
													// document.getElementsByName("antenna_0_dwell")[0].readOnly = false;
													// document.getElementsByName("antenna_0_dwell")[0].value = <?php echo $inventorymode->antenna[0]->dwell; ?>;
													// document.getElementsByName("antenna_0_invcnt")[0].readOnly = false;
													// document.getElementsByName("antenna_0_invcnt")[0].value = <?php echo $inventorymode->antenna[0]->invcnt; ?>;
													// document.getElementsByName("antenna_1_dwell")[0].readOnly = false;
													// document.getElementsByName("antenna_1_dwell")[0].value = <?php echo $inventorymode->antenna[1]->dwell; ?>;
													// document.getElementsByName("antenna_1_invcnt")[0].readOnly = false;
													// document.getElementsByName("antenna_1_invcnt")[0].value = <?php echo $inventorymode->antenna[1]->invcnt; ?>;
												}
											}
											if(document.getElementById("addinfo_memory_enabled").addEventListener){
												document.getElementById("addinfo_memory_enabled").addEventListener("change", onAddinfoMemoryEnabledChanged, false);
											}else if(document.getElementById("addinfo_memory_enabled").attachEvent){
												document.getElementById("addinfo_memory_enabled").attachEvent("onchange", onAddinfoMemoryEnabledChanged);
											}else{
												document.getElementById("addinfo_memory_enabled").onchange = onAddinfoMemoryEnabledChanged;
											}
										</script>
									</section>
								</column>
							</row>
							<div id="addmemoryconfig">
								<p>タグメモリの読み込みに必要な情報を設定してしてください。</p>
								<div style="padding-bottom:16px;">
									<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
									<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
										タグメモリを有効にする場合、アンテナの<strong>Dwell,InvCnt</strong>は規定値に固定されます。
									</div>
								</div>
								<section>
									<label>Password</label>
									<?php
echo <<< EOF
<input type="text" class="width-6 custom" name="addinfo_memory_password" value="{$senddat->addinfo->memory->password}" placeholder="{$senddat->addinfo->memory->password}">
EOF;
									?>
								</section>
								<section>
									<label>バンク</label>
									<row>
										<column cols="6">
											<select class="select custom" class="width-10" name="addinfo_memory_bank">
											<?php
												if (  $senddat->addinfo->memory->bank == "reserved"){
													echo "<option value=reserved selected>Reservedバンク</option><option value=epc>EPCバンク</option><option value=tid>TIDバンク</option><option value=user>Userバンク</option>";
												} else if ( $senddat->addinfo->memory->bank == "epc"){
													echo "<option value=reserved>Reservedバンク</option><option value=epc selected>EPCバンク</option><option value=tid>TIDバンク</option><option value=user>Userバンク</option>";
												} else if ( $senddat->addinfo->memory->bank == "tid"){
													echo "<option value=reserved>Reservedバンク</option><option value=epc>EPCバンク</option><option value=tid selected>TIDバンク</option><option value=user>Userバンク</option>";
												} else if ( $senddat->addinfo->memory->bank == "user"){
													echo "<option value=reserved>Reservedバンク</option><option value=epc>EPCバンク</option><option value=tid>TIDバンク</option><option value=user selected>Userバンク</option>";
												}
											?>
											</select>
										</column>
									</row>
								</section>
								<section>
									<label>Offset : byte</label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="addinfo_memory_offset" value="{$senddat->addinfo->memory->offset}" placeholder="{$senddat->addinfo->memory->offset}">
EOF;
									?>
								</section>
								<section>
									<label>Length : byte</label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="addinfo_memory_length" value="{$senddat->addinfo->memory->length}" placeholder="{$senddat->addinfo->memory->length}">
EOF;
									?>
								</section>
								<?php
									if ( $senddat->addinfo->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("addinfoconfig").style.display="block";
</script>
EOF;
									} else if ( $senddat->addinfo->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("addinfoconfig").style.display="none";
</script>
EOF;
									}
								?>
							</div>
						</div>
					</fieldset>
					<fieldset>
						<legend><h5>その他情報</h5></legend>
						<section>
							<label>エラー情報</label>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" name="errorinfo_enabled">
										<?php
										if ( $senddat->errorinfo->enabled == "true"){
											echo "<option value=true selected>エラー情報を送信する</option><option value=false>エラー情報を送信しない</option>";
										} else if ( $senddat->errorinfo->enabled == "false"){
											echo "<option value=true>エラー情報を送信する</option><option value=false selected>エラー情報を送信しない</option>";
										}
										?>
									</select>
								</column>
							</row>
						</section>
						<section>
							<label>ノーリード情報</label>
							<div style="padding-bottom:16px;">
								<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
								<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
									ノーリード情報はモード2のみで有効です。
								</div>
							</div>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" name="noreadinfo_enabled">
										<?php
										if ( $senddat->noreadinfo->enabled == "true"){
											echo "<option value=true selected>ノーリード情報を送信する</option><option value=false>ノーリード情報を送信しない</option>";
										} else if ( $senddat->noreadinfo->enabled == "false"){
											echo "<option value=true>ノーリード情報を送信する</option><option value=false selected>ノーリード情報を送信しない</option>";
										}
										?>
									</select>
								</column>
							</row>
						</section>
					</fieldset>
					<?php
						if ( $senddat->addinfo->memory->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("addmemoryconfig").style.display="block";

// document.getElementsByName("antenna_0_dwell")[0].readOnly = true;
// document.getElementsByName("antenna_0_dwell")[0].value="100";
// document.getElementsByName("antenna_0_invcnt")[0].readOnly = true;
// document.getElementsByName("antenna_0_invcnt")[0].value="2";

// document.getElementsByName("antenna_1_dwell")[0].readOnly = true;
// document.getElementsByName("antenna_1_dwell")[0].value="100";
// document.getElementsByName("antenna_1_invcnt")[0].readOnly = true;
// document.getElementsByName("antenna_1_invcnt")[0].value="2";
</script>
EOF;
						} else if ( $senddat->addinfo->memory->enabled == "false"){
echo <<< EOF
<script type="text/javascript">

 document.getElementById("addmemoryconfig").style.display="none";

// document.getElementsByName("antenna_0_dwell")[0].readOnly = false;
// document.getElementsByName("antenna_0_dwell")[0].value　=　{$antenna[0]->dwell};
// document.getElementsByName("antenna_0_invcnt")[0].readOnly = false;
// document.getElementsByName("antenna_0_invcnt")[0].value = {$antenna[0]->invcnt};

// document.getElementsByName("antenna_1_dwell")[0].readOnly = false;
// document.getElementsByName("antenna_1_dwell")[0].value = {$antenna[1]->dwell};
// document.getElementsByName("antenna_1_invcnt")[0].readOnly = false;
// document.getElementsByName("antenna_1_invcnt")[0].value = {$antenna[1]->invcnt};
</script>
EOF;
						}
					?>
					
					<section>
						<button class="primary" name="filter_submit">変更する</button>
						<button class="secondary" type="button" name="filter_toCurrent" onclick="onToCurrentClicked()">現在の設定に戻す</button>
						<button class="secondary" type="button" name="filter_toDefault" onclick="onToDefaultClicked()">デフォルトに戻す</button>
					</section>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	function onToCurrentClicked(){
	<?php
		if ( $filter->pc->enabled == "true"){
			echo "document.getElementsByName(\"pcfilter_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"pcfilterconfig\").style.display=\"block\";";
		} else if ( $filter->pc->enabled == "false"){
			echo "document.getElementsByName(\"pcfilter_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"pcfilterconfig\").style.display=\"none\";";
		}
		echo "document.getElementsByName(\"pcfilter_offset\")[0].value = \"".$filter->pc->offset."\";";
		echo "document.getElementsByName(\"pcfilter_length\")[0].value = \"".$filter->pc->length."\";";
		echo "document.getElementsByName(\"pcfilter_data\")[0].value = \"".$filter->pc->data."\";";
		if ( $filter->epc->enabled == "true"){
			echo "document.getElementsByName(\"epcfilter_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"epcfilterconfig\").style.display=\"block\";";
		} else if ( $filter->epc->enabled == "false"){
			echo "document.getElementsByName(\"epcfilter_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"epcfilterconfig\").style.display=\"none\";";
		}
		echo "document.getElementsByName(\"epcfilter_offset\")[0].value = \"".$filter->epc->offset."\";";
		echo "document.getElementsByName(\"epcfilter_length\")[0].value = \"".$filter->epc->length."\";";
		echo "document.getElementsByName(\"epcfilter_data\")[0].value = \"".$filter->epc->data."\";";
		if ( $filter->rssi->lower->enabled == "true"){
			echo "document.getElementsByName(\"rssifilter_lower_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"rssifilterlowerconfig\").style.display=\"block\";";
		} else if ( $filter->rssi->lower->enabled == "false"){
			echo "document.getElementsByName(\"rssifilter_lower_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"rssifilterlowerconfig\").style.display=\"none\";";
		}
		echo "document.getElementsByName(\"rssifilter_lower_value\")[0].value = \"".$filter->rssi->lower->value."\";";
		if ( $filter->rssi->upper->enabled == "true"){
			echo "document.getElementsByName(\"rssifilter_upper_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"rssifilterupperconfig\").style.display=\"block\";";
		} else if ( $filter->rssi->upper->enabled == "false"){
			echo "document.getElementsByName(\"rssifilter_upper_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"rssifilterupperconfig\").style.display=\"none\";";
		}
		echo "document.getElementsByName(\"rssifilter_upper_value\")[0].value = \"".$filter->rssi->upper->value."\";";
	?>
	}
	function onToDefaultClicked(){
		document.getElementsByName("pcfilter_enabled")[0].selectedIndex= "1";
		document.getElementById("pcfilterconfig").style.display="none";
		document.getElementsByName("pcfilter_offset")[0].value= "0";
		document.getElementsByName("pcfilter_length")[0].value= "0";
		document.getElementsByName("pcfilter_data")[0].value= "0000";
		document.getElementsByName("epcfilter_enabled")[0].selectedIndex= "1";
		document.getElementById("epcfilterconfig").style.display="none";
		document.getElementsByName("epcfilter_offset")[0].value= "0";
		document.getElementsByName("epcfilter_length")[0].value= "0";
		document.getElementsByName("epcfilter_data")[0].value= "00000000000000000000000000000000";
		document.getElementsByName("rssifilter_lower_enabled")[0].selectedIndex= "1";
		document.getElementById("rssifilterlowerconfig").style.display="none";
		document.getElementsByName("rssifilter_lower_value")[0].value= "-70";
		document.getElementsByName("rssifilter_upper_enabled")[0].selectedIndex= "1";
		document.getElementById("rssifilterupperconfig").style.display="none";
		document.getElementsByName("rssifilter_upper_value")[0].value= "-20";
	}
	function onShowLength(idn, str) {
	   document.getElementById(idn).innerHTML = str.length + "文字";
	}
	function onCountDownLength(idn, str, mnum) {
	   document.getElementById(idn).innerHTML = "あと" + (mnum - str.length) + "文字";
	}
</script>
</body>
</html>

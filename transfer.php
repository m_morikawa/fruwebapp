<?php
	require_once('function/load_translation.php');
	list($langFlag,$common_translation) = loadTranslation('common');
	list($langFlag,$translation) = loadTranslation('transfer');

	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'ModeParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}

	$model = new Model();
	$config = new ModeParameter();

	$isUpdateFormSeen = 0;
	$isErrorLabelSeen = 0;
	$errorMessage = "";

	if(isset($_POST['transfer_submit'])){
		$msg = array();
		$ret = FALSE;
		require_once("function/validate_transfer.php");
		$ret = validate_transfer($msg);
		if(!$ret){
			$isErrorLabelSeen = 1;
			foreach($msg as $value){
				$errorMessage .= $value;
				$errorMessage .= "<br/>";
			}
		}else{
			require_once("function/apply_transfer.php");
			apply_transfer();
			$config->save();
			$isUpdateFormSeen = 1;
		}
	}

	$transferinfo = json_decode(json_encode($config->xml()->automode->transferinfo),TRUE);
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<script src="js/vue.js"></script>
<script src="js/axios.min.js"></script>
<script src="js/promise-7.0.4.min.js"></script>

<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>

function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));

function onCancelUpdate(){
	document.getElementById("updateform").style.display = "none";
}

</script>
</head>
<body>

<div id="wrapper">
  <div id="header">
	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<span  style="float: right;">
		<p class="macaddress" id="devicemacaddress" style="display: inline-block;"></p>
		<select v-model="language" @change="fetchLanguage" class="select lang" class="width-10">
			<option v-for="language in languages" v-bind:value="language.value">
				{{language.name}}
			</option>
		</select>
	</span>
  </div>

  <div id="contents">
  	<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::TRANSFER); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong><?php echo TITLE; ?></strong></h2>
				<form method="post" action="" class="forms" id="updateform" v-if="isUpdateFormSeen">
					<div class="alert alert-primary">
						<strong><?php echo UPDATE_TITLE; ?></strong> <br/>
						<?php echo COMMON_UPDATE_MESSAGE; ?>
						<div style="padding-top:24px;">
							<button id="btn_do_update" type="button" class="width-4 primary"><strong><?php echo COMMON_MSG_RESTART; ?></strong></button>
							<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()"><?php echo COMMON_MSG_NOTRESTART; ?></button>
						</div>
					</div>
				</form>
				<div class="alert alert-error" v-if="isErrorLabelSeen" v-html="errorMessage">	
				</div>	
				<form method="post" action="transfer.php" class="forms">
					<fieldset>
						<legend><h5><?php echo HEADER_SENDTO; ?></h5></legend>
						<section>
							<label><?php echo ITEM_PROTOCOL; ?></label>
							<row>
								<column cols="6">
									<select v-model="protocol" @change="fetchProtocol" class="select custom" class="width-10" name="transfer_protocol" id="transfer_protocol">
										<option v-for="protocol in protocols" v-bind:value="protocol.name">
											{{protocol.name}}
										</option>
									</select>
								</column>
							</row>
							<div id="socketconfig" v-show="isSocketConfigSeen">
								<section>
									<label><?php echo ITEM_IPHOST; ?></label>
									<input type="text" class="width-6 custom" name="transfer_ipaddress" v-model="ipAddress" >
								</section>
								<section>
									<label><?php echo ITEM_PORT; ?></label>
									<input type="text" class="width-4 custom" name="transfer_tcpport" v-model="tcpPort">
								</section>
							</div>
							<div id="httpconfig" v-show="isHTTPConfigSeen">
								<section>
									<label><?php echo ITEM_METHOD; ?></label>
									<row>
										<column cols="6">
											<select v-model="httpMethod" class="select custom" class="width-10" name="transfer_httpmethod">
												<option v-for="method in methods" v-bind:value="method.name">
													{{method.name}}
												</option>
											</select>
										</column>
									</row>
								</section>
								<section>
									<label><?php echo ITEM_IPHOST; ?></label>
									<input type="text" class="width-6 custom" name="transfer_host" v-model="host" >
								</section>
								<section>
									<label><?php echo ITEM_PATH; ?></label>
									<input type="text" class="width-6 custom" name="transfer_path" v-model="path" >
								</section>
								<section>
									<label><?php echo ITEM_PORT; ?></label>
									<input type="text" class="width-6 custom" name="transfer_httpport" v-model="httpPort" >
								</section>
								<section>
									<label><?php echo ITEM_FORMAT; ?></label>
									<row>
										<column cols="6">
											<select v-model="format" class="select custom" class="width-10" name="transfer_format">
												<option v-for="format in formats" v-bind:value="format.name">
													{{format.name}}
												</option>
											</select>
										</column>
									</row>
								</section>
								<section>
									<label><?php echo ITEM_BASICUSER; ?></label>
									<input type="text" class="width-6 custom" name="transfer_authuser" v-model="authUser" >
								</section>
								<section>
									<label><?php echo ITEM_BASICPASS; ?></label>
									<input type="text" class="width-6 custom" name="transfer_authpass" v-model="authPass" >
								</section>
							</div>
						</section>
					</fieldset>
					<section>
						<button class="primary" name="transfer_submit"><?php echo COMMON_BTN_CHANGE; ?></button>
						<button class="secondary" type="button" name="transfer_toCurrent" v-on:click="toCurrent"><?php echo COMMON_BTN_TOCURRENT; ?></button>
						<button class="secondary" type="button" name="transfer_toDefault"><?php echo COMMON_BTN_TODEFAULT; ?></button>
					</section>
				</form>
			</div>
		</div>
  	</div>
</div>

<script>
var translation_config = JSON.parse('<?php echo json_encode($translation,true); ?>');
var common_translation_config = JSON.parse('<?php echo json_encode($common_translation,true); ?>');
var current_config = JSON.parse('<?php echo json_encode($transferinfo,true); ?>');
var v_header = new Vue({
	el: "#header",
	data: {
		languages:[
			{name:'Japanese',value:"jp"},
			{name:'English',value:"en"},
		],
		language:'<?php echo $langFlag; ?>'
	},
	methods: {
		fetchLanguage:function(){
			var rand = Math.floor( Math.random() * 9998 ) + 1 ;
			axios
			.get('./js/languageswitch.php?lang='+this.language+'&rand='+rand)
			.then(function (response){location.reload();})
			.catch(function (error) {
				if (error.response) {
					console.log(error.response.data);
					console.log(error.response.status);
					console.log(error.response.headers);
				}
			});
		},
	},
	created:function(){
		
	}
});
var v_main = new Vue({
	el: "#main",
	data: {
		protocols:[
			{name:'serial'},
			{name:'socket'},
			{name:'http'},
			{name:'https'},
		],
		methods:[
			{name:'post'},
			{name:'query'},
		],
		formats:[
			{name:'query'},
			{name:'json'},
		],
		protocol:current_config["protocol"],
		ipAddress:current_config["ipaddress"],
		tcpPort:current_config["tcpport"],
		httpMethod:current_config["httpmethod"],
		host:current_config["host"],
		path:current_config["path"],
		httpPort:current_config["httpport"],
		format:current_config["format"],
		authUser:current_config["authuser"],
		authPass:current_config["authpass"],
		isSocketConfigSeen:false,
		isHTTPConfigSeen:false,

		isUpdateFormSeen:<?php echo $isUpdateFormSeen; ?>,
		isErrorLabelSeen:<?php echo $isErrorLabelSeen; ?>,
		errorMessage:"<?php echo $errorMessage; ?>",
	},
	methods: {
		fetchProtocol:function(){
			this.isSocketConfigSeen = (this.protocol=="socket");
			this.isHTTPConfigSeen = (this.protocol=="http" || this.protocol=="https");
		},
		toCurrent:function(){
			this.protocol = current_config["protocol"];
			this.ipAddress = current_config["ipaddress"];
			this.tcpPort = current_config["tcpport"];
			this.httpMethod = current_config["httpmethod"];
			this.host = current_config["host"];
			this.path = current_config["path"];
			this.httpPort = current_config["httpport"];
			this.format = current_config["format"];
			this.authUser = current_config["authuser"];
			this.authPass = current_config["authpass"];
			this.fetchProtocol();
		},
		toDefault:function(){
			//TODO /etc/default/fruappconfig.xmlを参照する
		},
		
	},
	created:function(){
		this.fetchProtocol();
	},
	computed: {

	}
});

function onUpdate(){
	document.getElementById('btn_do_update').style.disabled = true;
	document.getElementById('btn_do_update').style.background = "#4CAF50";
	document.getElementById('btn_do_update').innerHTML = common_translation_config["COMMON_MSG_RESTARTED"];
	setTimeout(function(){
		document.getElementById('updateform').submit();
		return true;
	}, 3000)
	return false;
}
if(document.getElementById("btn_do_update") != null){
	if(document.getElementById('btn_do_update').addEventListener){
		document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
	}else if(document.getElementById('btn_do_update').attachEvent){
		document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
	}else{
		document.getElementById('btn_do_update').onclick = onUpdate;
	}
}
</script>
</body>
</html>

<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'ModeParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}

	$selectedmode = -1;
	exec('sudo frucgi mode',$mode);
	$state = json_decode($mode[0],true);
	if(strpos($state['state'], "Manualmode") !== FALSE){
		$selectedmode = 0;
	}else if(strpos($state['state'], "Mode1") !== FALSE){
		$selectedmode = 1;
	}else if(strpos($state['state'], "Mode2") !== FALSE){
		$selectedmode = 2;
	}else if(strpos($state['state'], "Mode3") !== FALSE){
		$selectedmode = 3;
	}
	if($selectedmode > -1){
		$_SESSION['currentmode'] =  $selectedmode;
	}

	$model = new Model();

	$config = new ModeParameter();
	$transferinfo = $config->transferinfo();
	$alertstate = array('visible' => false,'result'=> false,'msg' => '');

	if(isset($_POST['transfer_submit'])){
		function validateTransfer(&$msg){
			$ret = TRUE;
			if(!filter_has_var(INPUT_POST, 'transfer_protocol')){
				$msg[] = "プロトコルを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['transfer_protocol'] === "serial"){
			
			}else if($_POST['transfer_protocol'] === "socket"){
				if(!filter_has_var(INPUT_POST, 'transfer_ipaddress')){
					$msg[] = "IPアドレス/ホスト名を入力して下さい。";
					$ret = FALSE;
				}
				if(strlen($_POST['transfer_ipaddress']) > 256){
					$msg[] = "IPアドレス/ホスト名は256文字以内で入力して下さい。";
					$ret = FALSE;
				}
				if(!filter_has_var(INPUT_POST, 'transfer_tcpport')){
					$msg[] = "ポートを入力して下さい。";
					$ret = FALSE;
				}
				if($_POST['transfer_tcpport'] !== "0"){
					if(!filter_input(INPUT_POST, 'transfer_tcpport', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>65535)))){
						$msg[] = "ポートは0から65535の整数で入力して下さい。";
						$ret = FALSE;
					}
				}
			}else if($_POST['transfer_protocol'] === "http" || $_POST['transfer_protocol'] === "https"){
				if(!filter_has_var(INPUT_POST, 'transfer_httpmethod')){
					$msg[] = "メソッドを入力して下さい。";
					$ret = FALSE;
				}
				if($_POST['transfer_httpmethod'] !== "post" && $_POST['transfer_httpmethod'] !== "get"){
					$msg[] = "不正なメソッドが入力されています。";
					$ret = FALSE;
				}
				if(!filter_has_var(INPUT_POST, 'transfer_host')){
					$msg[] = "IPアドレス/ホスト名を入力して下さい。";
					$ret = FALSE;
				}
				if(strlen($_POST['transfer_host']) > 256){
					$msg[] = "IPアドレス/ホスト名は256文字以内で入力して下さい。";
					$ret = FALSE;
				}
				if(!filter_has_var(INPUT_POST, 'transfer_path')){
					$msg[] = "パスを入力して下さい。";
					$ret = FALSE;
				}
				if(strlen($_POST['transfer_path']) > 256){
					$msg[] = "IPアドレス/ホスト名は256文字以内で入力して下さい。";
					$ret = FALSE;
				}
				if(!"http://".filter_var($_POST['transfer_host'].$_POST['transfer_path'],FILTER_VALIDATE_URL)){
					$msg[] = "不正なホスト名、パスが入力されています。";
					$ret = FALSE;
				}
				if(!filter_has_var(INPUT_POST, 'transfer_httpport')){
					$msg[] = "ポートを入力して下さい。";
					$ret = FALSE;
				}
				if($_POST['transfer_httpport'] !== "0"){
					if(!filter_input(INPUT_POST, 'transfer_httpport', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>65535)))){
						$msg[] = "ポートは0から65535の整数で入力して下さい。";
						$ret = FALSE;
					}
				}
				if(!filter_has_var(INPUT_POST, 'transfer_format')){
					$msg[] = "フォーマットを入力して下さい。";
					$ret = FALSE;
				}
				if($_POST['transfer_format'] !== "query" && $_POST['transfer_format'] !== "json"){
					$msg[] = "不正なフォーマットが入力されています。";
					$ret = FALSE;
				}
				if(!filter_has_var(INPUT_POST, 'transfer_authuser')){
					$msg[] = "Basic認証ユーザ名を入力して下さい。";
					$ret = FALSE;
				}
				if(strlen($_POST['transfer_authuser']) > 32){
					$msg[] = "Basic認証ユーザ名は32文字以内で入力して下さい。";
					$ret = FALSE;
				}
				if(!filter_has_var(INPUT_POST, 'transfer_authpass')){
					$msg[] = "Basic認証パスワードを入力して下さい。";
					$ret = FALSE;
				}
				if(strlen($_POST['transfer_authpass']) > 32){
					$msg[] = "Basic認証パスワードは32文字以内で入力して下さい。";
					$ret = FALSE;
				}
			}else{
				$msg[] = "不正なプロトコルが入力されています。";
				$ret = FALSE;
			}
			return $ret;
		}

		$msg = array();
		if(!validateTransfer($msg)){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			$transferinfo->protocol[0] = $_POST['transfer_protocol'];
			if($transferinfo->protocol[0] == 'serial'){
				
			}else if($transferinfo->protocol[0] == 'socket'){
				$transferinfo->ipaddress[0] = $_POST['transfer_ipaddress'];
				$transferinfo->tcpport[0] = $_POST['transfer_tcpport'];
			}else if($transferinfo->protocol[0] == 'http' || $transferinfo->protocol[0] == 'https'){
				$transferinfo->httpmethod[0] = $_POST['transfer_httpmethod'];
				$transferinfo->host[0] = $_POST['transfer_host'];
				$transferinfo->path[0] = $_POST['transfer_path'];
				$transferinfo->httpport[0] = $_POST['transfer_httpport'];
				$transferinfo->format[0] = $_POST['transfer_format'];
				$transferinfo->authuser[0] = $_POST['transfer_authuser'];
				$transferinfo->authpass[0] = $_POST['transfer_authpass'];
			}
			$config->save();
			exec('sudo frucgi update mode',$res);	
			$status = json_decode($res[0],true);
			if(strpos($status['status'],'success') !== false){
				$alertstate['visible'] = true;
				$alertstate['state'] = true;
			}else{
				$alertstate['visible'] = true;
				$alertstate['state'] = false;
				$alertstate['msg']  = "送信先設定に失敗しました。";
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
  <div id="header">
	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  </div>

  <div id="contents">
  	<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::TRANSFER); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>オートモード設定 > 送信先設定</strong></h2>
				<?php
					if($alertstate['visible']){
						if($alertstate['state']){
echo <<< EOF
<form method="post" action="index.php" class="forms" id="updateform">
<input type="hidden" name="modeupdateflag" value="update">
<div class="alert alert-primary">
<strong>送信先設定を変更しました。</strong> <br/>
この設定をリーダーに反映して、動作モードを再起動するためには、以下の<strong>設定を反映する</strong>ボタンをクリックして下さい。
<div style="padding-top:24px;">
<script>
function onCancelUpdate(){
document.getElementById("updateform").style.display = "none";
}
</script>
<button id="btn_do_update" class="width-4 primary"><strong>設定を反映する</strong></button>
<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()">今は反映しない</button>
<script>
function onUpdate(){
document.getElementById('btn_do_update').style.disabled = true;
document.getElementById('btn_do_update').style.background = "#4CAF50";
document.getElementById('btn_do_update').innerHTML = "設定を反映しました";
setTimeout(function(){
document.getElementById('updateform').submit();
return true;
}, 3000)
return false;
}
if(document.getElementById('btn_do_update').addEventListener){
document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
}else if(document.getElementById('btn_do_update').attachEvent){
document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
}else{
document.getElementById('btn_do_update').onclick = onUpdate;
}
</script>
</div>
</div>
</form>
EOF;
			}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
						}
					}
				?>
				<form method="post" action="transfer.php" class="forms">
					<fieldset>
						<legend><h5>データ送信先</h5></legend>

						<section>
							<label>プロトコル</label>
							<row>
								<column cols="6">
									<select class="select custom" class="width-10" name="transfer_protocol" id="transfer_protocol">
										<?php
										if ( $transferinfo->protocol == "serial"){
											echo "<option value=serial selected>Serial</option><option value=socket>Socket</option><option value=http>HTTP</option><option value=https>HTTPS</option>";
										} else if ( $transferinfo->protocol == "socket"){
											echo "<option value=serial>Serial</option><option value=socket selected>Socket</option><option value=http>HTTP</option><option value=https>HTTPS</option>";
										} else if ( $transferinfo->protocol == "http"){
											echo "<option value=serial>Serial</option><option value=socket>Socket</option><option value=http selected>HTTP</option><option value=https>HTTPS</option>";
										} else if( $transferinfo->protocol == "https"){
											echo "<option value=serial>Serial</option><option value=socket>Socket</option><option value=http>HTTP</option><option value=https selected>HTTPS</option>";
										}
										?>
									</select>
								</column>
							</row>
							<script>
								function onTransferProtocolChanged(){
									var select = document.getElementById('transfer_protocol');
									var options = document.getElementById('transfer_protocol').options;

									if(options.item(select.selectedIndex).value == "serial"){
										document.getElementById("socketconfig").style.display="none";
										document.getElementById("httpconfig").style.display="none";
									}else if(options.item(select.selectedIndex).value == "socket"){
										document.getElementById("socketconfig").style.display="block";
										document.getElementById("httpconfig").style.display="none";
									}else if(options.item(select.selectedIndex).value == "http"){
										document.getElementById("socketconfig").style.display="none";
										document.getElementById("httpconfig").style.display="block";
										document.getElementById("hostheader").innerHTML="(http:// 以下)";
									}else if(options.item(select.selectedIndex).value == "https"){
										document.getElementById("socketconfig").style.display="none";
										document.getElementById("httpconfig").style.display="block";
										document.getElementById("hostheader").innerHTML = "(https:// 以下)";
									}
								}
								if(document.getElementById("transfer_protocol").addEventListener){
									document.getElementById("transfer_protocol").addEventListener("change", onTransferProtocolChanged);
								}else if(document.getElementById("transfer_protocol").attachEvent){
									document.getElementById("transfer_protocol").attachEvent("onchange", onTransferProtocolChanged);
								}else{
									document.getElementById("transfer_protocol").onchange = onTransferProtocolChanged;
								}
							</script>
						</section>
						<div id="socketconfig">
							<section>
								<label>IPアドレス/ホスト名</label>
								<?php
echo <<< EOF
<input type="text" class="width-6 custom" name="transfer_ipaddress" value="{$transferinfo->ipaddress}" placeholder="{$transferinfo->ipaddress}">
EOF;
								?>
							</section>
							<section>
								<label>ポート</label>
								<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="transfer_tcpport" value="{$transferinfo->tcpport}" placeholder="{$transferinfo->tcpport}">
EOF;
								?>
							</section>
						</div>
						<div id="httpconfig">
							<section>
								<label>メソッド</label>
								<row>
									<column cols="6">
										<select class="select custom" class="width-10" name="transfer_httpmethod">
											<?php
											if ( $transferinfo->httpmethod == "post"){
												echo "<option value=post selected>POST</option><option value=get>GET</option>";
											} else if ( $transferinfo->httpmethod == "get"){
												echo "<option value=post>POST</option><option value=get selected>GET</option>";
											}
											?>
										</select>
									</column>
								</row>
							</section>
							<section>
								<label>IPアドレス/ホスト名</label><p id="hostheader" style="display:inline;"></p>
								<?php
echo <<< EOF
<input type="text" class="width-6 custom" name="transfer_host" value="{$transferinfo->host}" placeholder="{$transferinfo->host}">
EOF;
								?>
							</section>
							<section>
								<label>パス名</label>
								<?php
echo <<< EOF
<input type="text" class="width-6 custom" name="transfer_path" value="{$transferinfo->path}" placeholder="{$transferinfo->path}">
EOF;
								?>
							</section>
							<section>
								<label>ポート</label>
								<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="transfer_httpport" value="{$transferinfo->httpport}" placeholder="{$transferinfo->httpport}">
EOF;
								?>
							</section>
							<section>
								<label>フォーマット</label>
								<row>
									<column cols="6">
										<select class="select custom" class="width-10" name="transfer_format">
											<?php
											if ( $transferinfo->format == "query"){
												echo "<option value=query selected>Query</option><option value=json>JSON</option>";
											} else if ( $transferinfo->format == "json"){
												echo "<option value=query>Query</option><option value=json selected>JSON</option>";
											}
											?>
										</select>
									</column>
								</row>
							</section>
							<section>
								<label>Basic認証ユーザ名</label>
								<?php
echo <<< EOF
<input type="text" class="width-6 custom" name="transfer_authuser" value="{$transferinfo->authuser}" placeholder="{$transferinfo->authuser}">
EOF;
								?>
							</section>
							<section>
								<label>Basic認証パスワード</label>
								<?php
echo <<< EOF
<input type="text" class="width-6 custom" name="transfer_authpass" value="{$transferinfo->authpass}" placeholder="{$transferinfo->authpass}">
EOF;
								?>
							</section>
						</div>
						<?php
						if ( $transferinfo->protocol == "serial"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("socketconfig").style.display="none";
document.getElementById("httpconfig").style.display="none";
</script>
EOF;
						} else if ( $transferinfo->protocol == "socket"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("socketconfig").style.display="block";
document.getElementById("httpconfig").style.display="none";
</script>
EOF;
						} else if ( $transferinfo->protocol == "http" || $transferinfo->protocol == "https"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("socketconfig").style.display="none";
document.getElementById("httpconfig").style.display="block";
</script>
EOF;
						}
						if($transferinfo->protocol == "http"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("hostheader").innerHTML="(http:// 以下)";
</script>
EOF;
						}else if($transferinfo->protocol == "https"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("hostheader").innerHTML="(https:// 以下)";
</script>
EOF;
						}
						?>
					</fieldset>
					<section>
						<button class="primary" name="transfer_submit">変更する</button>
						<button class="secondary" type="button" name="transfer_toCurrent" onclick="onToCurrentClicked()">現在の設定に戻す</button>
						<button class="secondary" type="button" name="transfer_toDefault" onclick="onToDefaultClicked()">デフォルトに戻す</button>
					</section>
				</form>
	    		</div>
		</div>
  	</div>
</div>
<script>
	function onToCurrentClicked(){
	<?php
		if( $transferinfo->protocol == "serial"){
			echo "document.getElementsByName(\"transfer_protocol\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"socketconfig\").style.display=\"none\";";
			echo "document.getElementById(\"httpconfig\").style.display=\"none\";";
		} else if ( $transferinfo->protocol == "socket"){
			echo "document.getElementsByName(\"transfer_protocol\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"socketconfig\").style.display=\"block\";";
			echo "document.getElementById(\"httpconfig\").style.display=\"none\";";
		} else if ( $transferinfo->protocol == "http"){
			echo "document.getElementsByName(\"transfer_protocol\")[0].selectedIndex= \"2\";";
			echo "document.getElementById(\"socketconfig\").style.display=\"none\";";
			echo "document.getElementById(\"httpconfig\").style.display=\"block\";";
			echo "document.getElementById(\"hostheader\").innerHTML=\"(http:// 以下)\";";
		} else if( $transferinfo->protocol == "https"){
			echo "document.getElementsByName(\"transfer_protocol\")[0].selectedIndex= \"3\";";
			echo "document.getElementById(\"socketconfig\").style.display=\"none\";";
			echo "document.getElementById(\"httpconfig\").style.display=\"block\";";
			echo "document.getElementById(\"hostheader\").innerHTML=\"(https:// 以下)\";";
		}
		echo "document.getElementsByName(\"transfer_ipaddress\")[0].value = \"".$transferinfo->ipaddress."\";";
		echo "document.getElementsByName(\"transfer_tcpport\")[0].value = \"".$transferinfo->tcpport."\";";
		if ( $transferinfo->httpmethod == "post"){
			echo "document.getElementsByName(\"transfer_httpmethod\")[0].selectedIndex= \"0\";";
		} else if ( $transferinfo->httpmethod == "get"){
			echo "document.getElementsByName(\"transfer_httpmethod\")[0].selectedIndex= \"1\";";
		}
		echo "document.getElementsByName(\"transfer_host\")[0].value = \"".$transferinfo->host."\";";
		echo "document.getElementsByName(\"transfer_path\")[0].value = \"".$transferinfo->path."\";";
		echo "document.getElementsByName(\"transfer_httpport\")[0].value = \"".$transferinfo->httpport."\";";
		if ( $transferinfo->format == "query"){
			echo "document.getElementsByName(\"transfer_format\")[0].selectedIndex= \"0\";";
		} else if ( $transferinfo->format == "json"){
			echo "document.getElementsByName(\"transfer_format\")[0].selectedIndex= \"1\";";
		}
		echo "document.getElementsByName(\"transfer_authuser\")[0].value = \"".$transferinfo->authuser."\";";
		echo "document.getElementsByName(\"transfer_authpass\")[0].value = \"".$transferinfo->authpass."\";";
	?>
	}
	function onToDefaultClicked(){
		document.getElementsByName("transfer_protocol")[0].selectedIndex= "1";
		document.getElementById("socketconfig").style.display="block";
		document.getElementById("httpconfig").style.display="none";
		document.getElementsByName("transfer_ipaddress")[0].value="192.168.209.100";
		document.getElementsByName("transfer_tcpport")[0].value="50001";
		document.getElementsByName("transfer_httpmethod")[0].selectedIndex= "0";
		document.getElementsByName("transfer_host")[0].value="";
		document.getElementsByName("transfer_path")[0].value="";
		document.getElementsByName("transfer_httpport")[0].value="80";
		document.getElementsByName("transfer_format")[0].selectedIndex= "0";
		document.getElementsByName("transfer_authuser")[0].value="";
		document.getElementsByName("transfer_authpass")[0].value="";
	}
</script>
</body>
</html>

<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'ModeParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}
	
	$selectedmode = -1;
	exec('sudo frucgi mode',$mode);
	$state = json_decode($mode[0],true);
	if(strpos($state['state'], "Manualmode") !== FALSE){
		$selectedmode = 0;
	}else if(strpos($state['state'], "Mode1") !== FALSE){
		$selectedmode = 1;
	}else if(strpos($state['state'], "Mode2") !== FALSE){
		$selectedmode = 2;
	}else if(strpos($state['state'], "Mode3") !== FALSE){
		$selectedmode = 3;
	}
	if($selectedmode > -1){
		$_SESSION['currentmode'] =  $selectedmode;
	}

	$model = new Model();

	$config = new ModeParameter();
	$rfidset= $config->rfidset();
	$alertstate = array('visible' => false,'result'=> false,'msg' => '');

	if(isset($_POST['filter_submit'])){
		function validateFilter(&$msg){
			$ret = TRUE;
			if(!filter_has_var(INPUT_POST, 'pcfilter_enabled')){
				$msg[] = "PCフィルタの状態を入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['pcfilter_enabled'] !== "true" && $_POST['pcfilter_enabled'] !== "false"){
				$msg[] = "不正なPCフィルタの状態が入力されています。";
				$ret = FALSE;
			}
			if($_POST['pcfilter_enabled'] === "true"){
				if(!filter_has_var(INPUT_POST, 'pcfilter_offset')){
					$msg[] = "PCフィルタのOffsetを入力して下さい。";
					$ret = FALSE;
				}
				if($_POST['pcfilter_offset'] !== "0"){
					if(!filter_input(INPUT_POST, 'pcfilter_offset', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>15)))){
						$msg[] = "PCフィルタのOffsetは0から15の整数で入力して下さい。";
						$ret = FALSE;
					}
				}
				if(!filter_has_var(INPUT_POST, 'pcfilter_length')){
					$msg[] = "PCフィルタのLengthを入力して下さい。";
					$ret = FALSE;
				}
				if(!filter_input(INPUT_POST, 'pcfilter_length', FILTER_VALIDATE_INT, array('options' => array('min_range'=>1, 'max_range'=>16)))){
					$msg[] = "PCフィルタのLengthは1から15の整数で入力して下さい。";
					$ret = FALSE;
				}
				if(!filter_has_var(INPUT_POST, 'pcfilter_data')){
					$msg[] = "PCフィルタのDataを入力して下さい。";
					$ret = FALSE;
				}
				if(!ctype_xdigit($_POST['pcfilter_data'])){
					$msg[] = "PCフィルタのDataは16進数で入力して下さい。";
					$ret = FALSE;
				}
				if(strlen($_POST['pcfilter_data']) != 4){
					$msg[] = "PCフィルタは2byteを入力して下さい。";
					$ret = FALSE;
				}
				if($ret){
					exec('sudo frucgi validate pcmask '.$_POST['pcfilter_offset'].' '.$_POST['pcfilter_length'].' '.$_POST['pcfilter_data'],$res);
					$validate = json_decode($res[0],true);
					if(strpos($validate["status"],'success') === false){
						$msg[] = "PCフィルタのフォーマットが不正です。";
						$ret = FALSE;
					}
				}
			}
			if(!filter_has_var(INPUT_POST, 'epcfilter_enabled')){
				$msg[] = "EPCフィルタの状態を入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['epcfilter_enabled'] !== "true" && $_POST['epcfilter_enabled'] !== "false"){
				$msg[] = "不正なEPCフィルタの状態が入力されています。";
				$ret = FALSE;
			}
			if($_POST['epcfilter_enabled'] === "true"){
				if(!filter_has_var(INPUT_POST, 'epcfilter_offset')){
					$msg[] = "EPCフィルタのOffsetを入力して下さい。";
					$ret = FALSE;
				}
				if($_POST['epcfilter_offset'] !== "0"){
					if(!filter_input(INPUT_POST, 'epcfilter_offset', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>127)))){
						$msg[] = "EPCフィルタのOffsetは0から127の整数で入力して下さい。";
						$ret = FALSE;
					}
				}
				if(!filter_has_var(INPUT_POST, 'epcfilter_length')){
					$msg[] = "EPCフィルタのLengthを入力して下さい。";
					$ret = FALSE;
				}
				if(!filter_input(INPUT_POST, 'epcfilter_length', FILTER_VALIDATE_INT, array('options' => array('min_range'=>1, 'max_range'=>128)))){
					$msg[] = "EPCフィルタのLengthは1から128の整数で入力して下さい。";
					$ret = FALSE;
				}
				if(!filter_has_var(INPUT_POST, 'epcfilter_data')){
					$msg[] = "EPCフィルタのDataを入力して下さい。";
					$ret = FALSE;
				}
				if(!ctype_xdigit($_POST['epcfilter_data'])){
					$msg[] = "EPCフィルタのDataは16進数で入力して下さい。";
					$ret = FALSE;
				}
				if(strlen($_POST['epcfilter_data']) != 32){
					$msg[] = "EPCフィルタは16byteを入力して下さい。";
					$ret = FALSE;
				}
				if($ret){
					unset($res);
					exec('sudo frucgi validate epcmask '.$_POST['epcfilter_offset'].' '.$_POST['epcfilter_length'].' '.$_POST['epcfilter_data'],$res);
					$validate = json_decode($res[0],true);
					if(strpos($validate["status"],'success') === false){
						$msg[] = "EPCフィルタのフォーマットが不正です。";
						$ret = FALSE;
					}
				}
			}
			if(!filter_has_var(INPUT_POST, 'rssifilter_lower_enabled')){
				$msg[] = "RSSIフィルタの下限の状態を入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['rssifilter_lower_enabled'] !== "true" && $_POST['rssifilter_lower_enabled'] !== "false"){
				$msg[] = "不正なRSSIフィルタの下限の状態が入力されています。";
				$ret = FALSE;
			}
			if($_POST['rssifilter_lower_enabled'] === "true"){
				if(!filter_has_var(INPUT_POST, 'rssifilter_lower_value')){
					$msg[] = "RSSIフィルタの下限値を入力して下さい。";
					$ret = FALSE;
				}
				if($_POST['rssifilter_lower_value']  !== '0'){
					if(!filter_input(INPUT_POST, 'rssifilter_lower_value', FILTER_VALIDATE_INT, array('options' => array('min_range'=>-100, 'max_range'=>0)))){
						$msg[] = "RSSIフィルタの下限値は-100から0の整数で入力して下さい。";
						$ret = FALSE;
					}
				}
			}
			if(!filter_has_var(INPUT_POST, 'rssifilter_upper_enabled')){
				$msg[] = "RSSIフィルタの上限の状態を入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['rssifilter_upper_enabled'] !== "true" && $_POST['rssifilter_upper_enabled'] !== "false"){
				$msg[] = "不正なRSSIフィルタの上限の状態が入力されています。";
				$ret = FALSE;
			}
			if($_POST['rssifilter_upper_enabled'] === "true"){
				if(!filter_has_var(INPUT_POST, 'rssifilter_upper_value')){
					$msg[] = "RSSIフィルタの上限値を入力して下さい。";
					$ret = FALSE;
				}
				if($_POST['rssifilter_upper_value']  !== '0'){
					if(!filter_input(INPUT_POST, 'rssifilter_upper_value', FILTER_VALIDATE_INT, array('options' => array('min_range'=>-100, 'max_range'=>0)))){
						$msg[] = "RSSIフィルタの上限値は-100から0の整数で入力して下さい。";
						$ret = FALSE;
					}
				}
			}
			if($_POST['rssifilter_lower_enabled'] === "true" && $_POST['rssifilter_upper_enabled'] === "true"){
				if($_POST['rssifilter_upper_value'] < $_POST['rssifilter_lower_value']){
					$msg[] = "RSSIフィルタの上限値は下限値より大きい値を入力して下さい。";
					$ret = FALSE;
				}
			}
			return $ret;
		}

		$msg = array();
		if(!validateFilter($msg)){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			$filter->pc[0]->enabled[0] = $_POST['pcfilter_enabled'];
			if($filter->pc[0]->enabled[0] == "true"){
				$filter->pc[0]->offset[0] = $_POST['pcfilter_offset'];
				$filter->pc[0]->length[0] = $_POST['pcfilter_length'];
				$filter->pc[0]->data[0] = $_POST['pcfilter_data'];
			}
			$filter->epc[0]->enabled[0] = $_POST['epcfilter_enabled'];
			if($filter->epc[0]->enabled[0] == "true"){
				$filter->epc[0]->offset[0] = $_POST['epcfilter_offset'];
				$filter->epc[0]->length[0] = $_POST['epcfilter_length'];
				$filter->epc[0]->data[0] = $_POST['epcfilter_data'];
			}
			$filter->rssi[0]->lower[0]->enabled[0] = $_POST['rssifilter_lower_enabled'];
			if($_POST['rssifilter_lower_enabled'] == "true"){
				$filter->rssi[0]->lower[0]->value[0] = $_POST['rssifilter_lower_value'];
			}
			$filter->rssi[0]->upper[0]->enabled[0] = $_POST['rssifilter_upper_enabled'];
			if($_POST['rssifilter_upper_enabled'] == "true"){
				$filter->rssi[0]->upper[0]->value[0] = $_POST['rssifilter_upper_value'];
			}
			$config->save();
			exec('sudo frucgi update mode',$res);	
			$status = json_decode($res[0],true);
			if(strpos($status['status'],'success') !== false){
				$alertstate['visible'] = true;
				$alertstate['state'] = true;
			}else{
				$alertstate['visible'] = true;
				$alertstate['state'] = false;
				$alertstate['msg']  = "フィルタ設定に失敗しました。";
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
	<div id="header">
		<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
		<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
	</div>

	<div id="contents">
  		<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::RFIDSET); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>オートモード設定 > RFID設定</strong></h2>
				<?php
					if($alertstate['visible']){
						if($alertstate['state']){
echo <<< EOF
<form method="post" action="index.php" class="forms" id="updateform">
<input type="hidden" name="modeupdateflag" value="update">
<div class="alert alert-primary">
<strong>フィルタ設定を変更しました。</strong> <br/>
この設定をリーダーに反映して、動作モードを再起動するためには、以下の<strong>設定を反映する</strong>ボタンをクリックして下さい。
<div style="padding-top:24px;">
<script>
function onCancelUpdate(){
document.getElementById("updateform").style.display = "none";
}
</script>
<button id="btn_do_update" class="width-4 primary"><strong>設定を反映する</strong></button>
<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()">今は反映しない</button>
<script>
function onUpdate(){
document.getElementById('btn_do_update').style.disabled = true;
document.getElementById('btn_do_update').style.background = "#4CAF50";
document.getElementById('btn_do_update').innerHTML = "設定を反映しました";
setTimeout(function(){
document.getElementById('updateform').submit();
return true;
}, 3000)
return false;
}
if(document.getElementById('btn_do_update').addEventListener){
document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
}else if(document.getElementById('btn_do_update').attachEvent){
document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
}else{
document.getElementById('btn_do_update').onclick = onUpdate;
}
</script>
</div>
</div>
</form>
EOF;
			}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
						}
					}
				?>
				<form method="post" action="rfidset.php" class="forms">
					<fieldset>
						<legend><h5>アンテナ設定</h5></legend>
						<div class="configcontainer">
							<section>
								<label><strong>アンテナ0 使用</strong></label>
								<row>
									<column cols="6">
										<select class="select custom" class="width-10" name="antenna_0_enabled" id="antenna_0_enabled">
											<?php
												if ( $rfidset->antenna[0]->enabled == "true"){
													echo "<option value=true selected>アンテナ0を使用する</option><option value=false>アンテナ0を使用しない</option>";
												} else if ( $rfidset->antenna[0]->enabled == "false"){
													echo "<option value=true>アンテナ0を使用する</option><option value=false selected>アンテナ0を使用しない</option>";
												}
											?>
										</select>
										<script>
											function onAntenna0EnabledChanged(){
												if(this.options[this.selectedIndex].value == "true"){
													document.getElementById("antenna0config").style.display="block";
												}else if(this.options[this.selectedIndex].value == "false"){
													document.getElementById("antenna0config").style.display="none";
												}
											}
											if(document.getElementById("antenna_0_enabled").addEventListener){
												document.getElementById("antenna_0_enabled").addEventListener("change", onAntenna0EnabledChanged, false);
											}else if(document.getElementById("antenna_0_enabled").attachEvent){
												document.getElementById("antenna_0_enabled").attachEvent("onchange", onAntenna0EnabledChanged);
											}else{
												document.getElementById("antenna_0_enabled").onchange = onAntenna0EnabledChanged;
											}
										</script>
									</column>
								</row>
							</section>
							<div id="antenna0config">
								<section>
									<label><strong>アンテナ0 Power</strong></label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_0_power" value="{$rfidset->antenna[0]->power}" placeholder="{$rfidset->antenna[0]->power}">
EOF;
									?>
								</section>
								<section>
									<label><strong>アンテナ0 Dwell</strong></label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_0_dwell" value="{$rfidset->antenna[0]->dwell}" placeholder="{$rfidset->antenna[0]->dwell}">
EOF;
									?>
								</section>
								<section>
									<label><strong>アンテナ0 InvCnt</strong></label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_0_invcnt" value="{$rfidset->antenna[0]->invcnt}" placeholder="{$rfidset->antenna[0]->invcnt}">
EOF;
									?>
								</section>
								<?php
									if ( $rfidset->antenna[0]->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("antenna0config").style.display="block";
</script>
EOF;
									} else if ( $rfidset->antenna[0]->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("antenna0config").style.display="none";
</script>
EOF;
									}
								?>
							</div>
						</div>
						<div class="configcontainer">
							<section>
								<label><strong>アンテナ1 使用</strong></label>
								<row>
									<column cols="6">
										<select class="select custom" class="width-10" name="antenna_1_enabled" id="antenna_1_enabled">
											<?php
												if ( $rfidset->antenna[1]->enabled == "true"){
													echo "<option value=true selected>アンテナ1を使用する</option><option value=false>アンテナ1を使用しない</option>";
												} else if ( $rfidset->antenna[1]->enabled == "false"){
													echo "<option value=true>アンテナ1を使用する</option><option value=false selected>アンテナ1を使用しない</option>";
												}
											?>
										</select>
										<script>
											function onAntenna1EnabledChanged(){
												if(this.options[this.selectedIndex].value == "true"){
													document.getElementById("antenna1config").style.display="block";
												}else if(this.options[this.selectedIndex].value == "false"){
													document.getElementById("antenna1config").style.display="none";
												}
											}
											if(document.getElementById("antenna_1_enabled").addEventListener){
												document.getElementById("antenna_1_enabled").addEventListener("change", onAntenna1EnabledChanged, false);
											}else if(document.getElementById("antenna_1_enabled").attachEvent){
												document.getElementById("antenna_1_enabled").attachEvent("onchange", onAntenna1EnabledChanged);
											}else{
												document.getElementById("antenna_1_enabled").onchange = onAntenna1EnabledChanged;
											}
										</script>
									</column>
								</row>
							</section>
							<div id="antenna1config">
								<section>
									<label><strong>アンテナ1 Power</strong></label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_1_power" value="{$rfidset->antenna[1]->power}" placeholder="{$rfidset->antenna[1]->power}">
EOF;
									?>
								</section>
								<section>
									<label><strong>アンテナ1 Dwell</strong></label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_1_dwell" value="{$rfidset->antenna[1]->dwell}" placeholder="{$rfidset->antenna[1]->dwell}">
EOF;
									?>
								</section>
								<section>
									<label><strong>アンテナ1 InvCnt</strong></label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_1_invcnt" value="{$rfidset->antenna[1]->invcnt}" placeholder="{$rfidset->antenna[1]->invcnt}">
EOF;
									?>
								</section>
								<?php
									if ( $rfidset->antenna[1]->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("antenna1config").style.display="block";
</script>
EOF;
									} else if ( $rfidset->antenna[1]->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("antenna1config").style.display="none";
</script>
EOF;
									}
								?>
							</div>
						</div>
						<div class="configcontainer">
							<section>
								<label><strong>アンテナ2 使用</strong></label>
								<row>
									<column cols="6">
										<select class="select custom" class="width-10" name="antenna_2_enabled" id="antenna_2_enabled">
											<?php
												if ( $rfidset->antenna[2]->enabled == "true"){
													echo "<option value=true selected>アンテナ2を使用する</option><option value=false>アンテナ2を使用しない</option>";
												} else if ( $rfidset->antenna[2]->enabled == "false"){
													echo "<option value=true>アンテナ2を使用する</option><option value=false selected>アンテナ2を使用しない</option>";
												}
											?>
										</select>
										<script>
											function onAntenna2EnabledChanged(){
												if(this.options[this.selectedIndex].value == "true"){
													document.getElementById("antenna2config").style.display="block";
												}else if(this.options[this.selectedIndex].value == "false"){
													document.getElementById("antenna2config").style.display="none";
												}
											}
											if(document.getElementById("antenna_2_enabled").addEventListener){
												document.getElementById("antenna_2_enabled").addEventListener("change", onAntenna2EnabledChanged, false);
											}else if(document.getElementById("antenna_2_enabled").attachEvent){
												document.getElementById("antenna_2_enabled").attachEvent("onchange", onAntenna2EnabledChanged);
											}else{
												document.getElementById("antenna_2_enabled").onchange = onAntenna2EnabledChanged;
											}
										</script>
									</column>
								</row>
							</section>
							<div id="antenna2config">
								<section>
									<label><strong>アンテナ2 Power</strong></label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_2_power" value="{$rfidset->antenna[2]->power}" placeholder="{$rfidset->antenna[2]->power}">
EOF;
									?>
								</section>
								<section>
									<label><strong>アンテナ2 Dwell</strong></label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_2_dwell" value="{$rfidset->antenna[2]->dwell}" placeholder="{$rfidset->antenna[2]->dwell}">
EOF;
									?>
								</section>
								<section>
									<label><strong>アンテナ2 InvCnt</strong></label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_2_invcnt" value="{$rfidset->antenna[2]->invcnt}" placeholder="{$rfidset->antenna[2]->invcnt}">
EOF;
									?>
								</section>
								<?php
									if ( $rfidset->antenna[2]->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("antenna2config").style.display="block";
</script>
EOF;
									} else if ( $rfidset->antenna[2]->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("antenna2config").style.display="none";
</script>
EOF;
									}
								?>
							</div>
						</div>
						<div class="configcontainer">
							<section>
								<label><strong>アンテナ3 使用</strong></label>
								<row>
									<column cols="6">
										<select class="select custom" class="width-10" name="antenna_3_enabled" id="antenna_3_enabled">
											<?php
												if ( $rfidset->antenna[3]->enabled == "true"){
													echo "<option value=true selected>アンテナ3を使用する</option><option value=false>アンテナ3を使用しない</option>";
												} else if ( $rfidset->antenna[3]->enabled == "false"){
													echo "<option value=true>アンテナ3を使用する</option><option value=false selected>アンテナ3を使用しない</option>";
												}
											?>
										</select>
										<script>
											function onAntenna3EnabledChanged(){
												if(this.options[this.selectedIndex].value == "true"){
													document.getElementById("antenna3config").style.display="block";
												}else if(this.options[this.selectedIndex].value == "false"){
													document.getElementById("antenna3config").style.display="none";
												}
											}
											if(document.getElementById("antenna_3_enabled").addEventListener){
												document.getElementById("antenna_3_enabled").addEventListener("change", onAntenna3EnabledChanged, false);
											}else if(document.getElementById("antenna_3_enabled").attachEvent){
												document.getElementById("antenna_3_enabled").attachEvent("onchange", onAntenna3EnabledChanged);
											}else{
												document.getElementById("antenna_3_enabled").onchange = onAntenna3EnabledChanged;
											}
										</script>
									</column>
								</row>
							</section>
							<div id="antenna3config">
								<section>
									<label><strong>アンテナ3 Power</strong></label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_3_power" value="{$rfidset->antenna[3]->power}" placeholder="{$rfidset->antenna[3]->power}">
EOF;
									?>
								</section>
								<section>
									<label><strong>アンテナ3 Dwell</strong></label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_3_dwell" value="{$rfidset->antenna[3]->dwell}" placeholder="{$rfidset->antenna[3]->dwell}">
EOF;
									?>
								</section>
								<section>
									<label><strong>アンテナ3 InvCnt</strong></label>
									<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_3_invcnt" value="{$rfidset->antenna[3]->invcnt}" placeholder="{$rfidset->antenna[3]->invcnt}">
EOF;
									?>
								</section>
								<?php
									if ( $rfidset->antenna[3]->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("antenna3config").style.display="block";
</script>
EOF;
									} else if ( $rfidset->antenna[3]->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("antenna3config").style.display="none";
</script>
EOF;
									}
								?>
							</div>
						</div>			
					</fieldset>
					<fieldset>
						<legend><h5>周波数設定</h5></legend>
						<div>
							<section>
								<label>周波数</label>
								<row>
									<column cols="6">
										<select class="select custom" class="width-10" name="frequency_ch" id="frequency_ch">
											<?php
											$options = $model->channelStrings();
											foreach ($options as $key=>$value) {
											$selected = "";
											if ($key == $rfidset->frequency) {
											$selected = " selected";
											}
											echo "<option value=\"$key\"$selected>$value</option>\n";
											}
											?>
										</select>
									</column>
								</row>
							</section>
						</div>
					</fieldset>
					<section>
						<button class="primary" name="filter_submit">変更する</button>
						<button class="secondary" type="button" name="filter_toCurrent" onclick="onToCurrentClicked()">現在の設定に戻す</button>
						<button class="secondary" type="button" name="filter_toDefault" onclick="onToDefaultClicked()">デフォルトに戻す</button>
					</section>
				</form>
			</div>
		</div>
	</div>
</div>
<script>
	function onToCurrentClicked(){
	<?php
		if ( $filter->pc->enabled == "true"){
			echo "document.getElementsByName(\"pcfilter_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"pcfilterconfig\").style.display=\"block\";";
		} else if ( $filter->pc->enabled == "false"){
			echo "document.getElementsByName(\"pcfilter_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"pcfilterconfig\").style.display=\"none\";";
		}
		echo "document.getElementsByName(\"pcfilter_offset\")[0].value = \"".$filter->pc->offset."\";";
		echo "document.getElementsByName(\"pcfilter_length\")[0].value = \"".$filter->pc->length."\";";
		echo "document.getElementsByName(\"pcfilter_data\")[0].value = \"".$filter->pc->data."\";";
		if ( $filter->epc->enabled == "true"){
			echo "document.getElementsByName(\"epcfilter_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"epcfilterconfig\").style.display=\"block\";";
		} else if ( $filter->epc->enabled == "false"){
			echo "document.getElementsByName(\"epcfilter_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"epcfilterconfig\").style.display=\"none\";";
		}
		echo "document.getElementsByName(\"epcfilter_offset\")[0].value = \"".$filter->epc->offset."\";";
		echo "document.getElementsByName(\"epcfilter_length\")[0].value = \"".$filter->epc->length."\";";
		echo "document.getElementsByName(\"epcfilter_data\")[0].value = \"".$filter->epc->data."\";";
		if ( $filter->rssi->lower->enabled == "true"){
			echo "document.getElementsByName(\"rssifilter_lower_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"rssifilterlowerconfig\").style.display=\"block\";";
		} else if ( $filter->rssi->lower->enabled == "false"){
			echo "document.getElementsByName(\"rssifilter_lower_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"rssifilterlowerconfig\").style.display=\"none\";";
		}
		echo "document.getElementsByName(\"rssifilter_lower_value\")[0].value = \"".$filter->rssi->lower->value."\";";
		if ( $filter->rssi->upper->enabled == "true"){
			echo "document.getElementsByName(\"rssifilter_upper_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"rssifilterupperconfig\").style.display=\"block\";";
		} else if ( $filter->rssi->upper->enabled == "false"){
			echo "document.getElementsByName(\"rssifilter_upper_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"rssifilterupperconfig\").style.display=\"none\";";
		}
		echo "document.getElementsByName(\"rssifilter_upper_value\")[0].value = \"".$filter->rssi->upper->value."\";";
	?>
	}
	function onToDefaultClicked(){
		document.getElementsByName("pcfilter_enabled")[0].selectedIndex= "1";
		document.getElementById("pcfilterconfig").style.display="none";
		document.getElementsByName("pcfilter_offset")[0].value= "0";
		document.getElementsByName("pcfilter_length")[0].value= "0";
		document.getElementsByName("pcfilter_data")[0].value= "0000";
		document.getElementsByName("epcfilter_enabled")[0].selectedIndex= "1";
		document.getElementById("epcfilterconfig").style.display="none";
		document.getElementsByName("epcfilter_offset")[0].value= "0";
		document.getElementsByName("epcfilter_length")[0].value= "0";
		document.getElementsByName("epcfilter_data")[0].value= "00000000000000000000000000000000";
		document.getElementsByName("rssifilter_lower_enabled")[0].selectedIndex= "1";
		document.getElementById("rssifilterlowerconfig").style.display="none";
		document.getElementsByName("rssifilter_lower_value")[0].value= "-70";
		document.getElementsByName("rssifilter_upper_enabled")[0].selectedIndex= "1";
		document.getElementById("rssifilterupperconfig").style.display="none";
		document.getElementsByName("rssifilter_upper_value")[0].value= "-20";
	}
	function onShowLength(idn, str) {
	   document.getElementById(idn).innerHTML = str.length + "文字";
	}
	function onCountDownLength(idn, str, mnum) {
	   document.getElementById(idn).innerHTML = "あと" + (mnum - str.length) + "文字";
	}
</script>
</body>
</html>

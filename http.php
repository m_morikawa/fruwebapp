<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'RwConfigParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');
	$model = new Model();
	$rfpower = $model->rfPower();

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}

	$alertstate = array('visible' => false,'result'=> false,'msg' => '');
	$parameter = new RwConfigParameter();
	$dat = $parameter->dat();
	$model = new Model();

	if(isset($_POST['antenna_submit'])){
		function validateAntenna(&$msg){
			global $model;
			$ret = TRUE;
			if(!filter_has_var(INPUT_POST, 'antenna_0_enabled')){
				$msg[] = "アンテナ0の状態を入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['antenna_0_enabled'] !== "0" && $_POST['antenna_0_enabled'] !== "1"){
				$msg[] = "不正なアンテナ0の状態が入力されています。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'antenna_0_power')){
				$msg[] = "アンテナ0のPowerを入力して下さい。";
				$ret = FALSE;
			}
			$options = array('options' => $model->rfPowerRange());
			if (!filter_input(INPUT_POST, 'antenna_0_power', FILTER_VALIDATE_INT, $options)) {
				$msg[] = "アンテナ0のPowerは" . $options['options']['min_range'] . "から" . $options['options']['max_range'] ."の整数で入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'antenna_0_dwell')){
				$msg[] = "アンテナ0のDwellを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['antenna_0_dwell'] !== "0"){
				if(!filter_input(INPUT_POST, 'antenna_0_dwell', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>65535)))){
					$msg[] = "アンテナ0のDwellは0から65535の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'antenna_0_invcnt')){
				$msg[] = "アンテナ0のInvCntを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['antenna_0_invcnt'] !== "0"){
				if(!filter_input(INPUT_POST, 'antenna_0_invcnt', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>65535)))){
					$msg[] = "アンテナ0のInvCntは0から65535の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if($_POST['antenna_0_dwell'] === "0" && $_POST['antenna_0_invcnt'] === "0" ){
					$msg[] = "アンテナ0のDwellとInvCntはともに0にはできません。";
					$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'antenna_1_enabled')){
				$msg[] = "アンテナ1の状態を入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['antenna_1_enabled'] !== "0" && $_POST['antenna_1_enabled'] !== "1"){
				$msg[] = "不正なアンテナ1の状態が入力されています。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'antenna_1_power')){
				$msg[] = "アンテナ1のPowerを入力して下さい。";
				$ret = FALSE;
			}
			if (!filter_input(INPUT_POST, 'antenna_1_power', FILTER_VALIDATE_INT, $options)) {
				$msg[] = "アンテナ1のPowerは" . $options['options']['min_range'] . "から" . $options['options']['max_range'] ."の整数で入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'antenna_1_dwell')){
				$msg[] = "アンテナ1のDwellを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['antenna_1_dwell'] !== "0"){
				if(!filter_input(INPUT_POST, 'antenna_1_dwell', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>65535)))){
					$msg[] = "アンテナ1のDwellは0から65535の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if(!filter_has_var(INPUT_POST, 'antenna_1_invcnt')){
				$msg[] = "アンテナ1のInvCntを入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['antenna_1_invcnt'] !== "0"){
				if(!filter_input(INPUT_POST, 'antenna_1_invcnt', FILTER_VALIDATE_INT, array('options' => array('min_range'=>0, 'max_range'=>65535)))){
					$msg[] = "アンテナ1のInvCntは0から65535の整数で入力して下さい。";
					$ret = FALSE;
				}
			}
			if($_POST['antenna_1_dwell'] === "0" && $_POST['antenna_1_invcnt'] === "0" ){
					$msg[] = "アンテナ1のDwellとInvCntはともに0にはできません。";
					$ret = FALSE;
			}
			return $ret;
		}

		$msg = array();
		if(!validateAntenna($msg)){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			function valueToArray($value,&$first,&$second){
				$first = $value % 256;
				$second = ($value - $first) / 256;
			}
			$dat[0x28 + 1] = $_POST['antenna_0_enabled'];
			valueToArray($_POST['antenna_0_power'],$dat[0x2A + 1],$dat[0x2B + 1]);
			valueToArray($_POST['antenna_0_dwell'],$dat[0x2C + 1],$dat[0x2D + 1]);
			valueToArray($_POST['antenna_0_invcnt'],$dat[0x2E + 1],$dat[0x2F + 1]);
			$dat[0x32 + 1] = $_POST['antenna_1_enabled'];
			valueToArray($_POST['antenna_1_power'],$dat[0x34 + 1],$dat[0x35 + 1]);
			valueToArray($_POST['antenna_1_dwell'],$dat[0x36 + 1],$dat[0x37 + 1]);
			valueToArray($_POST['antenna_1_invcnt'],$dat[0x38 + 1],$dat[0x39 + 1]);
			if(!$parameter->save($dat)){
				$alertstate['visible'] = true;
				$alertstate['state'] = false;
				$alertstate['msg'] .= "アンテナ設定の保存に失敗しました。";
			}else{
				$alertstate['visible'] = true;
				$alertstate['state'] = true;
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
	<div id="header">
		<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
		<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  </div>

  <div id="contents">
  	<div id="contentsInner">
		<?php require_once('function/put_nav.php'); put_nav(NavState::HTTP); ?>
		<div id="main">
			<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>マニュアルモード設定 > HTTP設定</strong></h2>
			<?php
				if($alertstate['visible']){
					if($alertstate['state']){
echo <<< EOF
<div class="alert alert-primary" id="updateform">
<section>
<strong>アンテナ設定を変更しました。</strong><Br>
この設定をリーダーに反映するには、以下の手順が必要です。
<div style="max-width:60em;padding-top:1em;">
<ol style="list-style-type:decimal">
<li style="width:100%;"><strong>設定を反映する</strong>ボタンをクリックします。</li>
<li style="width:100%;">リーダーの電源をオフにします。</li>
<li style="width:100%;">リーダーの電源をオンします。</li>
<li style="width:100%;">リーダーのLED A(緑)とB(赤)が点滅後消灯し、LED A(緑)が点灯します。</li>
<li style="width:100%;">リーダーの電源をオフにします。</li>
<li style="width:100%;">リーダーの電源をオンします。</li>
</ol>
</div>
</section>

<section>
<div style="max-width:60em;padding-top:1em;">
<script>
function onCancelUpdate(){
document.getElementById("updateform").style.display = "none";
}
</script>
<button id="btn_do_update" class="width-4 primary"><strong>設定を反映する</strong></button>
<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()">今は反映しない</button>
<script>
function onUpdate(){
var elem = document.getElementById('update_antenna');
if(elem){
document.getElementsByTagName('body')[0].removeChild(elem);
}
var script = document.createElement('script');
script.src = './js/updaterwcfg.php';
script.id = 'update_antenna';
document.getElementsByTagName('body')[0].appendChild(script);
document.getElementById('btn_do_update').style.disabled = true;
document.getElementById('btn_do_update').className = "feedback";
document.getElementById('btn_do_update').innerHTML = "設定を反映しました";
setTimeout(function(){
document.getElementById('btn_do_update').style.disabled = false;
document.getElementById('btn_do_update').className = "primary";
document.getElementById('btn_do_update').innerHTML = "設定を反映する";
document.getElementById('btn_do_update').style.display="none";
document.getElementById('btn_cancel_update').style.display="none";
}, 3000)
}
if(document.getElementById('btn_do_update').addEventListener){
document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
}else if(document.getElementById('btn_do_update').attachEvent){
document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
}else{
document.getElementById('btn_do_update').onclick = onUpdate;
}
</script>
</div>
</section>

</div>
EOF;
		}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
					}
				}
			?>
			<form method="post" action="antenna.php" class="forms">
				<fieldset>
					<legend><h5>アンテナ0</h5></legend>
					<section>
			              <label>アンテナ0</label>
						<row>
							<column cols="6">
								<select class="select custom" name="antenna_0_enabled">
								<?php
									if ( $dat[0x28 + 1] == 0){
										echo "<option value=0 selected>無効</option><option value=1>有効</option>";
									} else if ( $dat[0x28 + 1] == 1){
										echo "<option value=0>無効</option><option value=1 selected>有効</option>";
									}
								?>
								</select>
							</column>
						</row>
					</section>
					<section>
						<label>Power</label>
						<?php
						$antenna0Power = hexdec(sprintf("%02X", $dat[0x2B + 1]).sprintf("%02X", $dat[0x2A + 1]));
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_0_power" value="{$antenna0Power}" placeholder="{$antenna0Power}">
EOF;
						?>
					</section>
					<section>
						<label>Dwell</label>
						<?php
						$antenna0Dwell = hexdec(sprintf("%02X", $dat[0x2D + 1]).sprintf("%02X", $dat[0x2C + 1]));
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_0_dwell" value="{$antenna0Dwell}" placeholder="{$antenna0Dwell}">
EOF;
						?>
					</section>
					<section>
						<label>InvCnt</label>
						<?php
						$antenna0InvCnt = hexdec(sprintf("%02X", $dat[0x2F + 1]).sprintf("%02X", $dat[0x2E + 1]));
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_0_invcnt" value="{$antenna0InvCnt}" placeholder="{$antenna0InvCnt}">
EOF;
						?>
					</section>
				</fieldset>
				<fieldset>
					<legend><h5>アンテナ1</h5></legend>
					<section>
			              <label>アンテナ1</label>
						<row>
							<column cols="6">
								<select class="select custom" class="width-10" name="antenna_1_enabled">
								<?php
									if ( $dat[0x32 + 1] == 0){
										echo "<option value=0 selected>無効</option><option value=1>有効</option>";
									} else if ( $dat[0x32 + 1] == 1){
										echo "<option value=0>無効</option><option value=1 selected>有効</option>";
									}
								?>
								</select>
							</column>
						</row>
					</section>
					<section>
						<label>Power</label>
						<?php
						$antenna1Power = hexdec(sprintf("%02X", $dat[0x35 + 1]).sprintf("%02X", $dat[0x34 + 1]));
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_1_power" value="{$antenna1Power}" placeholder="{$antenna1Power}">
EOF;
						?>
					</section>
					<section>
						<label>Dwell</label>
						<?php
						$antenna1Dwell = hexdec(sprintf("%02X", $dat[0x37 + 1]).sprintf("%02X", $dat[0x36 + 1]));
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_1_dwell" value="{$antenna1Dwell}" placeholder="{$antenna1Dwell}">
EOF;
						?>
					</section>
					<section>
						<label>InvCnt</label>
						<?php
						$antenna1InvCnt = hexdec(sprintf("%02X", $dat[0x39 + 1]).sprintf("%02X", $dat[0x38 + 1]));
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_1_invcnt" value="{$antenna1InvCnt}" placeholder="{$antenna1InvCnt}">
EOF;
						?>
					</section>
				</fieldset>
				<section>
					<button class="primary" name="antenna_submit">変更する</button>
					<button class="secondary" type="button" name="antenna_toCurrent" onclick="onToCurrentClicked()">現在の設定に戻す</button>
					<button class="secondary" type="button" name="antenna_toDefault" onclick="onToDefaultClicked()">デフォルトに戻す</button>
				</section>
			</form>
		</div>
  </div>
</div>
<script>
	var rfpower = <?php echo $rfpower; ?>;
	function onToCurrentClicked(){
	<?php
		if ( $dat[0x28 + 1] == 0){
			echo "document.getElementsByName(\"antenna_0_enabled\")[0].selectedIndex= \"0\";";
		} else if ( $dat[0x28 + 1] == 1){
			echo "document.getElementsByName(\"antenna_0_enabled\")[0].selectedIndex= \"1\";";
		}
		echo "document.getElementsByName(\"antenna_0_power\")[0].value = \"".$antenna0Power."\";";
		echo "document.getElementsByName(\"antenna_0_dwell\")[0].value = \"".$antenna0Dwell."\";";
		echo "document.getElementsByName(\"antenna_0_invcnt\")[0].value = \"".$antenna0InvCnt."\";";
		if ( $dat[0x32 + 1] == 0){
			echo "document.getElementsByName(\"antenna_1_enabled\")[0].selectedIndex= \"0\";";
		} else if ( $dat[0x32 + 1] == 1){
			echo "document.getElementsByName(\"antenna_1_enabled\")[0].selectedIndex= \"1\";";
		}
		echo "document.getElementsByName(\"antenna_1_power\")[0].value = \"".$antenna1Power."\";";
		echo "document.getElementsByName(\"antenna_1_dwell\")[0].value = \"".$antenna1Dwell."\";";
		echo "document.getElementsByName(\"antenna_1_invcnt\")[0].value = \"".$antenna1InvCnt."\";";
	?>
	}
	function onToDefaultClicked(){
		document.getElementsByName("antenna_0_enabled")[0].selectedIndex= "1";
		document.getElementsByName("antenna_0_power")[0].value = rfpower;
		document.getElementsByName("antenna_0_dwell")[0].value = "2000";
		document.getElementsByName("antenna_0_invcnt")[0].value = "8192";
		document.getElementsByName("antenna_1_enabled")[0].selectedIndex= "0";
		document.getElementsByName("antenna_1_power")[0].value = rfpower;
		document.getElementsByName("antenna_1_dwell")[0].value = "2000";
		document.getElementsByName("antenna_1_invcnt")[0].value = "8192";
	}
</script>
</body>
</html>

<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'SystemParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}
	$model = new Model();

	$parameter = new SystemParameter();
	$basicauth = array('user'=>'','password'=>'');

	function getBasicAuth(&$authpair){
		exec('sudo frucgi password get',$res);
		$userandpass = json_decode($res[0],true);
		if(isset($userandpass['user'])){
			$authpair['user'] = $userandpass['user'];
		}
		if(isset($userandpass['password'])){
			$authpair['password'] = $userandpass['password'];
		}
	}

	getBasicAuth($basicauth);

	if(isset($_POST['password_submit'])){
		function validatePassword($user,$pass,&$msg){
			$ret = TRUE;
			if(!filter_has_var(INPUT_POST, 'password_oldpassword')){
				$msg[] = "現在のパスワードを入力して下さい。";
				$ret = FALSE;
			}
			if(strlen($_POST['password_oldpassword']) > 8){
				$msg[] = "パスワードは8文字以内で入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'password_newpassword')){
				$msg[] = "新しいパスワードを入力して下さい。";
				$ret = FALSE;
			}
			if(strlen($_POST['password_newpassword']) > 8){
				$msg[] = "パスワードは8文字以内で入力して下さい。";
				$ret = FALSE;
			}
			if(!filter_has_var(INPUT_POST, 'password_confirmnewpassword')){
				$msg[] = "新しいパスワード(確認)を入力して下さい。";
				$ret = FALSE;
			}
			if(strlen($_POST['password_confirmnewpassword']) > 8){
				$msg[] = "パスワードは8文字以内で入力して下さい。";
				$ret = FALSE;
			}
			if($_POST['password_newpassword'] !== $_POST['password_confirmnewpassword']){
				$msg[] = "新しいパスワードが間違っています。";
				$ret = FALSE;
			}
			$crypted = crypt($_POST['password_oldpassword'], $pass);
			if($crypted !== $pass){
				$msg[] = "現在のパスワードが一致しません。";
				$ret = FALSE;
			}
			if($ret){
				exec("sudo frucgi password set mts ".$_POST['password_newpassword'],$out,$res);
				$state = json_decode($out[0],true);
				if($state == NULL){
					$state = json_decode($out[1],true);
				}
				if(strpos($state['status'], "success") !== FALSE){
					$ret = TRUE;
				}else{
					$msg[] = "パスワードの設定に失敗しました。";
					$ret = FALSE;
				}
			}
			return $ret;
		}

		$msg = array();
		if(!validatePassword($basicauth['user'],$basicauth['password'],$msg)){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			getBasicAuth($basicauth);
			$alertstate['visible'] = true;
			$alertstate['state'] = true;
		}
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
  <div id="header">
	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  </div>

  <div id="contents">
  	<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::PASSWORD); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>システム設定 > パスワード</strong></h2>
				<?php
					if($alertstate['visible']){
						if($alertstate['state']){
echo <<< EOF
<div class="alert alert-primary">
<strong>パスワードの変更に成功しました。</strong> <br/>
</div>
EOF;
			}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
						}
					}
				?>
				<form method="post" action="password.php" class="forms">
					<section>
						<label>ユーザ名</label>
						<?php
echo <<< EOF
<p class="lead">{$basicauth['user']}</p>
EOF;
						?>
					</section>
					<section>
						<label>現在のパスワード</label>
						<?php
echo <<< EOF
<input type="password" class="width-4 custom" name="password_oldpassword" maxlength="8" value="" placeholder="">
EOF;
						?>
					</section>
					<section>
						<label>新しいパスワード</label>
						<?php
echo <<< EOF
<input type="password" class="width-4 custom" name="password_newpassword" maxlength="8" value="" placeholder="">
EOF;
						?>
					</section>
					<section>
						<label>新しいパスワード(確認)</label>
						<?php
echo <<< EOF
<input type="password" class="width-4 custom" name="password_confirmnewpassword" maxlength="8" value="" placeholder="">
EOF;
						?>
					</section>
					<section>
						<button class="primary" name="password_submit">変更する</button>
					</section>
				</form>
	    </div>
		</div>
  </div>
</div>
</body>
</html>

<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class Reboot extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['target'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['target'])){
				return NULL;
			}
			if(strlen($cmd['target']) != 2){
				return NULL;
			}
			$cmdstr = 'S';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['target'];
			return $cmdstr;
		}

		public function response($res){
			if(strlen($res) == 5){
				$this->m_res['result'] = substr($res,3,2);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'93','result'=>'FF');
	}
?>

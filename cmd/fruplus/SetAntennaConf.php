<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class SetAntennaConf extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['antenna'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['antenna'])){
				return NULL;
			}
			if(strlen($cmd['antenna']) != 2){
				return NULL;
			}
			if(!isset($cmd['act'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['act'])){
				return NULL;
			}
			if(strlen($cmd['act']) != 2){
				return NULL;
			}
			if(!isset($cmd['power'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['power'])){
				return NULL;
			}
			if(strlen($cmd['power']) != 4){
				return NULL;
			}
			if(!isset($cmd['dwell'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['dwell'])){
				return NULL;
			}
			if(strlen($cmd['dwell']) != 4){
				return NULL;
			}
			if(!isset($cmd['invcnt'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['invcnt'])){
				return NULL;
			}
			if(strlen($cmd['invcnt']) != 4){
				return NULL;
			}
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['antenna'];
			$cmdstr .= $cmd['act'];
			$cmdstr .= $cmd['power'];
			$cmdstr .= $cmd['dwell'];
			$cmdstr .= $cmd['invcnt'];
			return $cmdstr;
		}

		public function response($res){
			if(strlen($res) == 5){
				$this->m_res['result'] = substr($res,3,2);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'84','result'=>'FF');
	}
?>

<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class WriteDO extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['mask'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['mask'])){
				return NULL;
			}
			if(strlen($cmd['mask']) != 2){
				return NULL;
			}
			if(!isset($cmd['output'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['output'])){
				return NULL;
			}
			if(strlen($cmd['output']) != 2){
				return NULL;
			}
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['mask'];
			$cmdstr .= $cmd['output'];
			return $cmdstr;
		}

		public function response($res){
			if(strlen($res) == 5){
				$this->m_res['result'] = substr($res,3,2);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'53','result'=>'FF');
	}
?>

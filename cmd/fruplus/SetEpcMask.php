<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class SetEpcMask extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['enable'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['enable'])){
				return NULL;
			}
			if(strlen($cmd['enable']) != 2){
				return NULL;
			}
			if($cmd['enable'] == '01'){
				if(!isset($cmd['offset'])){
					return NULL;
				}
				if(!ctype_xdigit($cmd['offset'])){
					return NULL;
				}
				if(strlen($cmd['offset']) != 2){
					return NULL;
				}
				if(!isset($cmd['length'])){
					return NULL;
				}
				if(!ctype_xdigit($cmd['length'])){
					return NULL;
				}
				if(strlen($cmd['length']) != 2){
					return NULL;
				}
				if(!isset($cmd['data'])){
					return NULL;
				}
				if(!ctype_xdigit($cmd['data'])){
					return NULL;
				}
			}
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['enable'];
			if($cmd['enable'] == '01'){
				$cmdstr .= $cmd['offset'];
				$cmdstr .= $cmd['length'];
				$cmdstr .= $cmd['data'];
			}
			return $cmdstr;
		}

		public function response($res){
			if(strlen($res) == 5){
				$this->m_res['result'] = substr($res,3,2);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'86','result'=>'FF');
	}
?>

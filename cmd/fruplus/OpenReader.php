<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class OpenReader extends AbstractCommand
	{
		public function command($cmd){
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			return $cmdstr;
		}

		public function response($res){
			if(strlen($res) == 9){
				$this->m_res['result'] = substr($res,3,2);
				$this->m_res['error'] = substr($res,5,4);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'00','result'=>'FF','error'=>'0000');
	}
?>

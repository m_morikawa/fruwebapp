<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class GetAntennaConf extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['antenna'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['antenna'])){
				return NULL;
			}
			if(strlen($cmd['antenna']) != 2){
				return NULL;
			}
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['antenna'];
			return $cmdstr;
		}

		public function response($res){
			$this->m_res['result'] = substr($res,3,2);
			if($this->m_res['result'] == '00'){
				$this->m_res['act'] = substr($res,5,2);
				$this->m_res['power'] = substr($res,7,4);
				$this->m_res['dwell'] = substr($res,11,4);
				$this->m_res['invcnt'] = substr($res,15,4);
			}else{
				unset($this->m_res['act']);
				unset($this->m_res['power']);
				unset($this->m_res['dwell']);
				unset($this->m_res['invcnt']);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'83','result'=>'FF','act'=>'00','power'=>'0064','dwell'=>'07D0','invcnt'=>'2000');
	}
?>

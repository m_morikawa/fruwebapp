<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class ReadDIO extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['mask'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['mask'])){
				return NULL;
			}
			if(strlen($cmd['mask']) != 2){
				return NULL;
			}

			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['mask'];
			return $cmdstr;
		}

		public function response($res){
			$this->m_res['result'] = substr($res,3,2);
			if($this->m_res['result'] == '00'){
				$this->m_res['dio'] = substr($res,5,2);
			}else{
				unset($this->m_res['dio']);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'52','result'=>'FF','dio'=>'00');
	}
?>

<?php
	abstract class AbstractCommand{
		abstract protected function command($cmd);
		abstract protected function response($res);
	}
?>
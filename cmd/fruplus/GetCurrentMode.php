<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class GetCurrentMode extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['target'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['target'])){
				return NULL;
			}
			if(strlen($cmd['target']) != 2){
				return NULL;
			}
			$cmdstr = 'S';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['target'];
			return $cmdstr;
		}

		public function response($res){
			$this->m_res['result'] = substr($res,3,2);
			if($this->m_res['result'] == '00'){
				$this->m_res['mode'] = substr($res,5,2);
			}else{
				unset($this->m_res['mode']);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'94','result'=>'FF','mode'=>'FF');
	}
?>

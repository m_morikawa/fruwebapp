<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class SetCurrentConf extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['address'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['address'])){
				return NULL;
			}
			if(strlen($cmd['address']) != 2){
				return NULL;
			}
			if(!isset($cmd['count'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['count'])){
				return NULL;
			}
			if(strlen($cmd['count']) != 2){
				return NULL;
			}
			if(!isset($cmd['data'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['data'])){
				return NULL;
			}
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['address'];
			$cmdstr .= $cmd['count'];
			$cmdstr .= $cmd['data'];
			return $cmdstr;
		}

		public function response($res){
			if(strlen($res) == 5){
				$this->m_res['result'] = substr($res,3,2);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'88','result'=>'FF');
	}
?>

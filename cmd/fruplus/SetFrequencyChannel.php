<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class SetFrequencyChannel extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['ch'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['ch'])){
				return NULL;
			}
			if(strlen($cmd['ch']) != 2){
				return NULL;
			}
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['ch'];
			return $cmdstr;
		}

		public function response($res){
			if(strlen($res) == 5){
				$this->m_res['result'] = substr($res,3,2);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'82','result'=>'FF');
	}
?>

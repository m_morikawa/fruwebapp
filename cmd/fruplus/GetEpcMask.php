<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class GetEpcMask extends AbstractCommand
	{
		public function command($cmd){
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			return $cmdstr;
		}

		public function response($res){
			$this->m_res['result'] = substr($res,3,2);
			if($this->m_res['result'] == '00'){
				$this->m_res['enable'] = substr($res,5,2);
				if($this->m_res['enable'] == '01'){
					$this->m_res['offset'] = substr($res,7,2);
					$this->m_res['length'] = substr($res,9,2);
					$this->m_res['data'] = substr($res,11,strlen($res) - 11);
				}else{
					unset($this->m_res['offset']);
					unset($this->m_res['length']);
					unset($this->m_res['data']);
				}
			}else{
				unset($this->m_res['enable']);
				unset($this->m_res['offset']);
				unset($this->m_res['length']);
				unset($this->m_res['data']);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'85','result'=>'FF','enable'=>'00','offset'=>'','length'=>'','data'=>'');
	}
?>

<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class SetDIOConf extends AbstractCommand
	{
		public function command($cmd){
			if(!isset($cmd['dioconf'])){
				return NULL;
			}
			if(!ctype_xdigit($cmd['dioconf'])){
				return NULL;
			}
			if(strlen($cmd['dioconf']) != 2){
				return NULL;
			}
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			$cmdstr .= $cmd['dioconf'];
			return $cmdstr;
		}

		public function response($res){
			if(strlen($res) == 5){
				$this->m_res['result'] = substr($res,3,2);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'8B','result'=>'FF');
	}
?>

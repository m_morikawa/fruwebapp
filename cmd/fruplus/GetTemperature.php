<?php
	spl_autoload_register(function ($class_name) {
		include $class_name . '.php';
	});

	class GetTemperature extends AbstractCommand
	{
		public function command($cmd){
			$cmdstr = 'P';
			$cmdstr .= $cmd['cmd'];
			return $cmdstr;
		}

		public function response($res){
			$this->m_res['result'] = substr($res,3,2);
			if($this->m_res['result'] == '00'){
				$this->m_res['ambientTemp'] = substr($res,5,8);
				$this->m_res['ambientTempThreshold'] = substr($res,13,8);
				$this->m_res['icTemp'] = substr($res,21,8);
				$this->m_res['icTempThreshold'] = substr($res,29,8);
				$this->m_res['powerAmpTemp'] = substr($res,37,8);
				$this->m_res['powerAmpTempThreshold'] = substr($res,45,8);
			}else{
				unset($this->m_res['ambientTemp']);
				unset($this->m_res['ambientTempThreshold']);
				unset($this->m_res['icTemp']);
				unset($this->m_res['icTempThreshold']);
				unset($this->m_res['powerAmpTemp']);
				unset($this->m_res['powerAmpTempThreshold']);
			}
			return $this->m_res;
		}

		private $m_res = array('type'=>'Q','cmd'=>'8C','result'=>'FF','ambientTemp'=>'','ambientTempThreshold'=>'','icTemp'=>'','icTempThreshold'=>'','powerAmpTemp'=>'','powerAmpTempThreshold'=>'');
	}
?>

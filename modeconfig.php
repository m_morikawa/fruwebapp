<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'ModeParameter');
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}

	$selectedmode = 1;
	if(isset($_POST["selectedmode"])){//submitした場合
		$selectedmode = $_POST["selectedmode"];
	}
	else if(isset($_POST["modetype"]))//select boxを選択した場合
	{
		if($_POST["modetype"] === "manual"){
			$selectedmode = 0;
		}else if($_POST["modetype"] === "realtime"){
			$selectedmode = 1;
		}else if($_POST["modetype"] === "inventory"){
			$selectedmode = 2;
		}else if($_POST["modetype"] === "inoutmonitor"){
			$selectedmode = 3;
		}
	}
	else//初めて表示した場合
	{
		call_user_func_array(function(&$selectedmode){
			exec('sudo frucgi mode',$mode);
			$state = json_decode($mode[0],true);
			if(strpos($state['state'], "Manualmode") !== FALSE){
				$selectedmode = 0;
			}else if(strpos($state['state'], "Mode1") !== FALSE){
				$selectedmode = 1;
			}else if(strpos($state['state'], "Mode2") !== FALSE){
				$selectedmode = 2;
			}else if(strpos($state['state'], "Mode3") !== FALSE){
				$selectedmode = 3;
			}
		},array(&$selectedmode));
	}
	$_SESSION['currentmode'] =  $selectedmode;

	$model = new Model();
	$rfpower = $model->rfPower();
	$deffreqch = $model->defaultFreqCh();
	$optionsToIndex = $model->chToIndex();
	$options = $model->channelStrings();

	$parameter = new ModeParameter();
	$realtimemode = $parameter->xml()->realtimemode;
	$inventorymode = $parameter->xml()->inventorymode;
	$inoutmonitormode = $parameter->xml()->inoutmonitormode;
	$manualmode = $parameter->xml()->manualmode;
	$alertstate = array('visible' => false,'result'=> false,'msg' => '');

	if(isset($_POST['mode_submit'])){
		$msg = array();
		$ret = FALSE;
		if(!filter_has_var(INPUT_POST, 'modetype')){
			$msg[] = "モードが選択されていません。";
		}else{
			if($_POST['modetype'] === "manual"){
				require_once("function/validate_manualmode.php");
			}else if($_POST['modetype'] === "realtime"){
				require_once("function/validate_realtimemode.php");
			}else if($_POST['modetype'] === "inventory"){
				require_once("function/validate_inventorymode.php");
			}else if($_POST['modetype'] === "inoutmonitor"){
				require_once("function/validate_inoutmonitormode.php");
			}
			$ret = validate_mode($msg);
		}

		if(!$ret){
			$alertstate['visible'] = true;
			$alertstate['state'] = false;
			foreach($msg as $value){
				$alertstate['msg'] .= $value;
				$alertstate['msg'] .= "<br/>";
			}
		}else{
			if($_POST['modetype'] === "manual"){
				require_once("function/apply_manualmode.php");
			}else if($_POST['modetype'] === "realtime"){
				require_once("function/apply_realtimemode.php");
			}else if($_POST['modetype'] === "inventory"){
				require_once("function/apply_inventorymode.php");
			}else if($_POST['modetype'] === "inoutmonitor"){
				require_once("function/apply_inoutmonitormode.php");
			}
			apply_mode();		

			$parameter->save();
			exec('sudo frucgi update mode',$res);	
			$status = json_decode($res[0],true);
			if(strpos($status['status'],'success') !== false){
				$alertstate['visible'] = true;
				$alertstate['state'] = true;
			}else{
				$alertstate['visible'] = true;
				$alertstate['state'] = false;
				$alertstate['msg']  = "モード設定に失敗しました。";
			}
		}
	}
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<script>
function onWindowLoaded(){
	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
  <div id="header">
	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  </div>


  <div id="contents">
  	<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::MODE); ?>
			<div id="main">
				<h2><span class="batch" style="padding-right:12px;" data-icon="&#xF06B;"></span><strong>動作モード設定 > モード</strong></h2>
				<?php
					if($alertstate['visible']){
						if($alertstate['state']){
echo <<< EOF
<form method="post" action="index.php" class="forms" id="updateform">
<input type="hidden" name="modeupdateflag" value="update">
<div class="alert alert-primary">
<strong>モード設定を変更しました。</strong> <br/>
この設定をリーダーに反映して、動作モードを再起動するためには、以下の<strong>設定を反映する</strong>ボタンをクリックして下さい。
<div style="padding-top:24px;">
<script>
function onCancelUpdate(){
document.getElementById("updateform").style.display = "none";
}
</script>
<button id="btn_do_update" class="width-4 primary"><strong>設定を反映する</strong></button>
<button id="btn_cancel_update" type="button" class="width-4 secondary" onclick="onCancelUpdate()">今は反映しない</button>
<script>
function onUpdate(){
document.getElementById('btn_do_update').style.disabled = true;
document.getElementById('btn_do_update').style.background = "#4CAF50";
document.getElementById('btn_do_update').innerHTML = "設定を反映しました";
setTimeout(function(){
document.getElementById('updateform').submit();
return true;
}, 3000)
return false;
}

if(document.getElementById('btn_do_update').addEventListener){
document.getElementById('btn_do_update').addEventListener("click", onUpdate, false);
}else if(document.getElementById('btn_do_update').attachEvent){
document.getElementById('btn_do_update').attachEvent("onclick", onUpdate);
}else{
document.getElementById('btn_do_update').onclick = onUpdate;
}
</script>
</div>
</div>
</form>
EOF;
			}else{
echo <<< EOF
<div class="alert alert-error">
{$alertstate['msg']}
</div>
EOF;
						}
					}
				?>
				<form method="post" action="modeconfig.php" class="forms" id="modeconfigform">
					<fieldset>
						<legend><h5>動作モード</h5></legend>
						<section>
							<label>動作モード</label>
							<row>
								<column cols="6">
									<select class="select custom" id="currentmode" name="currentmode">
										<?php
											if( $selectedmode == 1){
												echo "<option value=1 selected>モード1</option>";
												echo "<option value=2>モード2</option>";
												echo "<option value=3>モード3</option>";
												echo "<option value=0>マニュアルモード</option>";
											}else if ( $selectedmode == 2){
												echo "<option value=1>モード1</option>";
												echo "<option value=2 selected>モード2</option>";
												echo "<option value=3>モード3</option>";
												echo "<option value=0>マニュアルモード</option>";
											}else if ( $selectedmode == 3){
												echo "<option value=1>モード1</option>";
												echo "<option value=2>モード2</option>";
												echo "<option value=3 selected>モード3</option>";
												echo "<option value=0>マニュアルモード</option>";
											}else if( $selectedmode == 0){
												echo "<option value=1>モード1</option>";
												echo "<option value=2>モード2</option>";
												echo "<option value=3>モード3</option>";
												echo "<option value=0 selected>マニュアルモード</option>";
											}else{
												echo "<option value=1 selected>モード1</option>";

												echo "<option value=2>モード2</option>";
												echo "<option value=3>モード3</option>";
												echo "<option value=0>マニュアルモード</option>";
											}
										?>
									</select>
								</column>
							</row>
							<script>
								function onCurrentmodeChanged(){
									var element = document.createElement('input');

									element.name = 'selectedmode';
									var select = document.getElementById('currentmode');
									var options = document.getElementById('currentmode').options;
									element.value = options.item(select.selectedIndex).value;
									element.style.display = "none";

									document.getElementById("modeconfigform").appendChild(element);
									document.getElementById("modeconfigform").submit();
								}
								if(document.getElementById("currentmode").addEventListener){
									document.getElementById("currentmode").addEventListener("change", onCurrentmodeChanged, false);
								}else if(document.getElementById("currentmode").attachEvent){
									document.getElementById("currentmode").attachEvent("onchange", onCurrentmodeChanged);
								}else{
									document.getElementById("currentmode").onchange = onCurrentmodeChanged;
								}
							</script>
						</section>
					</fieldset>
				</form>
				<form method="post" action="modeconfig.php" class="forms">
					<?php
					if( $selectedmode == 1){
						include(dirname(__FILE__).'/realtimemode.php');
					}else if ( $selectedmode == 2){
						include(dirname(__FILE__).'/inventorymode.php');
					}else if ( $selectedmode == 3){
						include(dirname(__FILE__).'/inoutmonitormode.php');
					}else if( $selectedmode == 0){
						include(dirname(__FILE__).'/manualmode.php');
					}else{
						include(dirname(__FILE__).'/realtimemode.php');
					}
					?>
					<section>
						<button class="primary" name="mode_submit">変更する</button>
						<button class="secondary" type="button" name="mode_toCurrent" onclick="onToCurrentClicked()">現在の設定に戻す</button>
						<button class="secondary" type="button" name="mode_toDefault" onclick="onToDefaultClicked()">デフォルトに戻す</button>
					</section>
				</form>
	    </div>
		</div>
  </div>
</div>
</body>
</html>

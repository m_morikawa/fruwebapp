<input type="hidden" name="modetype" value="inoutmonitor">
<fieldset>
	<legend><h5>動作設定</h5></legend>
	<section>
		<h4>新しいEPCコードを持ったタグを検知した際に、上位へタグデータを送信します。</h4>
		<p>イベント通知を有効にするかどうか設定してください。</p>
		<section>
			<label>検知時イベント通知</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" name="event_in" id="event_in">
					<?php
						if ( $inoutmonitormode->event->in == "true"){
							echo "<option value=true selected>通知を行う</option><option value=false>通知を行わない</option>";
						} else if ( $inoutmonitormode->event->in == "false"){
							echo "<option value=true>通知を行う</option><option value=false selected>通知を行わない</option>";
						}
					?>
					</select>
				</column>
			</row>
		</section>
	</section>
	<section>
		<h4>新しいEPCコードを持ったタグを検知した際に、外部出力を制御できます。</h4>
		<p>外部出力に関する設定をしてください。</p>
		<section>
			<label>検知時外部出力制御</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" id="dout_in_enabled" name="dout_in_enabled">
						<?php
							if ( $inoutmonitormode->dout->in->enabled == "true"){
								echo "<option value=true selected>外部出力を制御する</option><option value=false>外部出力を制御しない</option>";
							} else if ( $inoutmonitormode->dout->in->enabled == "false"){
								echo "<option value=true>外部出力を制御する</option><option value=false selected>外部出力を制御しない</option>";
							}
						?>
					</select>
				</column>
			</row>
			<script>
				function onInDoutEnabledChanged(){
					if(this.options[this.selectedIndex].value == "true"){
						document.getElementById("indoutconfig").style.display="block";
					}else if(this.options[this.selectedIndex].value == "false"){
						document.getElementById("indoutconfig").style.display="none";
					}
				}
				if(document.getElementById("dout_in_enabled").addEventListener){
					document.getElementById("dout_in_enabled").addEventListener("change", onInDoutEnabledChanged, false);
				}else if(document.getElementById("dout_in_enabled").attachEvent){
					document.getElementById("dout_in_enabled").attachEvent("onchange", onInDoutEnabledChanged);
				}else{
					document.getElementById("dout_in_enabled").onchange = onInDoutEnabledChanged;
				}
			</script>
		</section>
		<div id="indoutconfig">
			<section>
				<label>ポート</label>
				<row>
					<column cols="6">
						<select class="select custom" class="width-10" name="dout_in_port">
						<?php
							if ( $inoutmonitormode->dout->in->port == "0"){
								echo "<option value=0 selected>ポート0</option><option value=1>ポート1</option>";
							} else if ( $inoutmonitormode->dout->in->port == "1"){
								echo "<option value=0>ポート0</option><option value=1 selected>ポート1</option>";
							}
						?>
						</select>
					</column>
				</row>
			</section>
			<section>
				<label>保持時間 : ms</label>
				<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="dout_in_downtime" value="{$inoutmonitormode->dout->in->downtime}" placeholder="{$inoutmonitormode->dout->in->downtime}">
EOF;
				?>
			</section>	
			<?php
				if ( $inoutmonitormode->dout->in->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("indoutconfig").style.display="block";
</script>
EOF;
				} else if ( $inoutmonitormode->dout->in->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("indoutconfig").style.display="none";
</script>
EOF;
				}
			?>
		</div>
	</section>
	<section>
		<h4>検知していたタグが検知されなくなった際に、上位へタグデータを送信します。</h4>
		<p>イベント通知を有効にするかどうか設定してください。</p>
		<section>
			<label>圏外時イベント通知</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" name="event_out" id="event_out">
					<?php
						if ( $inoutmonitormode->event->out == "true"){
							echo "<option value=true selected>通知を行う</option><option value=false>通知を行わない</option>";
						} else if ( $inoutmonitormode->event->out == "false"){
							echo "<option value=true>通知を行う</option><option value=false selected>通知を行わない</option>";
						}
					?>
					</select>
				</column>
			</row>
		</section>
	</section>
	<section>
		<h4>検知していたタグが検知されなくなった際に、外部出力を制御できます。</h4>
		<p>外部出力に関する設定をしてください。</p>
		<section>
			<label>圏外時外部出力制御</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" id="dout_out_enabled" name="dout_out_enabled">
						<?php
							if ( $inoutmonitormode->dout->out->enabled == "true"){
								echo "<option value=true selected>外部出力を制御する</option><option value=false>外部出力を制御しない</option>";
							} else if ( $inoutmonitormode->dout->out->enabled == "false"){
								echo "<option value=true>外部出力を制御する</option><option value=false selected>外部出力を制御しない</option>";
							}
						?>
					</select>
				</column>
			</row>
			<script>
				function onOutDoutEnabledChanged(){
					if(this.options[this.selectedIndex].value == "true"){
						document.getElementById("outdoutconfig").style.display="block";
					}else if(this.options[this.selectedIndex].value == "false"){
						document.getElementById("outdoutconfig").style.display="none";
					}
				}
				if(document.getElementById("dout_out_enabled").addEventListener){
					document.getElementById("dout_out_enabled").addEventListener("change", onOutDoutEnabledChanged, false);
				}else if(document.getElementById("dout_out_enabled").attachEvent){
					document.getElementById("dout_out_enabled").attachEvent("onchange", onOutDoutEnabledChanged);
				}else{
					document.getElementById("dout_out_enabled").onchange = onOutDoutEnabledChanged;
				}
			</script>
		</section>
		<div id="outdoutconfig">
			<section>
				<label>ポート</label>
				<row>
					<column cols="6">
						<select class="select custom" class="width-10" name="dout_out_port">
						<?php
							if ( $inoutmonitormode->dout->out->port == "0"){
								echo "<option value=0 selected>ポート0</option><option value=1>ポート1</option>";
							} else if ( $inoutmonitormode->dout->out->port == "1"){
								echo "<option value=0>ポート0</option><option value=1 selected>ポート1</option>";
							}
						?>
						</select>
					</column>
				</row>
			</section>
			<section>
				<label>保持時間 : ms</label>
				<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="dout_out_downtime" value="{$inoutmonitormode->dout->out->downtime}" placeholder="{$inoutmonitormode->dout->out->downtime}">
EOF;
				?>
			</section>	
			<?php
				if ( $inoutmonitormode->dout->out->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("outdoutconfig").style.display="block";
</script>
EOF;
				} else if ( $inoutmonitormode->dout->out->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("outdoutconfig").style.display="none";
</script>
EOF;
				}
			?>
		</div>
	</section>
	<section>
		<div style="padding-bottom:16px;">
			<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
			<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
				タイムアウト時間とは、タグが最後に検知されてから、タグがアンテナ圏外に出たと判断するまでの時間のことを示します。
			</div>
		</div>
		<label>タイムアウト時間 : ms</label>
		<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="event_timeout" value="{$inoutmonitormode->event->timeout}" placeholder="{$inoutmonitormode->event->timeout}">
EOF;
		?>
	</section>
	<section>
		<h4>イベントにより上位へ通知するデータを設定できます。</h4>
		<p>通知データ設定してください。</p>
		<section>
			<label>通知データ</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" name="event_datacond">
					<?php
						if ( $inoutmonitormode->event->datacond == "trans"){
							echo "<option value=trans selected>イベントが発生したタグの情報のみ送信する</option><option value=all>検知しているすべてのタグ情報を送信する</option>";
						} else if ( $inoutmonitormode->event->datacond == "all"){
							echo "<option value=trans>イベントが発生したタグの情報のみ送信する</option><option value=all selected>検知しているすべてのタグ情報を送信する</option>";
						}
					?>
					</select>
				</column>
			</row>
		</section>
	</section>
	<section>
		<h4>リーダーの送信データに対して、上位サーバーからレスポンスを返すことで外部出力を制御できます。</h4>
		<p>サーバーレスポンスに関する設定をしてください。</p>
		<section>
			<label>サーバーレスポンス</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" name="response_enabled" id="response_enabled">
					<?php
						if ( $inoutmonitormode->response->enabled == "true"){
							echo "<option value=true selected>有効</option><option value=false>無効</option>";
						} else if ( $inoutmonitormode->response->enabled == "false"){
							echo "<option value=true>有効</option><option value=false selected>無効</option>";
						}
					?>
					</select>
				</column>
			</row>
			<script>
				function onResponseEnabledChanged() {
					if(this.options[this.selectedIndex].value == "true"){
						document.getElementById("responseconfig").style.display="block";
					}else if(this.options[this.selectedIndex].value == "false"){
						document.getElementById("responseconfig").style.display="none";
					}
				}
				if(document.getElementById("response_enabled").addEventListener){
					document.getElementById("response_enabled").addEventListener("change", onResponseEnabledChanged, false);
				}else if(document.getElementById("response_enabled").attachEvent){
					document.getElementById("response_enabled").attachEvent("onchange", onResponseEnabledChanged);
				}else{
					document.getElementById("response_enabled").onchange = onResponseEnabledChanged;
				}
			</script>
		</section>
		<div id="responseconfig">
			<section>
				<div style="padding-bottom:16px;">
					<span class="label label-primary" outline bold><span style="font-size:13.5px;" class="batch" data-icon="&#xF139;"></span>INFO</span>
					<div style="border-width:1px 0 1px 0;border-color:#283593;border-style:solid;margin:8px 16px 8px 0px;padding:8px 8px;">
						リーダーは上位へデータ送信後、タイムアウト時間レスポンスを待ちます。<br>
						レスポンス待ちの間は、上位へのデータ送信は行われずそのデータは破棄されます。
					</div>
				</div>
				<label>タイムアウト時間 : ms</label>
				<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="response_timeout" value="{$inoutmonitormode->response->timeout}" placeholder="{$inoutmonitormode->response->timeout}">
EOF;
				?>
			</section>
			<?php
				if ( $inoutmonitormode->response->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("responseconfig").style.display="block";
</script>
EOF;
				} else if ( $inoutmonitormode->response->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("responseconfig").style.display="none";
</script>
EOF;
				}
			?>
		</div>
	</section>
	<section>
		<h4>上位サーバーからのリクエストで外部出力を制御できます。</h4>
		<p>サーバーリクエストに関する設定をしてください。</p>
		<section>
			<label>サーバーリクエスト</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" name="request_enabled" id="request_enabled">
					<?php
						if ( $inoutmonitormode->request->enabled == "true"){
							echo "<option value=true selected>有効</option><option value=false>無効</option>";
						} else if ( $inoutmonitormode->request->enabled == "false"){
							echo "<option value=true>有効</option><option value=false selected>無効</option>";
						}
					?>
					</select>
				</column>
			</row>
		</section>
	</section>
	<section>
		<h4>検知しているタグ情報を定期的に上位へ送信することができます。</h4>
		<p>モニタデータ送信を有効にするかどうか設定してください。</p>
		<section>
			<label>モニタデータ送信</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" name="monitor_enabled" id="monitor_enabled">
					<?php
						if ( $inoutmonitormode->monitor->enabled == "true"){
							echo "<option value=true selected>有効</option><option value=false>無効</option>";
						} else if ( $inoutmonitormode->monitor->enabled == "false"){
							echo "<option value=true>有効</option><option value=false selected>無効</option>";
						}
					?>
					</select>
				</column>
			</row>
			<script>
				function onMonitorEnabledChanged(){
					if(this.options[this.selectedIndex].value == "true"){
						document.getElementById("monitorconfig").style.display="block";
					}else if(this.options[this.selectedIndex].value == "false"){
						document.getElementById("monitorconfig").style.display="none";
					}
				}
				if(document.getElementById("monitor_enabled").addEventListener){
					document.getElementById("monitor_enabled").addEventListener("change", onMonitorEnabledChanged, false);
				}else if(document.getElementById("monitor_enabled").attachEvent){
					document.getElementById("monitor_enabled").attachEvent("onchange", onMonitorEnabledChanged);
				}else{
					document.getElementById("monitor_enabled").onchange = onMonitorEnabledChanged;
				}
			</script>
		</section>
		<div id="monitorconfig">
			<section>
				<label>モニタデータ送信インターバル : s</label>
				<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="monitor_interval" value="{$inoutmonitormode->monitor->interval}" placeholder="{$inoutmonitormode->monitor->interval}">
EOF;
				?>
			</section>
			<?php
				if ( $inoutmonitormode->monitor->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("monitorconfig").style.display="block";
</script>
EOF;
				} else if ( $inoutmonitormode->monitor->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("monitorconfig").style.display="none";
</script>
EOF;
				}
			?>
		</div>
	</section>
</fieldset>
<fieldset>
	<legend><h5>送信データ設定</h5></legend>
	<section>
		<h4>RFIDタグのEPCコードの指定の一部分のみ送信することができます。</h4>
		<p>EPCコードのフォーマットの設定をしてください。</p>
		<section>
			<label>EPCコードフォーマット</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" id="taginfo_format" name="taginfo_format">
					<?php
						if ( $inoutmonitormode->taginfo->format == "all"){
							echo "<option value=all selected>EPCコード</option><option value=part>EPCコードの一部</option>";
						} else if ( $inoutmonitormode->taginfo->format == "part"){
							echo "<option value=all>EPCコード</option><option value=part selected>EPCコードの一部</option>";
						}
					?>
					</select>
				</column>
			</row>
			<script>
				function onTaginfoFormatChanged(){
					if(this.options[this.selectedIndex].value == "all"){
						document.getElementById("tagformatconfig").style.display="none";
					}else if(this.options[this.selectedIndex].value == "part"){
						document.getElementById("tagformatconfig").style.display="block";
					}
				}
				if(document.getElementById("taginfo_format").addEventListener){
					document.getElementById("taginfo_format").addEventListener("change", onTaginfoFormatChanged, false);
				}else if(document.getElementById("taginfo_format").attachEvent){
					document.getElementById("taginfo_format").attachEvent("onchange", onTaginfoFormatChanged);
				}else{
					document.getElementById("taginfo_format").onchange = onTaginfoFormatChanged;
				}
			</script>
		</section>
		<div id="tagformatconfig">
			<section>
				<label>Offset : byte</label>
				<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="taginfo_offset" value="{$inoutmonitormode->taginfo->offset}" placeholder="{$inoutmonitormode->taginfo->offset}">
EOF;
				?>
			</section>
			<section>
				<label>Length : byte</label>
				<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="taginfo_length" value="{$inoutmonitormode->taginfo->length}" placeholder="{$inoutmonitormode->taginfo->length}">
EOF;
				?>
			</section>
			<?php
				if ( $inoutmonitormode->taginfo->format == "all"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("tagformatconfig").style.display="none";
</script>
EOF;
				} else if ( $inoutmonitormode->taginfo->format == "part"){
echo <<< EOF
<script type="text/javascript">
	document.getElementById("tagformatconfig").style.display="block";

</script>
EOF;
	}
			?>
		</div>
	</section>
	<section>
		<h4>EPCコードの他に指定の情報を付加して上位へ送信することができます。</h4>
		<p>付加情報の設定をしてください。</p>
		<section>
			<label>付加情報</label>
			<row>
				<column cols="6">
					<section>
						<select class="select custom" class="width-10" name="addinfo_enabled" id="addinfo_enabled">
						<?php
							if ( $inoutmonitormode->addinfo->enabled == "true"){
								echo "<option value=true selected>付加情報を送信する</option><option value=false>付加情報を送信しない</option>";
							} else if ( $inoutmonitormode->addinfo->enabled == "false"){

								echo "<option value=true>付加情報を送信する</option><option value=false selected>付加情報を送信しない</option>";
							}
						?>
						</select>
					</section>
				</column>
			</row>
			<script>
				function onAddinfoEnabledChanged(){
					if(this.options[this.selectedIndex].value == "true"){
						document.getElementById("addinfoconfig").style.display="block";
					}else if(this.options[this.selectedIndex].value == "false"){
						document.getElementById("addinfoconfig").style.display="none";
					}
				}
				if(document.getElementById("addinfo_enabled").addEventListener){
					document.getElementById("addinfo_enabled").addEventListener("change", onAddinfoEnabledChanged, false);
				}else if(document.getElementById("addinfo_enabled").attachEvent){
					document.getElementById("addinfo_enabled").attachEvent("onchange", onAddinfoEnabledChanged);
				}else{
					document.getElementById("addinfo_enabled").onchange = onAddinfoEnabledChanged;
				}
			</script>
		</section>
		<div id="addinfoconfig">
			<row>
				<column cols="6">
					<section>
						<label>端末ID</label>
						<select class="select custom" class="width-10" name="addinfo_id">
						<?php
							if ( $inoutmonitormode->addinfo->id == "true"){
								echo "<option value=true selected>付加する</option><option value=false>付加しない</option>";
							} else if ( $inoutmonitormode->addinfo->id == "false"){
								echo "<option value=true>付加する</option><option value=false selected>付加しない</option>";
							}
						?>
						</select>
					</section>
					<section>
						<label>端末時刻</label>
						<select class="select custom" class="width-10" name="addinfo_time">
						<?php
							if ( $inoutmonitormode->addinfo->time == "true"){
								echo "<option value=true selected>付加する</option><option value=false>付加しない</option>";
							} else if ( $inoutmonitormode->addinfo->time == "false"){
								echo "<option value=true>付加する</option><option value=false selected>付加しない</option>";
							}
						?>
						</select>
					</section>
					<section>
						<label>アンテナポート</label>
						<select class="select custom" class="width-10" name="addinfo_antennano">
						<?php
							if ( $inoutmonitormode->addinfo->antennano == "true"){
								echo "<option value=true selected>付加する</option><option value=false>付加しない</option>";
							} else if ( $inoutmonitormode->addinfo->antennano == "false"){
								echo "<option value=true>付加する</option><option value=false selected>付加しない</option>";
							}
						?>
						</select>
					</section>
					<section>
						<label>PCコード</label>
						<select class="select custom" class="width-10" name="addinfo_pc">
						<?php
							if ( $inoutmonitormode->addinfo->pc == "true"){
								echo "<option value=true selected>付加する</option><option value=false>付加しない</option>";
							} else if ( $inoutmonitormode->addinfo->pc == "false"){
								echo "<option value=true>付加する</option><option value=false selected>付加しない</option>";
							}
						?>
						</select>
					</section>
					<section>
						<label>RSSI</label>
						<select class="select custom" class="width-10" name="addinfo_rssi">
						<?php
							if ( $inoutmonitormode->addinfo->rssi == "true"){
								echo "<option value=true selected>付加する</option><option value=false>付加しない</option>";
							} else if ( $inoutmonitormode->addinfo->rssi == "false"){
								echo "<option value=true>付加する</option><option value=false selected>付加しない</option>";
							}
						?>
						</select>
					</section>
					<section>
						<label>タグ状態</label>
						<select class="select custom" class="width-10" name="addinfo_state">
						<?php
							if ( $inoutmonitormode->addinfo->state == "true"){
								echo "<option value=true selected>付加する</option><option value=false>付加しない</option>";
							} else if ( $inoutmonitormode->addinfo->state == "false"){
								echo "<option value=true>付加する</option><option value=false selected>付加しない</option>";
							}
						?>
						</select>
					</section>
					<?php
						if ( $inoutmonitormode->addinfo->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("addinfoconfig").style.display="block";
</script>
EOF;
						} else if ( $inoutmonitormode->addinfo->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("addinfoconfig").style.display="none";
</script>
EOF;
						}
					?>
				</column>
			</row>
		</div>
	</section>
	<section>
		<h4>動作中にエラーが発生した場合に、その情報を上位へ送信をします。</h4>
		<p>エラー情報の設定をしてください。</p>
		<section>
			<label>エラー情報</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" name="errorinfo_enabled">
						<?php
						if ( $inoutmonitormode->errorinfo->enabled == "true"){
							echo "<option value=true selected>エラー情報を送信する</option><option value=false>エラー情報を送信しない</option>";
						} else if ( $inoutmonitormode->errorinfo->enabled == "false"){
							echo "<option value=true>エラー情報を送信する</option><option value=false selected>エラー情報を送信しない</option>";
						}
						?>
					</select>
				</column>
			</row>
		</section>
	</section>
</fieldset>
<fieldset>
	<legend><h5>RFID設定</h5></legend>
	<section>
		<h4>アンテナ0の設定を行います。</h4>
		<p>アンテナ0の設定をしてください。</p>
		<section>
			<label>アンテナ0</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" name="antenna_0_enabled" id="antenna_0_enabled">
						<?php
							if ( $inoutmonitormode->antenna[0]->enabled == "true"){
								echo "<option value=true selected>アンテナ0を使用する</option><option value=false>アンテナ0を使用しない</option>";
							} else if ( $inoutmonitormode->antenna[0]->enabled == "false"){
								echo "<option value=true>アンテナ0を使用する</option><option value=false selected>アンテナ0を使用しない</option>";
							}
						?>
					</select>
				</column>
			</row>
			<script>
				function onAntenna0EnabledChanged(){
					if(this.options[this.selectedIndex].value == "true"){
						document.getElementById("antenna0config").style.display="block";
					}else if(this.options[this.selectedIndex].value == "false"){
						document.getElementById("antenna0config").style.display="none";
					}
				}
				if(document.getElementById("antenna_0_enabled").addEventListener){
					document.getElementById("antenna_0_enabled").addEventListener("change", onAntenna0EnabledChanged, false);
				}else if(document.getElementById("antenna_0_enabled").attachEvent){
					document.getElementById("antenna_0_enabled").attachEvent("onchange", onAntenna0EnabledChanged);
				}else{
					document.getElementById("antenna_0_enabled").onchange = onAntenna0EnabledChanged;
				}
			</script>
		</section>
		<div id="antenna0config">
			<section>
				<label>Power</label>
				<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_0_power" value="{$inoutmonitormode->antenna[0]->power}" placeholder="{$inoutmonitormode->antenna[0]->power}">
EOF;
				?>
			</section>
			<section>
				<label>Dwell</label>
				<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_0_dwell" value="{$inoutmonitormode->antenna[0]->dwell}" placeholder="{$inoutmonitormode->antenna[0]->dwell}">
EOF;
				?>
			</section>
			<section>
				<label>InvCnt</label>
				<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_0_invcnt" value="{$inoutmonitormode->antenna[0]->invcnt}" placeholder="{$inoutmonitormode->antenna[0]->invcnt}">
EOF;
				?>
			</section>
			<?php
				if ( $inoutmonitormode->antenna[0]->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("antenna0config").style.display="block";
</script>
EOF;
				} else if ( $inoutmonitormode->antenna[0]->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("antenna0config").style.display="none";
</script>
EOF;
				}
			?>
		</div>
	</section>
	<section>
		<h4>アンテナ1の設定を行います。</h4>
		<p>アンテナ1の設定をしてください。</p>
		<section>
			<label>アンテナ1</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" name="antenna_1_enabled" id="antenna_1_enabled">
						<?php
							if ( $inoutmonitormode->antenna[1]->enabled == "true"){
								echo "<option value=true selected>アンテナ1を使用する</option><option value=false>アンテナ1を使用しない</option>";
							} else if ( $inoutmonitormode->antenna[1]->enabled == "false"){
								echo "<option value=true>アンテナ1を使用する</option><option value=false selected>アンテナ1を使用しない</option>";
							}
						?>
					</select>
				</column>
			</row>
			<script>
				function onAntenna1EnabledChanged(){
					if(this.options[this.selectedIndex].value == "true"){
						document.getElementById("antenna1config").style.display="block";
					}else if(this.options[this.selectedIndex].value == "false"){
						document.getElementById("antenna1config").style.display="none";
					}
				}
				if(document.getElementById("antenna_1_enabled").addEventListener){
					document.getElementById("antenna_1_enabled").addEventListener("change", onAntenna1EnabledChanged, false);
				}else if(document.getElementById("antenna_1_enabled").attachEvent){
					document.getElementById("antenna_1_enabled").attachEvent("onchange", onAntenna1EnabledChanged);
				}else{

					document.getElementById("antenna_1_enabled").onchange = onAntenna1EnabledChanged;
				}
			</script>
		</section>
		<div id="antenna1config">
			<section>
				<label>Power</label>
				<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_1_power" value="{$inoutmonitormode->antenna[1]->power}" placeholder="{$inoutmonitormode->antenna[1]->power}">
EOF;
				?>
			</section>
			<section>
				<label>Dwell</label>
				<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_1_dwell" value="{$inoutmonitormode->antenna[1]->dwell}" placeholder="{$inoutmonitormode->antenna[1]->dwell}">
EOF;
				?>
			</section>
			<section>
				<label>InvCnt</label>
				<?php
echo <<< EOF
<input type="text" class="width-4 custom" name="antenna_1_invcnt" value="{$inoutmonitormode->antenna[1]->invcnt}" placeholder="{$inoutmonitormode->antenna[1]->invcnt}">
EOF;
				?>
			</section>
			<?php
				if ( $inoutmonitormode->antenna[1]->enabled == "true"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("antenna1config").style.display="block";
</script>
EOF;
				} else if ( $inoutmonitormode->antenna[1]->enabled == "false"){
echo <<< EOF
<script type="text/javascript">
document.getElementById("antenna1config").style.display="none";
</script>
EOF;
				}
			?>
		</div>
	</section>
	<section>
		<h4>周波数の設定を行います。</h4>
		<p>周波数の設定をしてください。</p>
		<section>
			<label>周波数</label>
			<row>
				<column cols="6">
					<select class="select custom" class="width-10" name="frequency_ch" id="frequency_ch">
						<?php
					    foreach ($options as $key=>$value) {
					      $selected = "";
					      if ($key == $inoutmonitormode->frequency->ch) {
						$selected = " selected";
					      }
					      echo "<option value=\"$key\"$selected>$value</option>\n";
					    }
						?>
					</select>
				</column>
			</row>
		</section>
	</section>
</fieldset>
<script>
	var rfpower = <?php echo $rfpower; ?>;
	var defaultFreqCh = <?php echo $optionsToIndex[$deffreqch]; ?>;
	function onToCurrentClicked(){
	<?php
		if($inoutmonitormode->antenna[0]->enabled == "true"){
			echo "document.getElementsByName(\"antenna_0_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"antenna0config\").style.display=\"block\";";
		}else if($inoutmonitormode->antenna[0]->enabled == "false"){
			echo "document.getElementsByName(\"antenna_0_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"antenna0config\").style.display=\"none\";";
		}
		echo "document.getElementsByName(\"antenna_0_power\")[0].value = \"".$inoutmonitormode->antenna[0]->power."\";";
		echo "document.getElementsByName(\"antenna_0_dwell\")[0].value = \"".$inoutmonitormode->antenna[0]->dwell."\";";
		echo "document.getElementsByName(\"antenna_0_invcnt\")[0].value = \"".$inoutmonitormode->antenna[0]->invcnt."\";";
		if($inoutmonitormode->antenna[1]->enabled == "true"){
			echo "document.getElementsByName(\"antenna_1_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"antenna1config\").style.display=\"block\";";
		}else if($inoutmonitormode->antenna[1]->enabled == "false"){
			echo "document.getElementsByName(\"antenna_1_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"antenna1config\").style.display=\"none\";";

		}
		echo "document.getElementsByName(\"antenna_1_power\")[0].value = \"".$inoutmonitormode->antenna[1]->power."\";";
		echo "document.getElementsByName(\"antenna_1_dwell\")[0].value = \"".$inoutmonitormode->antenna[1]->dwell."\";";
		echo "document.getElementsByName(\"antenna_1_invcnt\")[0].value = \"".$inoutmonitormode->antenna[1]->invcnt."\";";
		echo "document.getElementsByName(\"frequency_ch\")[0].selectedIndex= \"".$optionsToIndex[(int)$inoutmonitormode->frequency->ch]."\";";
		if( $inoutmonitormode->event->in == "true"){
			echo "document.getElementsByName(\"event_in\")[0].selectedIndex= \"0\";";
		} else if( $inoutmonitormode->event->in == "false"){
			echo "document.getElementsByName(\"event_in\")[0].selectedIndex= \"1\";";
		}
		if( $inoutmonitormode->event->out == "true"){
			echo "document.getElementsByName(\"event_out\")[0].selectedIndex= \"0\";";
		} else if( $inoutmonitormode->event->out == "false"){
			echo "document.getElementsByName(\"event_out\")[0].selectedIndex= \"1\";";
		}
		echo "document.getElementsByName(\"event_timeout\")[0].value = \"".$inoutmonitormode->event->timeout."\";";
		if( $inoutmonitormode->event->datacond == "trans"){
			echo "document.getElementsByName(\"event_datacond\")[0].selectedIndex= \"0\";";
		} else if( $inoutmonitormode->event->datacond == "all"){
			echo "document.getElementsByName(\"event_datacond\")[0].selectedIndex= \"1\";";
		}
		if( $inoutmonitormode->monitor->enabled == "true"){
			echo "document.getElementsByName(\"monitor_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"monitorconfig\").style.display=\"block\";";
		} else if( $inoutmonitormode->monitor->enabled  == "false"){
			echo "document.getElementsByName(\"monitor_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"monitorconfig\").style.display=\"none\";";
		}
		echo "document.getElementsByName(\"monitor_interval\")[0].value = \"".$inoutmonitormode->monitor->interval."\";";
		if ( $inoutmonitormode->taginfo->format == "all"){
			echo "document.getElementsByName(\"taginfo_format\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"tagformatconfig\").style.display=\"none\";";
		} else if ( $inoutmonitormode->taginfo->format == "part"){
			echo "document.getElementsByName(\"taginfo_format\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"tagformatconfig\").style.display=\"block\";";
		}
		echo "document.getElementsByName(\"taginfo_offset\")[0].value = \"".$inoutmonitormode->taginfo->offset."\";";
		echo "document.getElementsByName(\"taginfo_length\")[0].value = \"".$inoutmonitormode->taginfo->length."\";";
		if ( $inoutmonitormode->addinfo->enabled == "true"){
			echo "document.getElementsByName(\"addinfo_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"addinfoconfig\").style.display=\"block\";";
		} else if ( $inoutmonitormode->addinfo->enabled == "false"){
			echo "document.getElementsByName(\"addinfo_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"addinfoconfig\").style.display=\"none\";";
		}
		if ( $inoutmonitormode->addinfo->id == "true"){
			echo "document.getElementsByName(\"addinfo_id\")[0].selectedIndex= \"0\";";
		} else if ( $inoutmonitormode->addinfo->id == "false"){
			echo "document.getElementsByName(\"addinfo_id\")[0].selectedIndex= \"1\";";
		}
		if ( $inoutmonitormode->addinfo->time == "true"){
			echo "document.getElementsByName(\"addinfo_time\")[0].selectedIndex= \"0\";";
		} else if ( $inoutmonitormode->addinfo->time == "false"){
			echo "document.getElementsByName(\"addinfo_time\")[0].selectedIndex= \"1\";";
		}
		if ( $inoutmonitormode->addinfo->antennano == "true"){
			echo "document.getElementsByName(\"addinfo_antennano\")[0].selectedIndex= \"0\";";
		} else if ( $inoutmonitormode->addinfo->antennano == "false"){
			echo "document.getElementsByName(\"addinfo_antennano\")[0].selectedIndex= \"1\";";
		}
		if ( $inoutmonitormode->addinfo->pc == "true"){
			echo "document.getElementsByName(\"addinfo_pc\")[0].selectedIndex= \"0\";";
		} else if ( $inoutmonitormode->addinfo->pc == "false"){
			echo "document.getElementsByName(\"addinfo_pc\")[0].selectedIndex= \"1\";";
		}
		if ( $inoutmonitormode->addinfo->rssi == "true"){
			echo "document.getElementsByName(\"addinfo_rssi\")[0].selectedIndex= \"0\";";
		} else if ( $inoutmonitormode->addinfo->rssi == "false"){
			echo "document.getElementsByName(\"addinfo_rssi\")[0].selectedIndex= \"1\";";
		}
		if ( $inoutmonitormode->addinfo->state == "true"){
			echo "document.getElementsByName(\"addinfo_state\")[0].selectedIndex= \"0\";";
		} else if ( $inoutmonitormode->addinfo->state == "false"){
			echo "document.getElementsByName(\"addinfo_state\")[0].selectedIndex= \"1\";";
		}
		if ( $inoutmonitormode->dout->in->enabled == "true"){
			echo "document.getElementsByName(\"dout_in_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"indoutconfig\").style.display=\"block\";";
		} else if ( $inoutmonitormode->dout->in->enabled == "false"){
			echo "document.getElementsByName(\"dout_in_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"indoutconfig\").style.display=\"none\";";
		}
		if ( $inoutmonitormode->dout->in->port == "0"){
			echo "document.getElementsByName(\"dout_in_port\")[0].selectedIndex= \"0\";";
		} else if ( $inoutmonitormode->dout->in->port == "1"){
			echo "document.getElementsByName(\"dout_in_port\")[0].selectedIndex= \"1\";";
		}
		echo "document.getElementsByName(\"dout_in_downtime\")[0].value = \"".$inoutmonitormode->dout->in->downtime."\";";
		if ( $inoutmonitormode->dout->out->enabled == "true"){
			echo "document.getElementsByName(\"dout_out_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"outdoutconfig\").style.display=\"block\";";
		} else if ( $inoutmonitormode->dout->out->enabled == "false"){
			echo "document.getElementsByName(\"dout_out_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"outdoutconfig\").style.display=\"none\";";
		}
		if ( $inoutmonitormode->dout->out->port == "0"){
			echo "document.getElementsByName(\"dout_out_port\")[0].selectedIndex= \"0\";";
		} else if ( $inoutmonitormode->dout->out->port == "1"){
			echo "document.getElementsByName(\"dout_out_port\")[0].selectedIndex= \"1\";";
		}
		echo "document.getElementsByName(\"dout_out_downtime\")[0].value = \"".$inoutmonitormode->dout->out->downtime."\";";
		if ( $inoutmonitormode->response->enabled == "true"){
			echo "document.getElementsByName(\"response_enabled\")[0].selectedIndex= \"0\";";
			echo "document.getElementById(\"responseconfig\").style.display=\"block\";";
		} else if ( $inoutmonitormode->response->enabled == "false"){
			echo "document.getElementsByName(\"response_enabled\")[0].selectedIndex= \"1\";";
			echo "document.getElementById(\"responseconfig\").style.display=\"none\";";
		}
		echo "document.getElementsByName(\"response_timeout\")[0].value = \"".$inoutmonitormode->response->timeout."\";";
		if ( $inoutmonitormode->request->enabled == "true"){
			echo "document.getElementsByName(\"request_enabled\")[0].selectedIndex= \"0\";";
		} else if ( $inoutmonitormode->request->enabled == "false"){
			echo "document.getElementsByName(\"request_enabled\")[0].selectedIndex= \"1\";";
		}
		if ( $inoutmonitormode->errorinfo[0]->enabled == "true"){
			echo "document.getElementsByName(\"errorinfo_enabled\")[0].selectedIndex= \"0\";";
		} else if ( $inoutmonitormode->errorinfo[0]->enabled == "false"){
			echo "document.getElementsByName(\"errorinfo_enabled\")[0].selectedIndex= \"1\";";
		}
	?>
	}
	function onToDefaultClicked(){
		document.getElementsByName("antenna_0_enabled")[0].selectedIndex= "0";
		document.getElementById("antenna0config").style.display="block";
		document.getElementsByName("antenna_0_power")[0].value= rfpower;
		document.getElementsByName("antenna_0_dwell")[0].value= "2000";
		document.getElementsByName("antenna_0_invcnt")[0].value= "8192";
		document.getElementsByName("antenna_1_enabled")[0].selectedIndex= "1";
		document.getElementById("antenna1config").style.display="none";
		document.getElementsByName("antenna_1_power")[0].value= rfpower;
		document.getElementsByName("antenna_1_dwell")[0].value= "2000";
		document.getElementsByName("antenna_1_invcnt")[0].value= "8192";
		document.getElementsByName("frequency_ch")[0].selectedIndex = defaultFreqCh;
		document.getElementsByName("event_in")[0].selectedIndex = "0";
		document.getElementsByName("event_out")[0].selectedIndex = "0";
		document.getElementsByName("event_timeout")[0].value= "1500";
		document.getElementsByName("event_datacond")[0].selectedIndex = "0";
		document.getElementsByName("monitor_enabled")[0].selectedIndex = "1";
		document.getElementById("monitorconfig").style.display="none";
		document.getElementsByName("monitor_interval")[0].value = "10";
		document.getElementsByName("taginfo_format")[0].selectedIndex= "0";
		document.getElementById("tagformatconfig").style.display="none";
		document.getElementsByName("taginfo_offset")[0].value= "0";
		document.getElementsByName("taginfo_length")[0].value= "1";
		document.getElementsByName("addinfo_enabled")[0].selectedIndex= "0";
		document.getElementById("addinfoconfig").style.display="block";
		document.getElementsByName("addinfo_id")[0].selectedIndex= "0";
		document.getElementsByName("addinfo_time")[0].selectedIndex= "0";
		document.getElementsByName("addinfo_antennano")[0].selectedIndex= "0";
		document.getElementsByName("addinfo_pc")[0].selectedIndex= "0";
		document.getElementsByName("addinfo_rssi")[0].selectedIndex= "0";
		document.getElementsByName("addinfo_state")[0].selectedIndex= "0";
		document.getElementsByName("dout_in_enabled")[0].selectedIndex= "1";
		document.getElementById("indoutconfig").style.display="none";
		document.getElementsByName("dout_in_port")[0].selectedIndex= "0";
		document.getElementsByName("dout_in_downtime")[0].value= "100";
		document.getElementsByName("dout_out_enabled")[0].selectedIndex= "1";
		document.getElementById("outdoutconfig").style.display="none";
		document.getElementsByName("dout_out_port")[0].selectedIndex= "1";
		document.getElementsByName("dout_out_downtime")[0].value= "100";
		document.getElementsByName("response_enabled")[0].selectedIndex= "1";
		document.getElementById("responseconfig").style.display="none";
		document.getElementsByName("response_timeout")[0].value= "300";
		document.getElementsByName("request_enabled")[0].selectedIndex= "1";
		document.getElementsByName("errorinfo_enabled")[0].selectedIndex= "0";
	}
</script>

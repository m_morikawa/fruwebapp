FORMAT: 1A
HOST: https://www.example.com

# API

API仕様書のサンプルです。



# Group Articles
## Articles [/v1/articles]
### GET

記事リストを取得する。

+ Request

    + Headers

            Content-Type: application/json

+ Response 200 (application/json)

    + Attributes (ArticleList)

### POST

記事を登録する。

+ Request

    + Headers

            Content-Type: application/json

    + Attributes (ArticleCreateParams)

+ Response 201 (application/json)

    + Attributes (Article)

# Group Comments
## Comments [/v1/articles/{article_id}/comments]
### GET

記事に対するコメントリストを取得する。

+ Parameters

    + article_id: 1 (number) - 記事ID

+ Request

    + Headers

            Content-Type: application/json

+ Response 200 (application/json)

    + Attributes (CommentList)


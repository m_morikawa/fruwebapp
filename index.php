<?php
	spl_autoload_register(function ($class_name) {
		include 'class/' . $class_name . '.php';
	},'Model');

	session_start();
	if(!isset($_SESSION['macaddress']) || $_SESSION['macaddress'] == ""){
		$_SESSION['macaddress'] = trim(file_get_contents("/sys/class/net/eth0/address"));
	}
	// if(isset($_POST['modeupdateflag']) && isset($_SESSION['currentmode'])){
	// 	exec("sudo frucgi restartmode " .$_SESSION['currentmode']);
	// }
	$model = new Model();
?>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="css/kube.css">
<link rel="stylesheet" href="css/fru.custom.css">
<title>FRUCONFIG</title>
<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
	<script src="./js/html5shiv.js"></script>
	<script src="./js/respond.min.js"></script>
<![endif]-->
<?php
	if(isset($_POST['modeupdateflag']) && isset($_SESSION['currentmode'])){
echo <<< EOF
<script type="text/javascript">
pageflag='restart';
</script>
EOF;
	}
?>
<script>
function updatepage(){
	var elem = document.getElementById('updatescript');
	if(elem){
		document.getElementById('deviceinfo').removeChild(elem);
	}

	var rand = Math.floor( Math.random() * 9998 ) + 1 ;
	var script = document.createElement('script');
	script.id = "updatescript";
	script.src = './js/deviceinfo.php?rand='+rand;
	script.type = "text/javascript";
	if(script.addEventListener){
		script.addEventListener("load", function(){
			if(typeof(pageflag) == "undefined"){
				appendMode(document.getElementById('devicestate'));
			}else{
				document.getElementById('devicestate').innerHTML="モード停止中です。";
			}
			if(typeof(pageflag) == "undefined"){
				document.getElementById('restartmessage').style.display = "none";
			}else{
				delete pageflag;
			}
			setTimeout('updatepage()', 5000);
		}, false);
	}else if(script.attachEvent){
		script.attachEvent("onload", function(){
			if(typeof(pageflag) == "undefined"){
				appendMode(document.getElementById('devicestate'));
			}else{
				document.getElementById('devicestate').innerHTML="モード停止中です。";
			}
			if(typeof(pageflag) == "undefined"){
				document.getElementById('restartmessage').style.display = "none";
			}else{
				delete pageflag;
			}
			setTimeout('updatepage()', 5000);
		});
		script.onreadystatechange = function(){
				if(this.readyState=="loaded"||this.readyState=="complete"){
					if(typeof(pageflag) == "undefined"){
						appendMode(document.getElementById('devicestate'));
					}else{
						document.getElementById('devicestate').innerHTML="モード停止中です。";
					}
					if(typeof(pageflag) == "undefined"){
						document.getElementById('restartmessage').style.display = "none";
					}else{
						delete pageflag;
					}
					setTimeout('updatepage()', 5000);
				}
		}
	}
	document.getElementById('deviceinfo').appendChild(script);
}

function helpDisplay(obj,obj_legend,obj_section){
	document.getElementById(obj).style.borderLeftStyle='solid';
}


function onWindowLoaded(){
	updatepage();
	var timer = setTimeout(function(){helpDisplay('helpbox_mode')},400);
	var timer = setTimeout(function(){helpDisplay('helpbox_reader')},800);
	var timer = setTimeout(function(){helpDisplay('helpbox_system')},1200);

	document.getElementById("devicemacaddress").innerHTML="<?php echo strtoupper($_SESSION['macaddress']);?>";
}

(function (myWindow) {
	if(myWindow.addEventListener){
		myWindow.addEventListener('load', onWindowLoaded, false);
	}else if(myWindow.attachEvent){
		myWindow.attachEvent('onload', onWindowLoaded);
	}else{
		myWindow.onload = onWindowLoaded;
	}
}(window));
</script>
</head>
<body>

<div id="wrapper">
  <div id="header">
  	<?php require_once('function/put_logo.php'); put_logo(array($model, "logoPath")); ?>
	<p class="macaddress" id="devicemacaddress" style="float:right;"></p>
  </div>

  <div id="contents">
	<div id="contentsInner">
			<?php require_once('function/put_nav.php'); put_nav(NavState::NOTANY); ?>
			<div id="main">
				<div id="restartmessage">
					<?php
						if(isset($_POST['modeupdateflag']) && isset($_SESSION['currentmode'])){
echo <<< EOF
<div class="alert alert-primary">
<strong>モードを再起動しています。</strong><Br>
モードが再起動すると、LEDAとBが点滅します。<Br>
しばらくお待ち下さい。
</div>
EOF;
						}
					?>
				</div>
				<div>
					<div id="deviceinfo" style="background:#CFD8DC;border-color:#607D8B;border-width: 4px 0px 4px 0px;border-style: solid;padding-top:24px;">
						<h2 id="devicestate" style="color:#616161;padding:24px 24px 24px 24px;text-align:left;"></h2>
						<p style="font-size:1em;color:#424242;padding:10px 24px 12px 24px;text-align:left;">設定は<strong>左側のメニュー</strong>から変更することができます。</p>
					</div>
				</div>
				<div style="padding-top:36px;">
					<div style="padding:12px 0px;">
						<div id="helpbox_mode" style="padding:12px 12px;background:transparent;border-left-color:#607D8B;border-left-width:4px;border-left-style:hidden;">
							<legend><h4><span class="batch" data-icon="&#xF13D;" style="padding-right:12px;"></span>リーダーの動作を設定する</h4></legend>
							<section style="padding-left:12px;">
								リーダーは、自律的に動くモード1/2/3とコマンドで制御するマニュアルモードを選択することができます。<Br>
							  	モードの選択は<a href="modeconfig.php#"><strong>動作モード設定のモード</strong></a>から行います。<Br>
								モード1/2/3の場合は、その他に<a href="transfer.php#"><strong>送信先</strong></a>・<a href="filter.php#"><strong>フィルター</strong></a>の設定を行います。
							</section>
						</div>
					</div>
					<div style="padding:12px 0px;">
						<div id="helpbox_reader" style="padding:12px 12px;background:transparent;border-left-color:#607D8B;border-left-width:4px;border-left-style:hidden;">
							<legend><h4><span class="batch" data-icon="&#xF13D;" style="padding-right:12px;"></span>起動時パラメータを設定する</h4></legend>
							<section style="padding-left:12px;">
								マニュアルモードを選択時の、リーダーの起動時のパラメータを<strong>リーダー設定</strong>から行います。<Br>
							  起動後のアンテナやDIOの設定コマンドの発行を省くことができます。
							</section>
						</div>
					</div>
					<div style="padding:12px 0px;">
						<div id="helpbox_system" style="padding:12px 12px;background:transparent;border-left-color:#607D8B;border-left-width:4px;border-left-style:hidden;">
							<legend><h4><span class="batch" data-icon="&#xF13D;" style="padding-right:12px;"></span>ネットワークや時刻を設定する</h4></legend>
							<section style="padding-left:12px;">
								リーダーのIPアドレスの設定や時刻合わせは<strong>システム設定</strong>から行います。
							</section>
						</div>
					</div>
				</div>
	    </div>
		</div>
  </div>
</div>
</body>
</html>

<?php
	exec('sudo frucgi mode',$mode);
	$res = json_decode($mode[0],true);
	$nowMode = isset($res['state']) ? $res['state'] : "端末からの応答がありません。";
	if(strpos($nowMode,'Booting') !== false){
		$nowMode = "端末はブート中です。";
	}else if(strpos($nowMode,'Ready') !== false){
		$nowMode = "モード起動準備中です。";
	}else if(strpos($nowMode,'Stopping') !== false){
		$nowMode = "モード停止中です。";
	}else if(strpos($nowMode,'Error') !== false){
		$nowMode = "モード起動中にエラーが発生しました。<Br>ログを確認してください。";
	}else if(strpos($nowMode,'Directmode') !== false){
		$nowMode = "<strong>ダイレクトモード</strong>が起動中です。";
	}else if(strpos($nowMode,'Manualmode') !== false){
		$nowMode = "<strong>マニュアルモード</strong>が起動中です。";
	}else if(strpos($nowMode,'Mode1') !== false){
		$nowMode = "<strong>モード1</strong>が起動中です。";
	}else if(strpos($nowMode,'Mode2') !== false){
		$nowMode = "<strong>モード2</strong>が起動中です。";
	}else if(strpos($nowMode,'Mode3') !== false){
		$nowMode = "<strong>モード3</strong>が起動中です。";
	}else if(strpos($nowMode,'Recovery') !== false){
		$nowMode = "端末の初期化が終了しました。 <Br><span style=\"text-decoration: underline\">電源の入り切りをしてください。</span>";
	}else if(strpos($nowMode,'RwConfigUpdating') !== false){
		$nowMode = "リーダー設定を更新しています。";
	}
	else if(strpos($nowMode,'RwConfigUpdateCompleted') !== false){
		$nowMode = "リーダー設定の更新が完了しました。 <Br><span style=\"text-decoration: underline\">電源の入り切りをしてください。</span>";
	}else if(strpos($nowMode,'RwConfigUpdateFailed') !== false){
		$nowMode = "リーダー設定の更新が中断しました。";
	}
	echo 'function appendMode(elem)';
	echo '{';
	echo 'elem.innerHTML=\'';
echo <<< EOF
{$nowMode}
EOF;
	echo '\'';
	echo '}';
?>

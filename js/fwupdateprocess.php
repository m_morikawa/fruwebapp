﻿<?php
echo <<< EOF
var updateret = false;
EOF;
	exec("sudo frucgi dev disconnect",$ret);
	exec("sudo frucgi stop",$ret);
	sleep(5);

	$ret = array();
	if(isset($_GET['kernel'])){
		unset($ret);
		exec("sudo frucgi kernelupdate",$ret);
		$status = json_decode($ret[0],true);
		if(strpos($status['status'],'success') === false){
echo <<< EOF
window.alert('カーネルの更新に失敗しました。FWの更新を終了します。');
EOF;
			exec("sudo frucgi fwupdate2",$ret);
			exit(1);
		}else{
echo <<< EOF
window.alert('カーネルの更新が完了しました。');
EOF;
		}
	}
	if(isset($_GET['userland'])){
		unset($ret);
		exec("sudo frucgi userlandupdate",$ret);
		$status = json_decode($ret[0],true);
		if(strpos($status['status'],'success') === false){
echo <<< EOF
window.alert('ユーザーランドの更新に失敗しました。');
EOF;
			exec("sudo frucgi fwupdate2",$ret);
			exit(1);
		}else{
echo <<< EOF
window.alert('ユーザーランドの更新が完了しました。FWの更新を終了します。');
EOF;
		}
	}
echo <<< EOF
updateret=true;
EOF;
	exec("sudo frucgi fwupdate2",$ret);
?>

<?php
	if(!isset($_GET['logtype'])){
		exit(1);
	}
	if($_GET['logtype'] == 'system'){
		$filepath = '/var/log/messages';
		$cmd = "sudo chmod 604 %s";
		exec(sprintf($cmd, $filepath), $msg);
	}else if($_GET['logtype'] == 'app'){
		$filepath = '/var/log/applog.log';
	}else if($_GET['logtype'] == 'sendaction'){
		exec('sudo frucgi log sendaction save', $msg);
		$filepath = '/var/log/sendaction.log';
	}else if($_GET['logtype'] == 'network'){
		exec('sudo frucgi log network save', $msg);
		$filepath = '/var/log/network.log';
	}else{
		exit(1);
	}

	$loglines = file($filepath,FILE_IGNORE_NEW_LINES|FILE_SKIP_EMPTY_LINES);
	if($loglines === FALSE){
		exit(1);
	}
	if(count($loglines) == 0){
		exit(1);
	}

	echo 'function appendLog(list)';
	echo '{';
	echo 'var fragment = document.createDocumentFragment();';
	if($loglines !== FALSE){
		foreach ($loglines as $line) {
			$line = str_replace("'", "", $line);
			$line = strip_tags($line);
			echo 'var log = document.createElement(\'li\');';
			echo 'log.innerHTML = \'';
			echo $line;
			echo '\';';
			echo 'fragment.appendChild(log);';
		}
	}
	echo 'list.appendChild(fragment);';
	echo '}';
?>
